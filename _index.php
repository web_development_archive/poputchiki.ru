<html>
<head>
	<title>Сайт онлайн-знакомств для путешествий Poputchiki.ru. Интернет-сайт поиска попутчиков</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<meta content="/favicon.png" itemprop="image">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

</head>
<body>
	<div style='position:fixed; width:100%; height:100%; background:url("/beach.jpg"); background-size:cover; z-index:-1000;'></div>
	
	<div style='padding:20px;'>
	<div class="container" style="max-width:600px; background:rgba(255, 255, 255, 0.9);  border-radius: 15px; padding:20px;">
		<h4><img src="logo_blue.png" style="width:64px;display:inline"/>Сайт онлайн-знакомств для путешествий Poputchiki.ru</h4>
		<h3>Регистрация на сайте знакомств Попутчики.ру</h3>
		<hr />
		<form id="motor" action="script.php?action=new_user" method="post">
			<div class="form-group">
				<label>Email</label>
				<input type="text" name="email" id="email" value="" placeholder="Email *" class="form-control" />
			</div>
			<div class="form-group">
				<label>Телефон</label>
				<input type="text" name="phone" id="phone" value="" placeholder="Телефон (необязательно)" class="form-control" />
			</div>
			<div class="form-group">
				<label>Пароль</label>
				<input type="password" name="password" id="pass" value="" placeholder="Пароль *" class="form-control" />
			</div>
			<div class="form-group">
				<label>Подтверждение пароля</label>
				<input type="password" name="password2" id="pass2" value="" placeholder="Подтверждение пароля *" class="form-control" />
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-default" value="Регистрация" />
			</div>		
		</form>
		<hr />
		<h3>О сайте</h3>
		<div class="text-left">
		<p>Билет в кармане, такси в аэропорт, еще пару часов, и Москва станет похожей на крошечный островок из бетона, изрезанный серыми улицами. Есть и другой вариант — дорога, поднятый к верху большой палец на вытянутой руке, а впереди тысяча километров и море новых впечатлений. Так начинается то, чего все мы ждем с нетерпением — путешествие. </p>

<p>Стоп! А как же компания? Путешествовать в одиночестве скучно, неинтересно, дорого. Нужен попутчик в отпуск? Нет проблем! Используйте интернет-сайт знакомств Poputchiki.ru, чтобы ездить по всему миру, находя новых друзей по интересам. </p>

<p><b>Как найти попутчика на отдых?</b></p>
<p>Заходите на Poputchiki.ru, регистрируйтесь и создавайте свой аккаунт — это очень просто! 
Просматривайте анкеты участников, ищите попутчиков и выбирайте тех, кто нравится. 
Приглашайте присоединиться к собственному путешествию из Москвы и других городов или предлагайте свой вариант отдыха. </p>
<p>Общайтесь лично и через сеть знакомств, делитесь информацией о поездках, выкладывайте фотки и ролики. </p>
<p>Получайте удовольствие от общения с интересными людьми, живите так, как хочется, дышите полной грудью! </p>
<p><b>Чем хорош онлайн-сайт знакомств Poputchiki.ru? </b></p>
<p>Самое важное — тематика. Это не просто бесплатный сайт знакомств, которых в Сети сейчас масса. Сервис позволяет находить единомышленников, спонсоров и просто увлеченных путешествиями людей, которые готовы составить компанию, частично или полностью оплатить вашу поездку. Здесь можно найти свою судьбу и съездить в один из роскошных отелей ОАЭ, спонсора для восхождения на Эльбрус, классного собеседника, друга или подругу. Это бесплатная сеть знакомств, объединяющая всех, кто живет свободно, активно и нацелен на познание. </p>
<p>Устали от одиночества или хотите развеяться? Пришло время сообщить всем: «Хочу отдохнуть! Ищу попутчика!». Путешествие, полное позитивных эмоций, после этого вам гарантировано! Заводите знакомства в Москве и других городах на Poputchiki.ru, чтобы ездить по миру, общаться с неординарными людьми, жить интересно и весело!</p>
<hr>Попутчики © 2004—2014 «Попутчики»</div>
	</div>
</div>	
	<script>
		$(function(){
			$('#motor').submit(function(){
				if($('#email').val() == ''){
					alert('Введите Email');
					return false;
				}
				
				if($('#pass').val() == ''){
					alert('Введите Пароль');
					return false;
				}
				
				if($('#pass2').val() != $('#pass').val()){
					alert('Пароль и подтверждение пароля должны совпадать');
					return false;
				}
				
				if(!isValidEmail($('#email').val())){
					alert('Введен некорректный email');
					return false;
				}
			});
		});
		
		function isValidEmail (email, strict)
		{
		 if ( !strict ) email = email.replace(/^\s+|\s+$/g, '');
		 return (/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i).test(email);
		}
	</script>
	
	<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter27388643 = new Ya.Metrika({id:27388643,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27388643" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	
	
</body>
</html>