<?php
include "social.php";
include ("core.php");
/*!@#
if(!get_magic_quotes_gpc())
{
array_callback($_GET, 'addslashes');
array_callback($_POST, 'addslashes');
array_callback($_COOKIE, 'addslashes');
array_callback($_REQUEST, 'addslashes');
$HTTP_GET_VARS = &$_GET;
$HTTP_POST_VARS = &$_POST;
$HTTP_COOKIE_VARS = &$_COOKIE;
}
*/

 if(isset($_REQUEST['token']))
 	session_id($_REQUEST['token']);
else if(isset($_COOKIE['token']))
	session_id($_COOKIE['token']);
	
session_start();

if($_REQUEST['func']!='login' && $_REQUEST['func']!='remind_password' && $_REQUEST['func']!='new_user' && $_REQUEST['func']!='vk_login' && $_REQUEST['func']!='fb_login' && $_REQUEST['func']!='vk_login_start' && $_REQUEST['func']!='fb_login_start')
{
    if(!isset($_SESSION['user_id']))
        die(json_encode(array('status'=>-1,'error'=>'Нужно войти в систему')));
}

if($_REQUEST['func']=='login')
	 $json = json_encode(login($_REQUEST['login'],$_REQUEST['pass']));
else if($_REQUEST['func']=='register')
	 $json = json_encode(register($_REQUEST['login'],$_REQUEST['pass']));
else if($_REQUEST['func']=='logout')
	 $json = json_encode(logout());
else if($_REQUEST['func']=='forgot_password')
	 $json = json_encode(forgot_password($_REQUEST['email']));
else if($_REQUEST['func']=='save_user_data')
	 $json = json_encode(save_user_data($_REQUEST['nm'],$_REQUEST['country'],$_REQUEST['city'],$_REQUEST['about'],$_REQUEST['birthday'],$_REQUEST['is_host'],$_REQUEST['is_sponsor'],$_REQUEST['growth'],$_REQUEST['weight']));
else if($_REQUEST['func']=='post_image')
	 $json = json_encode(post_image($_REQUEST['img']));
else if($_REQUEST['func']=='get_user_profile')
	 $json = json_encode(get_user_profile($_REQUEST['user_id']));
else if($_REQUEST['func']=='get_user_media')
	 $json = json_encode(get_user_media($_REQUEST['user_id']));
else if($_REQUEST['func']=='get_statuses')
	 $json = json_encode(get_statuses($_REQUEST['offset'],$_REQUEST['txt'],$_REQUEST['sex'],$_REQUEST['city'],$_REQUEST['country']));
else if($_REQUEST['func']=='get_fav')
	 $json = json_encode(get_fav());
else if($_REQUEST['func']=='search_media')
	 $json = json_encode(search_media());
else if($_REQUEST['func']=='get_visitors')
	 $json = json_encode(get_visitors());
else if($_REQUEST['func']=='set_status')
	 $json = json_encode(set_status($_REQUEST['status_text']));
else if($_REQUEST['func']=='get_promo_strip')
	 $json = json_encode(get_promo_strip());
else if($_REQUEST['func']=='add_to_promo')
	 $json = json_encode(add_to_promo($_REQUEST['media_id'],$_REQUEST['type']));
else if($_REQUEST['func']=='search_people')
	 $json = json_encode(search_people($_REQUEST['destinations'],$_REQUEST['host'],$_REQUEST['sponsor'],$_REQUEST['country'],$_REQUEST['agemin'],$_REQUEST['agemax'],$_REQUEST['heighttmin'],$_REQUEST['growthmax'],$_REQUEST['weightmin'],$_REQUEST['weightmax'],$_REQUEST['sex']));
else if($_REQUEST['func']=='invite')
	 $json = json_encode(invite($_REQUEST['user_id']));
else if($_REQUEST['func']=='get_user_status')
	 $json = json_encode(get_user_status($_REQUEST['user_id']));
else if($_REQUEST['func']=='delete_from_fav')
	 $json = json_encode(delete_from_fav($_REQUEST['user_id']));
else if($_REQUEST['func']=='add_to_fav')
	 $json = json_encode(add_to_fav($_REQUEST['user_id']));
else if($_REQUEST['func']=='change_user_avatar')
	 $json = json_encode(change_user_avatar($_REQUEST['avatar_id']));
else if($_REQUEST['func']=='topup')
	 $json = json_encode(topup($_REQUEST['amount']));
else if($_REQUEST['func']=='set_invisible_status')
	 $json = json_encode(set_invisible_status($_REQUEST['value']));
else if($_REQUEST['func']=='get_updates')
	 $json = json_encode(get_updates($_REQUEST['type']));
else if($_REQUEST['func']=='get_photo_likes')
	 $json = json_encode(get_photo_likes($_REQUEST['photo_id']));
else if($_REQUEST['func']=='get_video_likes')
	 $json = json_encode(get_video_likes($_REQUEST['photo_id']));
else if($_REQUEST['func']=='get_status_likes')
	 $json = json_encode(get_status_likes($_REQUEST['photo_id']));
else if($_REQUEST['func']=='get_country')
	 $json = json_encode(get_country($_REQUEST['starts_with']));
else if($_REQUEST['func']=='get_city')
	 $json = json_encode(get_city($_REQUEST['starts_with'],$_REQUEST['country']));
else if($_REQUEST['func']=='like_photo')
	 $json = json_encode(like_photo($_REQUEST['photo_id']));
else if($_REQUEST['func']=='unlike_photo')
	 $json = json_encode(unlike_photo($_REQUEST['photo_id']));
else if($_REQUEST['func']=='unlike_status')
	 $json = json_encode(unlike_status($_REQUEST['status_id']));
else if($_REQUEST['func']=='like_status')
	 $json = json_encode(like_status($_REQUEST['status_id']));
else if($_REQUEST['func']=='get_updates_count')
	 $json = json_encode(get_updates_count());
else if($_REQUEST['func']=='mark_update')
	 $json = json_encode(mark_update($_REQUEST['update_id']));
else if($_REQUEST['func']=='switch_on_vip')
	 $json = json_encode(switch_on_vip($_REQUEST['duration']));
else if($_REQUEST['func']=='get_all_dialogs')
	 $json = json_encode(get_all_dialogs());
else if($_REQUEST['func']=='read_message')
	 $json = json_encode(read_message($_REQUEST['message_id']));
else if($_REQUEST['func']=='get_messages_with_user')
	 $json = json_encode(get_messages_with_user($_REQUEST['user_id']));
else if($_REQUEST['func']=='delete_message')
	 $json = json_encode(delete_message($_REQUEST['message_id']));
else if($_REQUEST['func']=='add_to_black_list')
	 $json = json_encode(add_to_black_list($_REQUEST['user_id']));
else if($_REQUEST['func']=='remove_from_black_list')
	 $json = json_encode(remove_from_black_list($_REQUEST['userId']));
else if($_REQUEST['func']=='send_message')
	 $json = json_encode(send_message($_REQUEST['txt'],$_REQUEST['img'],$_REQUEST['user_id']));
else if($_REQUEST['func']=='remove_messages')
	 $json = json_encode(remove_messages($_REQUEST['user_id']));
else if($_REQUEST['func']=='travel')
	 $json = json_encode(travel($_REQUEST['country'],$_REQUEST['phone']));
else if($_REQUEST['func']=='delete_updates')
	 $json = json_encode(delete_updates($_REQUEST['type']));
else if($_REQUEST['func']=='unlike_video')
	 $json = json_encode(unlike_video($_REQUEST['video_id']));
else if($_REQUEST['func']=='like_video')
	 $json = json_encode(like_video($_REQUEST['video_id']));
else if($_REQUEST['func']=='post_audio')
	 $json = json_encode(post_audio($_REQUEST['audio']));
else if($_REQUEST['func']=='post_video')
	 $json = json_encode(post_video($_REQUEST['video']));
else if($_REQUEST['func']=='push_token')
	 $json = json_encode(push_token($_REQUEST['token']));
else if($_REQUEST['func']=='delete_photo')
	 $json = json_encode(delete_photo($_REQUEST['photo_id']));
else if($_REQUEST['func']=='delete_video')
	 $json = json_encode(delete_video($_REQUEST['video_id']));
else if($_REQUEST['func']=='delete_audio')
	 $json = json_encode(delete_audio($_REQUEST['audio_id']));

else if($_REQUEST['func']=='push_test')
	 $json = json_encode(push_test());
else if($_REQUEST['func']=='vk_login')
	 $json = json_encode(vk_login($_REQUEST['code']));
else if($_REQUEST['func']=='fb_login')
	 $json = json_encode(fb_login($_REQUEST['code']));
else if($_REQUEST['func']=='vk_login_start')
	 $json = json_encode(vk_login_start($_REQUEST['code']));
else if($_REQUEST['func']=='fb_login_start')
	 $json = json_encode(fb_login_start());
else if($_REQUEST['func']=='new_user')
	 $json = json_encode(new_user($_REQUEST['login'], $_REQUEST['pass']));
else if($_REQUEST['func']=='remind_password')
	 $json = json_encode(remind_password($_REQUEST['login']));
else if($_REQUEST['func']=='edit_user')
	 $json = json_encode(edit_user($_REQUEST['name'], $_REQUEST['phone'], $_REQUEST['login'], $_REQUEST['old_pass'], $_REQUEST['new_pass']));
else if($_REQUEST['func']=='get_user')
	 $json = json_encode(get_user($_REQUEST['user_id']));
	 
	 
	 
	  
	 

if(isset($_REQUEST['pretty']))
    echo "<pre>".pretty($json)."</pre>";
else
    echo $json;



function pretty($json)
{

    $result      = '';
    $pos         = 0;
    $strLen      = strlen($json);
    $indentStr   = '  ';
    $newLine     = "\n";
    $prevChar    = '';
    $outOfQuotes = true;

    for ($i=0; $i<=$strLen; $i++)
    {

        // Grab the next character in the string.
        $char = substr($json, $i, 1);

        // Are we inside a quoted string?
        if ($char == '"' && $prevChar != '\\')
        {
            $outOfQuotes = !$outOfQuotes;

            // If this character is the end of an element,
            // output a new line and indent the next line.
        } else if(($char == '}' || $char == ']') && $outOfQuotes)
        {
            $result .= $newLine;
            $pos --;
            for ($j=0; $j<$pos; $j++)
            {
                $result .= $indentStr;
            }
        }

        // Add the character to the result string.
        $result .= $char;

        // If the last character was the beginning of an element,
        // output a new line and indent the next line.
        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes)
        {
            $result .= $newLine;
            if ($char == '{' || $char == '[')
            {
                $pos ++;
            }

            for ($j = 0; $j < $pos; $j++)
            {
                $result .= $indentStr;
            }
        }

        $prevChar = $char;
    }

}
?>