 <?php

ini_set('error_reporting', E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);

include_once "soc.php";
include "db.php";
session_start();

define("ROOT", $_SERVER['DOCUMENT_ROOT']);  
$config = array(
    'home' => 'http://poputchiki.ru/index.php',
	'site' => 'http://poputchiki.ru/',
    'rowPerPage' => 10,
    'uploadAvatar'=>'uploads/images/',
    'images' => '/photos/',
    'thumbs' => '/photos/thumbs/',
    'imagesUpload' => 'photos/',
    'thumbsDir' => 'photos/thumbs/',
    'rowPerPage' => 20,
    'defaultAvatarMini' => 'http://firepic.org/images/2014-07/24/nl3phjadnfm2.png'
);

function get_purchase($pur_id){
	$q = q("SELECT * FROM purchase_items WHERE id = :id", array('id' => $pur_id));
	return $q[0];
}

function password_recovery(){
	
	$email = $_POST['email'];
	
	if(empty($email)){
		buildMsg('Введите E-mail', 'danger');
		return false;
	}
	
	$user = q("SELECT * FROM users WHERE login = :login", array('login' => $email));
	
	if($user == nulL){
		buildMsg('Пользователь с таким E-mail адресом не найден', 'danger');
		return false;
	}
	
	$pass = generatePassword(8);
	$hashed = md5($pass);
	
	if(q2("UPDATE users SET pass = :pass WHERE id = :id", array('pass' => $hashed, 'id' => $user[0]['id']))){
		
		$message = "Ваш новый пароль на сайте poputchiki.ru: ".$pass;
		send_email($email, 'Новый пароль на сайте poputchiki.ru', $message);
		buildMsg('Новый пароль отправлен на почту');
		return true;
	}
	
	return false;
	
}

function activate_user($hash){
	
	q2("UPDATE users SET active = 1 WHERE hash = :hash", array('hash' => $hash));
	buildMsg('Вы успешно активировали свой аккаунт. Теперь вы можете войти в систему');
	return true;
	
}

function send_email($to, $subject, $body, $file_path = null, $file_name = null){

	include_once "phpmailer/PHPMailerAutoload.php";

	$email = new PHPMailer();
	$email->CharSet = 'UTF-8';
	$email->From      = 'info@rtserver.ru';
	$email->FromName  = 'Попутчики';
	$email->Subject   = $subject;
	$email->Body      = $body;
	$email->AddAddress($to);
	$email->IsHTML(true);
	
	if($file_path != null){
		$file_to_attach = $file_path;
		$email->AddAttachment( $file_to_attach , $file_name );
	}
	
	return $email->Send();
}

function startSocketServer()
{
	//if($_SESSION['admin']!=1) return;	
	shell_exec("nohup php /var/www/lelouch/data/www/poputchiki.ru/server/serv.php > /var/www/lelouch/data/www/poputchiki.ru/server/serv.log 2>&1  </dev/null &");		
}
	
function stopSocketServer()
{
	//if($_SESSION['admin']!=1) return;
	shell_exec("kill -TERM $(cat /var/www/lelouch/data/www/poputchiki.ru/server/serv.pid)");
}
	
function killSocketServer() {
	//if($_SESSION['admin']!=1) return;
	shell_exec("pkill -9 -f serv.php");
	//truncateConnections();
	@unlink('/var/www/lelouch/data/www/poputchii.ru/server/serv.pid');
}

function get_user($user_id){
	
	$q = q("SELECT * FROM users WHERE id = :id", array('id' => $user_id));
	return $q[0];
	
}

function send_msg_in_profile($user_id, $txt){
	
	if(!is_numeric($user_id)){
		return array('status' => 0, 'msg' => 'Не смешно'.$txt);
	}
	
	if($user_id == 0){
		return array('status' => 0, 'msg' => 'Выберите пользователя');
	}
	
	if(empty($txt)){
		return array('status' => 0, 'msg' => 'Введите сообщение');
	}
	
	q2("INSERT INTO messages(sender_id, receiver_id, dt, txt) VALUES(:sender_id, :receiver_id, :dt, :txt)", array(
		'sender_id' => $_SESSION['user']['id'],
		'receiver_id' => $user_id,
		'dt' => date('Y-m-d H:i:s'),
		'txt' => $txt
	));
	
	return array('status' => 1, 'msg' => 'Сообщение успешно отправлено');
	
}

function localizeDate($tm)
{
		$tm = str_replace("Dec", "дек", $tm);
		$tm = str_replace("Jan", "янв", $tm);
		$tm = str_replace("Feb", "фев", $tm);
		$tm = str_replace("Mar", "мар", $tm);
		$tm = str_replace("Apr", "апр", $tm);
		$tm = str_replace("May", "май", $tm);
		$tm = str_replace("Jun", "июн", $tm);
		$tm = str_replace("Jul", "июл", $tm);
		$tm = str_replace("Aug", "авг", $tm);
		$tm = str_replace("Sep", "сен", $tm);
		$tm = str_replace("Oct", "окт", $tm);
		$tm = str_replace("Nov", "ноя", $tm);	
		return $tm;	
}
	
function fancyTime($tm)
{
		$today = strtotime("00:00:01");
		$yesterday = strtotime("-1 day", $today);
		if($tm>$today)
			return date("H:i", $tm);
		else if ($tm<$today &&$tm>$yesterday)
			return "вчера";
		else return localizeDate(date("d M",$tm));
}

function get_messages_between($user_id1, $user_id2) {
	
	$q = q("SELECT m.*, 
			CONCAT(u.first_name, ' ', u.last_name) as fullname_sender, 
			CONCAT(u1.first_name, ' ', u1.last_name) as fullname_receiver, 
			IF(m.sender_id=u.id, 1, 0) as isender,
			IF(m.sender_id=u.id, p.url, p1.url) as ava
			FROM messages m 
			LEFT JOIN users u ON (u.id=m.sender_id) 
			LEFT JOIN users u1 ON (u1.id=m.receiver_id) 
			LEFT JOIN photos p ON (u.avatar_id=p.id)
			LEFT JOIN photos p1 ON (u1.avatar_id=p1.id)
			WHERE (m.sender_id = :id1 AND m.receiver_id = :id2) OR (m.sender_id = :id4 AND m.receiver_id = :id3) 
			group by m.id order by m.dt desc LIMIT 0, 20", array('id1' => $user_id1, 'id2' => $user_id2, 'id3' => $user_id1, 'id4' => $user_id2));
	return $q;
	
}

function get_user_dialogs($user_id){
	
	//$q = q("SELECT m.*, CONCAT(u.first_name, ' ', u.last_name) as fullname_sender, CONCAT(u1.first_name, ' ', u1.last_name) as fullname_receiver, IF(m.sender_id=u.id, 1, 0) as isender FROM messages m LEFT JOIN users u ON (u.id=m.sender_id) LEFT JOIN users u1 ON (u1.id=m.receiver_id) WHERE m.sender_id = :id OR m.receiver_id = :id1 GROUP BY m.sender_id, m.receiver_id order by m.dt desc",  array('id' => $user_id, 'id1' => $user_id));
	$q = q("SELECT * FROM 
			(SELECT m.*, CONCAT(u.first_name, ' ', u.last_name) as fullname_sender, 
			CONCAT(u1.first_name, ' ', u1.last_name) as fullname_receiver, 
			IF(m.sender_id = :id2, m.receiver_id, m.sender_id) as target_id,
			IF(m.sender_id=:id, 1, 0) as isender,
			IF(m.sender_id = :id4, p1.url, p.url) as ava
			FROM messages m 
			LEFT JOIN users u ON (u.id=m.sender_id) 
			LEFT JOIN users u1 ON (u1.id=m.receiver_id) 
			LEFT JOIN photos p ON (u.avatar_id=p.id)
			LEFT JOIN photos p1 ON (u1.avatar_id=p1.id)
			WHERE m.receiver_id = :id1 or m.sender_id = :id3
			order by m.dt desc) as inv 
			group by target_id", array('id1' => $user_id, 'id' => $user_id, 'id2' => $user_id, 'id3' => $user_id, 'id4'=>$user_id));
	return $q;
}

function send_msg($msg, $tokens)
{

	include_once ("./push/push.php");
	send_push($msg, $tokens, $badge);
}


function getUser($id)
{
    $user = q(SQL_GET_USER, array('id' => $id));    
    return $user[0];
}



function getUserByLogin($login)
{
    $q = q(SQL_GET_USER_BY_LOGIN, array('login' => $login));
    if($q == null) return false;
    return $q[0];
}

function is_login_busy($login)
{	
	$user = q(SQL_CHECK_LOGIN, array('login' => $login));
	
	if($user == null) return false;
	else return true;
}

function generatePassword($length = 8){
    $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
    $numChars = strlen ($chars);
    $string = '';
    for ($i = 0; $i < $length; $i++) {
        $string .= substr ($chars, rand (1, $numChars) - 1, 1);
    }
    return $string;
}


function push_test()
{
	$tokens = q("SELECT * FROM push_tokens", array());	
	$arr = Array();
	for( $i=0; $i<count($tokens); $i++)
	{
		$arr[] = $tokens[$i]['push_token'];
	}
	send_msg("It works!", $arr);
}



function login($login,$pass)
 { 	$check = q("SELECT u.* FROM users u WHERE u.login = :login AND u.pass=MD5(:pass)", array("login" => $login, "pass" => $pass));

    if(!is_null($check[0]["login"]))
    {
        $_SESSION["user_id"] = $check[0]["id"];
		$_COOKIE["token"] = session_id();        
        return array("status"=>0, "token"=>session_id(), "user" => $check[0]);
    }else
    {
        return array("status"=>1, "error"=>"В доступе отказано");
    } 
 } 

function register($login,$pass)
 { 
	return array('status'=>0);
			 
 } 

function sess_destroy(){
	unset($_SESSION['user']);
	session_destroy();
	return true;
}
 
function logout()
 { 	
	unset($_SESSION["user_id"]);
    q2("DELETE FROM push_tokens WHERE push_token = :tok", array("tok" => $push_token));
    session_destroy();
    return array("status"=>-1); 
 } 

function forgot_password($email)
 { 
	return array('status'=>0);
			 
 } 

function save_user_data($nm,$country,$city,$about,$birthday,$is_host,$is_sponsor,$growth,$weight)
 { 
	return array('status'=>0);
			 
 } 

function post_image($img)
 { 
	return array('status'=>0);
			 
 } 

function get_preferences($like)
{
    $k = q(SQL_GET_PREFERENCE_OF_USER,array('id'=>$like));
    return $k[0];
}

function get_user_profile($user_id)
 {  
	$res = q('SELECT * from `users` where id=:id',array('id'=>$user_id));
	return array('status'=>0,'response'=>$res[0]);
			 
 } 

function get_user_media($user_id)
 { 
	return array('status'=>0);
			 
 }

// временная

function get_likes($id)
{
    $k = q(SQL_GET_STATS_LIKES,array('id'=>$id));
    return $k[0];
}

function get_statuses($like)//$offset,$txt,$sex,$city,$country)
 {
     $k = q(SQL_GET_STATS_LIKE,array('this'=>$like));

     if(isset($k))
     {
         return json_encode($k);
     }else
     {
         return 1;// q(SQL_GET_STATS,null);
     }
 } 

function get_fav()
 { 
	return array('status'=>0);
			 
 } 

function search_media()
 { 
	return array('status'=>0);
			 
 } 

function get_visitors()
 { 
	return q(SQL_GET_STATS_GUEST,array('user_id'=>$_SESSION['user']['id']));
			 
 }
// Временная функция
function set_visitors($u)
{
        q2(SQL_SET_VISITOR,array('user_id'=>$u,'guest_id'=>$_SESSION['user']['id']));
    return true;
}

function set_status($status_text,$user_id)
 {

     $stat_id = q(SQL_GET_STATUS_OF_USER,array('id'=> $user_id));
     $d = date('Y-m-d H:i:s');

     if(!empty($stat_id[0]))
     {
         q2(SQL_UPDATE_STATUS,array('text'=>$status_text,'datet'=>$d,'id'=>$stat_id[0]['id']));
         return $status_text;
     }else
     {
         q2(SQL_SET_STATUS,array('user_id'=>$user_id,'status_text'=>$status_text,'dt'=>$d));
         return $status_text;
     }

	return "Ошибка статуса";
			 
 } 

function get_promo_strip()
 { 
	return array('status'=>0);
			 
 } 

function add_to_promo($media_id,$type)
 { 
	return array('status'=>0);
			 
 } 

function search_people($destinations,$host,$sponsor,$country,$agemin,$agemax,$heighttmin,$growthmax,$weightmin,$weightmax,$sex)
 { 
	return array('status'=>0);
			 
 } 

function invite($user_id)
 { 
	return array('status'=>0);
			 
 } 

function get_user_status($user_id)
 { 
	return array('status'=>0);
 } 

function delete_from_fav($user_id)
 {
     q2(SQL_DEL_FAV,array('user_id'=>$_SESSION['user']['id'],'fav_id'=>$user_id));
 } 

// временная функция
function send_gift($gift_id,$send)
{
    $u = getUser($_SESSION['user']['id']);
    $g = get_gift($gift_id);
    if($u['balance']>$g['price'])
    {
        $balance = $u['balance'] - $g['price'];
        q2(SQL_USER_UPDATE_BALANCE,array('user_id'=>$_SESSION['user']['id'],'balance'=>$balance));
        set_user_gifts($send,$_SESSION['user']['id'],$gift_id);
        return true;
    }else return false;

}


function add_to_fav($user_id)
 {
     if(q(SQL_GET_FAV,array('user_id'=>$_SESSION['user']['id'],'fav_id'=>$user_id))==null)
     {
         q2(SQL_SET_FAV,array('user_id'=>$_SESSION['user']['id'],'fav_id'=>$user_id));
         return true;
     }else
     {
         q2(SQL_DEL_FAV,array('user_id'=>$_SESSION['user']['id'],'fav_id'=>$user_id));
         return false;
     }
	return false;
 }
// временная функция
function likes_check($like_id)
{
    if(q(SQL_GET_STAT1,array('user_id'=>$_SESSION['user']['id'],'satus_id'=>$like_id))==null)
    {
        q2(SQL_SET_STAT1,array('user_id'=>$_SESSION['user']['id'],'satus_id'=>$like_id));
        $s = q(SQL_GET_STATS_LIKES,array('id'=>$like_id));
        return $s[0]['likes'];
    }else
    {
        q2(SQL_DEL_STAT1,array('user_id'=>$_SESSION['user']['id'],'satus_id'=>$like_id));
        $s = q(SQL_GET_STATS_LIKES,array('id'=>$like_id));
        return $s[0]['likes'];
    }
    $s = q(SQL_GET_STATS_LIKES,array('id'=>$like_id));
    return $s[0]['likes'];
}

// временная функция
function photo_likes_check($like_id)
{
    if(q(SQL_GET_LIKE_PHOTO,array('user_id'=>$_SESSION['user']['id'],'photo_id'=>$like_id))==null)
    {
        q2(SQL_SET_LIKE_PHOTO,array('user_id'=>$_SESSION['user']['id'],'photo_id'=>$like_id));
        $s = q(SQL_GET_PHOTO_LIKES,array('id'=>$like_id));
        return $s[0]['likes'];
    }else
    {
        q2(SQL_DEL_LIKE_PHOTO,array('user_id'=>$_SESSION['user']['id'],'photo_id'=>$like_id));
        $s = q(SQL_GET_PHOTO_LIKES,array('id'=>$like_id));
        return $s[0]['likes'];
    }
    $s = q(SQL_GET_PHOTO_LIKES,array('id'=>$like_id));
    return $s[0]['likes'];
}

// временная функция

function check_favorite($user_id)
{
    return q(SQL_GET_FAV,array('user_id'=>$_SESSION['user']['id'],'fav_id'=>$user_id));
}

// временная функция

function get_visithors()
{
    return q(SQL_GET_STATS_GUEST,array('user_id'=>$_SESSION['user']['id']));
}


// временная функция

function get_all_gifts()
{
    return q(SQL_GET_ALL_GIFTS,null);
}

// временная функция

function get_gift($id)
{
    $k =  q(SQL_GET_GIFT,array('id'=>$id));
    return $k[0];
}

// временная функция

function get_user_gifts($id)
{
    return q(SQL_GET_USER_GIFTS,array('user_id'=>$id));
}

// временная функция

function set_user_gifts($response,$request,$gift_id)
{
    return q2(SQL_SET_GIFT,array('gift_id'=>$gift_id,'user_request'=>$request,'user_response'=>$response));
}

// временная функция

function get_favorite($status_id)
{
    return q(SQL_GET_STATS_FAV,array('user_id'=>$status_id));
}
function get_favorite_rand()
{
    return q(SQL_GET_STATS_FAV_RAND,null);
}
function get_favorite_rand_m()
{
    return q(SQL_GET_STATS_FAV_RAND_M,null);
}

function change_user_avatar($avatar_id)
 { 
	return array('status'=>0);
			 
 } 

function topup($amount)
 { 
	return array('status'=>0);
			 
 } 

function set_invisible_status($value)
 {
     $u = get_user_profile($value);

     if($u['response']['invisible']==1)
     {
         q2(SQL_USER_UPDATE_INVISIBLE, array('sponsor' => 0, 'user_id' => $value));
         return false;
     }else if($u['response']['invisible']==0)
     {
         q2(SQL_USER_UPDATE_INVISIBLE, array('sponsor' => 1, 'user_id' => $value));
         return true;
     }
 }
// Временые функции
function set_sponsor_status($value)
{
    $u = get_user_profile($value);

    if($u['response']['is_sponsor']==1)
    {
        q2(SQL_USER_UPDATE_SPONSOR, array('sponsor' => 0, 'user_id' => $value));
        return false;
    }else if($u['response']['is_sponsor']==0)
    {
        q2(SQL_USER_UPDATE_SPONSOR, array('sponsor' => 1, 'user_id' => $value));
        return true;
    }
}

// Временые функции
function set_interes_city($value)
{
    q2(SQL_UPDATE_PREFERENCE_CITY, array('city_id' => $value, 'user_id' => $_SESSION['user']['id']));
    $k =  q(SQL_GET_CITY_NAME,array('id'=>$value));
    return $k[0]['nm'];
}
// Временые функции
function set_interes_age($age_f,$age_s)
{
    return q2(SQL_UPDATE_PREFERENCE_AGE, array('age_f' => $age_f,'age_s'=>$age_s, 'user_id' => $_SESSION['user']['id']));
}

// Временые функции
function set_interes_width($width_f,$width_s)
{
    return q2(SQL_UPDATE_PREFERENCE_WIDTH, array('width_f' => $width_f,'width_s'=>$width_s, 'user_id' => $_SESSION['user']['id']));
}

// Временые функции
function del_interes_directions($direction)
{
    q2(SQL_DEL_PREFERENCE_DIRECTION, array('id' => $direction));
    return true;
}

function get_interes_directions()
{
    $k = q(SQL_GET_PREFERENCE_DIRECTION,array('user_id'=>$_SESSION['user']['id']));
    return $k;
}

// Временые функции
function set_interes_directions($direction)
{
    if(q(SQL_CHECK_PREFERENCE_DIRECTION,array('user_id'=>$_SESSION['user']['id'],'country_id'=>$direction))==null)
    {
        q2(SQL_SET_PREFERENCE_DIRECTION, array('country_id' => $direction, 'user_id' => $_SESSION['user']['id']));
        return qInsertId();
    }

    return false;
}

// Временые функции
function set_poputchik($direction)
{
    if(!isset($direction))
    {
        buildMsg("Выберите фото!", 'danger');
        return false;
    }
    $u = getUser($_SESSION['user']['id']);
    if($u['balance']<150)
    {
        buildMsg("Недостаточно денег на счету! Пополните баланс!", 'danger');
        return false;
    }
    $b = $u['balance'] - 150;
    q2(SQL_USER_UPDATE_BALANCE,array('user_id'=>$_SESSION['user']['id'], 'balance'=>$b));

    q2(SQL_GET_PROMO_STRIP, array('photo_id' => $direction, 'user_id' => $_SESSION['user']['id']));

    //buildMsg("");

    return true;
}

// Временые функции
function set_interes_country($value)
{
    q2(SQL_UPDATE_PREFERENCE_COUNTRY, array('country_id' => $value, 'user_id' => $_SESSION['user']['id']));
    $k =  q(SQL_GET_COUNTRY_NAME,array('id'=>$value));
    return $k[0]['nm'];
}

function get_country_name($v)
{
    $k =  q(SQL_GET_COUNTRY_NAME,array('id'=>$v));
    return $k[0]['nm'];
}

// Временые функции
function set_interes_female($value)
{
    $u = get_preferences($value);

    if($u['sex_f']==1)
    {
        q2(SQL_UPDATE_PREFERENCE_SEX_F, array('sex_f' => 0, 'user_id' => $value));
        return false;
    }else if($u['sex_f']==0)
    {
        q2(SQL_UPDATE_PREFERENCE_SEX_F, array('sex_f' => 1, 'user_id' => $value));
        return true;
    }
}
// Временые функции
function set_interes_male($value)
{
    $u = get_preferences($value);

    if($u['sex_m']==1)
    {
        q2(SQL_UPDATE_PREFERENCE_SEX_M, array('sex_m' => 0, 'user_id' => $value));
        return false;
    }else if($u['sex_m']==0)
    {
        q2(SQL_UPDATE_PREFERENCE_SEX_M, array('sex_m' => 1, 'user_id' => $value));
        return true;
    }
}

// Временые функции
function set_host_status($value)
{
    $u = get_user_profile($value);
    if($u['response']['is_host']==1)
    {
        q2(SQL_USER_UPDATE_HOST, array('sponsor' => 0, 'user_id' => $value));
        return false;
    }else if($u['response']['is_host']==0)
    {
        q2(SQL_USER_UPDATE_HOST, array('sponsor' => 1, 'user_id' => $value));
        return true;
    }
}

function get_updates($type)
 { 
	return array('status'=>0);
			 
 } 

function get_photo_likes($photo_id)
 {
     $k = q(SQL_GET_PHOTO_LIKES,array('id'=>$photo_id));
     return $k[0];
			 
 } 

function get_video_likes($photo_id)
 { 
	return array('status'=>0);
			 
 } 

function get_status_likes($photo_id)
 { 
	return array('status'=>0);
			 
 } 

function get_country($starts_with=null)
 {
     $c = q(SQL_GET_COUNTRY.$starts_with, null);

     return $c;
 } 

function get_city($country,$starts_with = null)
 {
     $c = q(SQL_GET_CITY.$starts_with, array('country_id'=>$country));

     return json_encode($c);
 } 
 function get_city2($country,$starts_with = null)
 {
     $c = q(SQL_GET_CITY.$starts_with, array('country_id'=>$country));

     return $c;
 } 

function like_photo($photo_id)
 { 
	return array('status'=>0);
			 
 } 

function unlike_photo($photo_id)
 { 
	return array('status'=>0);
			 
 } 

function unlike_status($status_id)
 { 
	return array('status'=>0);
			 
 }
function like_status($status_id)
 {

	return 0;

 } 

function get_updates_count()
 { 
	return array('status'=>0);
			 
 } 

function mark_update($update_id)
 { 
	return array('status'=>0);
			 
 } 

function switch_on_vip($duration)
 { 
	return array('status'=>0);
			 
 } 

function get_all_dialogs()
 { 
	return array('status'=>0);
			 
 } 

function read_message($message_id)
 { 
	return array('status'=>0);
			 
 } 

function get_messages_with_user($user_id)
 { 
	return array('status'=>0);
			 
 } 

function delete_message($message_id)
 { 
	return array('status'=>0);
			 
 } 

function add_to_black_list($user_id)
 { 
	return array('status'=>0);
			 
 } 

function remove_from_black_list($userId)
 { 
	return array('status'=>0);
			 
 } 

function send_message($txt,$img,$user_id)
 { 
	return array('status'=>0);
			 
 } 

function remove_messages($user_id)
 { 
	return array('status'=>0);
			 
 } 

function travel($country,$phone)
 { 
	return array('status'=>0);
			 
 } 

function delete_updates($type)
 { 
	return array('status'=>0);
			 
 } 

function unlike_video($video_id)
 { 
	return array('status'=>0);
			 
 } 

function like_video($video_id)
 { 
	return array('status'=>0);
			 
 } 

function post_audio($audio)
 { 
	return array('status'=>0);
			 
 } 

function post_video($video)
 { 
	return array('status'=>0);
			 
 } 

function push_token($token)
 { 

    $tokens = q("SELECT * FROM push_tokens WHERE push_token = :tok", array("tok" => $push_token));

    if($tokens == null)
    {
        q2("INSERT INTO push_tokens(user_id, push_token) VALUES(:user_id, :tok)", array("user_id" => $_SESSION["user_id"], "tok" => $push_token));
    }

    return array("status" => 0); 
 } 

function delete_photo($photo_id)
 { 
	return array('status'=>0);
			 
 } 

function delete_video($video_id)
 { 
	return array('status'=>0);
			 
 } 

function delete_audio($audio_id)
 { 
	return array('status'=>0);
			 
 }

function get_users2($start=null,$limit=null,$order = null)
{
    $query ='';
    if(!is_null($start) && !is_null($limit)) $query = " LIMIT $start, $limit";

    $p = get_preferences($_SESSION['user']['id']);
    $order = " ORDER BY registration_dt DESC  ";
    //$users = q(SQL_GET_USERS2.$query.$order,array('sex_m'=>$p['sex_m'],'sex_f'=>$p['sex_f'],'age_s'=>$p['age_s'],'age_f'=>$p['age_f']
    //,'width_f'=>$p['width_f'],'width_s'=>$p['width_s'],'country_id'=>$p['country_id'],'city_id'=>$p['city_id']));//,'user_id'=>$_SESSION['user']['id']



    if($p['sex_m']==1) $sexm = " u.sex = 'm' ";
    if($p['sex_f']==1) $sexf = " u.sex = 'f' ";
    if(isset($sexm) && isset($sexf)) $or = $sexm." OR ".$sexf." AND";
    elseif(isset($sexm) && !isset($sexf)) $or = $sexm." AND";
    elseif(!isset($sexm) && isset($sexf)) $or = $sexf." AND";
    elseif(!isset($sexm) && !isset($sexf)) $or ="";
    $users = q("SELECT DISTINCT * FROM users u WHERE  ".$or."  TIMESTAMPDIFF(YEAR,u.birthday,CURDATE()) BETWEEN ".$p['age_f']." AND ".$p['age_s'].$order.$query,null);


    return $users;
}


function get_users($start=null,$limit=null,$order = null)
{//SELECT * FROM users ORDER BY registration_dt DESC LIMIT 5, 10 
    $order =" ORDER BY registration_dt DESC ";
    if(!is_null($start) && !is_null($limit)) $query = " LIMIT $start, $limit";
        $users = q(SQL_GET_USERS.$order.$query,null);
    return $users;
}


function get_users_poputchiki()
{//SELECT * FROM users ORDER BY registration_dt DESC LIMIT 5, 10 
	
    $users = q("SELECT * FROM users WHERE id IN (SELECT user_id FROM promo_strip)  ORDER BY registration_dt DESC ",null);

         
    return $users;
}


function doLogin($login,$pass,$rem)
{
    $user = q(SQL_LOGIN,array('login'=>$login,'pass'=>$pass));
    if($user != null)
    {
        $_SESSION['user'] = $user[0];
        if($rem == 'on')
           {
           	setcookie('user', $user[0], 999999);


           }
		   
		   q2(SQL_UPDATE_USER_LOGIN,array('login'=>$login,'pass'=>$pass));
		//build_msg('Вы успешно вошли в аккаунт');
        return $user[0];
    }
	
	buildMsg('Логин или пароль неверный', 'danger');
	
    return null;

}

function get_user_by_mail($mail) {
	$q = q("SELECT * FROM users WHERE lower(login) = lower(:login)", array('login' => $mail));
	return $q[0] != null ? $q[0] : false;
}

function editopassofuser($pass, $pass2, $oldpass){

    $old_pass = getUser($_SESSION['user']['id']);
    $bla = false;
    if(empty($oldpass)){
        buildMsg("Вы оставили пустым поле Старый пароль!", 'danger');
        $bla = true;
    }

    if(empty($pass)){
        buildMsg("Вы оставили пустым поле Новый пароль!", 'danger');
        $bla = true;
    }
    if(empty($pass2)){
        buildMsg("Вы оставили пустым поле Новый пароль еще раз!", 'danger');
        $bla = true;
    }

    if($pass != $pass2){
        buildMsg("Пароли должны совпадать!", 'danger');
        $bla = true;
    }
    $oldpass_one = md5($oldpass);

    if($oldpass_one != $old_pass['pass']){
        buildMsg("Старый пароль должен совпадать!", 'danger');
        $bla = true;
    }

    if($bla ==true) return false;

    if(q2(SQL_USER_UPDATE_PASS,array('pass'=>$pass, 'user_id'=>$_SESSION['user']['id']) )) {
        buildMsg('Пароль успешно изменен');
        return true;
    }   else return false;




}


function new_user($name,$date,$sex,$email, $pass, $pass2, $phone, $sec, $site = false){

    if(empty($email)){
        return array(
            'msg' => 'Вы оставили пустым поле Email',
            'color' => 'red',
			'type' => 'danger',
            'status' => 0
        );
    }

    if(empty($name)){
        return array(
            'msg' => 'Вы оставили пустым поле Имя',
            'color' => 'red',
			'type' => 'danger',
            'status' => 0
        );
    }

    if(empty($date)){
        return array(
            'msg' => 'Вы оставили пустым поле Дата рождения',
            'color' => 'red',
			'type' => 'danger',
            'status' => 0
        );
    }

    if(empty($sex) || (strpos($sex, "0") == TRUE)){
        return array(
            'msg' => 'Вы оставили пустым поле Пол',
            'color' => 'red',
			'type' => 'danger',
            'status' => 0
        );
    }

    if(empty($pass)){
        return array(
            'msg' => 'Вы оставили пустым поле Пароль',
            'color' => 'red',
			'type' => 'danger',
            'status' => 0
        );
    }

    if($pass != $pass2){
        return array(
            'msg' => 'Пароли должны совпадать',
            'color' => 'red',
			'type' => 'danger',
            'status' => 0
        );
    }
	
	if($site){
		$g = get_user_by_mail($email);
		if($g !== false){
			return array(
				'msg' => 'Такой E-mail уже используется',
				'color' => 'red',
				'type' => 'danger',
				'status' => 0
			);
		}
		
		if($_SESSION['secpic']!=strtolower($sec)){
			return array(
				'msg' => 'Неверный проверочный код',
				'color' => 'red',
				'type' => 'danger',
				'status' => 0
			);
		}
	}

    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        return array(
            'msg' => 'Введен некорректный Email',
			'type' => 'danger',
            'color' => 'red',
            'status' => 0
        );
    }

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
	$hash = md5(time()."$email");
	
    if(q2(SQL_NEW_USER, array('first_name'=>$name,'login' => $email,'sex'=>$sex, 'bd'=>$date, 'pass' => $pass, 'phone' => $phone, 'hash' => $hash)))
    {
        $user_id = qInsertId();
        q2(SQL_NEW_PREFERENCES, array('user_id' => $user_id));
		
		$url = getConfig('home')."?action=permit_signup&hash=".$hash;
		
		$message = <<<HTML
		<p>Здравствуйте!</p>
		<p>Вы зарегистрировались на сайте poputchiki.ru</p>
		<p>Для того, чтобы завершить регистрацию перейдите по данной ссылке</p>
		<p><a href="{$url}">{$url}</a></p>
		<p>Если вы не регистрировались на сайте и не знаете, что это за письмо, то просим Вас проигнорировать данное сообщение.</p>
HTML;
		
		send_email($email, 'Подтверждение регистрации на сайте poputchiki.ru', $message);
		
        return array(
            'msg' => 'Регистрация прошла успешно! На ваш e-mail отправлено письмо для активации вашего акаунта',
            'color' => 'green',
			'type' => 'success',
            'status' => 1
        );
    }

    return array(
        'msg' => 'Ошибка Регистрации!',
		'type' => 'danger',
        'color' => 'red',
        'status' => 0
    );

}

function getConfig($state){
	global $config;
	return $config[$state];
}

function createInteres($user_id)
{
    return q2(SQL_NEW_PREFERENCES, array('user_id' => $user_id));
}

function getStatuses($id=null,$start=null,$limit=null)
{
    $query ='';
    if(!is_null($start) && !is_null($limit)) $query = " LIMIT $start, $limit";
    if($id!=null)
        $k = q(SQL_GET_STAT,array('id'=>$id));
    else $k = q(SQL_GET_STATS.$query,null);
    if(!empty($k))
    {
        return $k;
    }
}
// временная
function getStatuses2($country,$sex,$city)
{
    $country = trim($country);
    $sex = trim($sex);
    $city = trim($city);
    if(!isset($sex) || empty($sex) || ($sex==null))
    {
        if(!isset($country) || empty($country) || ($country==null))
        {
            if(!isset($city) || empty($city) || ($city==null))
            {
                $k = q(SQL_GET_STATS,null);
            }else
            {
                $k = q(SQL_GET_STATS5,array('city'=>$city));
            }
        }else
        {
            if(!isset($city) || empty($city) || ($city==null))
            {
                $k = q(SQL_GET_STATS7,array('country'=>$country));
            }else
            {
                $k = q(SQL_GET_STATS6,array('city'=>$city,'country'=>$country));
            }
        }
    }else
    {
        if(!isset($country) || empty($country) || ($country==null))
        {
            if(!isset($city) || empty($city) || ($city==null))
            {
                $k = q(SQL_GET_STATS2,array('sex'=>$sex));
            }else
            {
                $k = q(SQL_GET_STATS3,array('sex'=>$sex,'city'=>$city));
            }
        }else
        {
            if(!isset($city) || empty($city) || ($city==null))
            {
                $k = q(SQL_GET_STATS8,array('sex'=>$sex,'country'=>$country));
            }else
            {
                $k = q(SQL_GET_STATS4,array('sex'=>$sex,'city'=>$city,'country'=>$country));
            }
        }
    }
    if(!empty($k))
    {
        return $k;
    }
}

function buildMsg($text, $class = "success")
{
    $_SESSION['messages'][] = array('text' => $text, 'class' => $class);
}


function getMessages()
{
    $messages = '';
    if(isset($_SESSION['messages']) && is_array($_SESSION['messages'])){
        foreach($_SESSION['messages'] as $msg){
            $messages .= "<div class=\"alert alert-{$msg['class']} alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>{$msg['text']}</div>";
        }
        unset($_SESSION['messages']);
    }
    return $messages;
}

function doEditUser($post)
{
    include 'WideImage/WideImage.php';
    global $config;
	
	if($_SESSION['user']['id'] != $post['user_id']){
		buildMsg('Некрасиво поступаешь', 'danger');
		return false;
	}
	
    $fillable = array(
        'last_name' => 'Фамилия',
        'first_name' => 'Имя',
        'email' => 'e-mail',
        'phone' => 'Телефон',
        'birthday' => 'Дата рождения',
        'sex' => 'Пол',
        'about' => 'О себе',
        'city' => 'Город',
        'avatar_id'=>'Вы не загрузили или не выбрали фото',
		'growth' => 'Рост',
		'weight'  =>  'Вес'
    );
    $data = array();

    //$err = array();
    //foreach($fillable as $k=>$v)  if(empty($post[$k])) $err[] = 'Введите поле: '.$v;

    /*
    if(count($err) > 0){
        foreach($err as $e) buildMsg($e, 'danger');
        return false;
    }else{

        'country' =>'Страна',
    */
        $data['country'] = get_country_name($post['country']);
        $data['user_id'] = $post['user_id'];

        foreach($fillable as $k=>$v) $data[$k] = $post[$k];

        if(q2(SQL_USER_UPDATE, $data)) {
            buildMsg('Изменения успешно сохранены');
            return $data;
        }   else return $data;
    //}
}


/**
 * функций связанные с изображениями
 */

function getImageThumb($image, $width = 150, $height = 150)
{
    global $config;
    include_once 'WideImage/WideImage.php';

    $original = $config['imagesUpload'].$image;
    $tDirName = $width.'x'.$height;
    $tDir = $config['thumbsDir'].$tDirName.DS;
    $tFileName = $tDir.$image;

    if(file_exists($tFileName)) return $config['home'].$config['thumbs'].$tDirName.DS.$image;

    if(file_exists($original)){
        $size = getimagesize($original);

        if($size[0] < $width && $size[1] < $height) return $config['home'].$config['images'].$image;

        if(!file_exists($tDir)) mkdir($tDir, 0777);

        $images = WideImage::loadFromFile($original)->resize($width+100)->crop("top", "left", $width, $height);
        $images->saveToFile($tFileName);

        return $config['home'].$config['thumbs'].$tDirName.DS.$image;
    }else return 'http://placehold.it/'.$tDirName;
}

function getImage($id){
    $i = q(SQL_GET_IMAGE, array('id' => $id));
    return $i[0];
}

function deleteImage($id){
    global $config;

    $image = q("SELECT * FROM photos WHERE id = :id AND user_id = :uid", array('id' => $id, 'uid' => $_SESSION['user']['id']));
	$image = $image[0];
	
	if($image == null){
		buildMsg('Можно удалять только свои изображения', 'danger');
		return false;
	}
	
    if(q2(SQL_DELETE_IMAGE, array('id' => $id))){

        unlink($image['url']); // Удаляет изображение
        unlink($image['thumb_url']);	// Удаляет фотографию

        //search_file($config['thumbsDir2'], $image['filename']);

        //foreach(glob($config['thumbsDir'].'*/'.$image['filename']) as $file){
          //  unlink($file);
        //}

        buildMsg("Фото удалено!");
        return true;
    }else {
		buildMsg('Произошлка ошибка', 'danger');
		return false;
	}
}

function uploadImage($file, $avatar = array('using' => false, 'type' => 'user', 'target' => 0), $show = 1)
{
    global $config;
    include_once 'WideImage/WideImage.php';

    // Проверка на изображение
    $type = explode('.', $file['name']);
    $types = array('gif', 'png', 'jpg', 'jpeg');
    if(!in_array(end($type), $types))
    {

        buildMsg("Не поддерживаемый формат".$file['name'], 'danger');
        return false;
    }

    

    $imageName = $config['imagesUpload'].time().totranslit(basename($file['name']));			 // Название изображения
    $imageName2 = $config['thumbsDir'].time().totranslit(basename($file['name']));			 // Название изображения
    $imageDir  = $imageName;		 // Полная картина путь
    $imageDir2  = $imageName2;


    $exif = exif_read_data($imageName);
	$orientation = $exif['Orientation'];
	switch ($orientation) {
	   case 3:
	      $image_p = imagerotate($imageName, 180, 0);
	      break;
	   case 6:
	      $image_p = imagerotate($imageName, -90, 0);
	      break;
	   case 8:
	      $image_p = imagerotate($imageName, 90, 0);
	      break;
	}
	imagejpeg($image_p, $imageName, 100);

	$exif = exif_read_data($imageName2);
	$orientation = $exif['Orientation'];
	switch ($orientation) {
	   case 3:
	      $image_p = imagerotate($imageName2, 180, 0);
	      break;
	   case 6:
	      $image_p = imagerotate($imageName2, -90, 0);
	      break;
	   case 8:
	      $image_p = imagerotate($imageName2, 90, 0);
	      break;
	}
	imagejpeg($image_p, $imageName2, 100);



    $fullImage = WideImage::loadFromFile($file['tmp_name']); // Полная картинка
  //  $fullImage2 = WideImage::loadFromFile($file['tmp_name']); // Полная картинка

    $fullImage->saveToFile($imageDir);

//    $fullImage2->saveToFile($imageDir2);

    WideImage::loadFromFile($file['tmp_name'])->resize('150', '150')->crop('center', 'center', 100, 100)->saveToFile($imageDir2);

	$tar = $avatar['using'] ? $avatar['target'] : $_SESSION['user']['id'];
	
    if(q2(SQL_ADD_NEW_IMAGE, array('url' => $imageName,'thumb_url'=>$imageDir2, 'user_id' => $tar))){

        $imageId = qInsertId();

        if($avatar['using'] == true){ // Если нужно обновить аватар
            switch($avatar['type']){
                case "user":
                    setUserImage($avatar['target'], $imageId);
                    break;
            }
        }
        {
            if($show) buildMsg("Фотографии загружены!");
            return array('id' => $imageId, 'name' => $imageName);
        }
    }   else
    {
        if($show) buildMsg("Ошибка в базе данных", 'danger');
        return false;
    }

}

function upload_image_by_url($img, $user_id){
	global $config;
	
    $ch = curl_init($img);
    $name = md5(time()).'.jpg';
    $fp = fopen($_SERVER['DOCUMENT_ROOT'].'/photos/'.$name, 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);

	$url = $config['images'].$name;
	
    q2("INSERT INTO photos(url, user_id, dt) VALUES(:url, :user_id, :dt)", array('url' => $url, 'user_id' => $user_id, 'dt' => date('Y-m-d H:i:s')));
    $id = qInsertId();

    return array('id' => $id, 'name' => $name);
}

function setUserImage($good_id, $image_id){
    if(q2(SQL_UPDATE_USER_IMAGE, array('user_id' => $good_id, 'image_id' => $image_id))) return true;
    else return false;
}

function getUserImages($id)
{
    $images = q(SQL_GET_USER_IMAGES, array('user_id' => $id));
    return $images;
}

function totranslit($var, $lower = true, $punkt = true) {
    global $langtranslit;

    if ( is_array($var) ) return "";

    if (!is_array ( $langtranslit ) OR !count( $langtranslit ) ) {

        $langtranslit = array(
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
            "ї" => "yi", "є" => "ye",

            'А' => 'A', 'Б' => 'B', 'В' => 'V',
            'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
            'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
            'И' => 'I', 'Й' => 'Y', 'К' => 'K',
            'Л' => 'L', 'М' => 'M', 'Н' => 'N',
            'О' => 'O', 'П' => 'P', 'Р' => 'R',
            'С' => 'S', 'Т' => 'T', 'У' => 'U',
            'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
            'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
            'Ь' => '', 'Ы' => 'Y', 'Ъ' => '',
            'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
            "Ї" => "yi", "Є" => "ye",
        );

    }

    $var = trim( strip_tags( $var ) );
    $var = preg_replace( "/\s+/ms", "-", $var );

    $var = strtr($var, $langtranslit);

    if ( $punkt ) $var = preg_replace( "/[^a-z0-9\_\-.]+/mi", "", $var );
    else $var = preg_replace( "/[^a-z0-9\_\-]+/mi", "", $var );

    $var = preg_replace( '#[\-]+#i', '-', $var );

    if ( $lower ) $var = strtolower( $var );

    $var = str_ireplace( ".php", "", $var );
    $var = str_ireplace( ".php", ".ppp", $var );

    if( strlen( $var ) > 200 ) {

        $var = substr( $var, 0, 200 );

        if( ($temp_max = strrpos( $var, '-' )) ) $var = substr( $var, 0, $temp_max );

    }

    return $var;
}

function enc($str)
{
    return htmlspecialchars(trim(addslashes(strip_tags($str))));
}


?>