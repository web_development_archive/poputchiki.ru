<?php
$dbConnection = new PDO('mysql:dbname=poputchiki;host=localhost;charset=utf8', 'poputchiki', 'travel1777Asia');

$dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbConnection->exec("set names utf8");


function debugQ($query, $params) {
    $keys = array();

    # build a regular expression for each parameter
    foreach ($params as $key => $value) {
        if (is_string($key)) {
            $keys[] = '/:'.$key.'/';
        } else {
            $keys[] = '/[?]/';
        }
    }

    $query = preg_replace($keys, $params, $query, 1, $count);

    #trigger_error('replaced '.$count.' keys');

    return $query;
}

function q($sql, $params) // запрос к базе — короткое имя для удобства
{
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);
    $stmt->execute($params);
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}

function qRows(){ // Выводит кол-во строк другим способом
    global $dbConnection;
    $stmt = $dbConnection->query('SELECT FOUND_ROWS() as num');
    return $stmt->fetchColumn(0);
}


function q2($sql, $params){ // Используется для insert и update
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);

    if($stmt->execute($params)) return true;
    else return false;
}

function qCount($sql, $params){ // Выводит количество записей
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);
    $stmt->execute($params);
    return $stmt->fetchColumn();
}

function qInsertId(){ // Последнйи автоинкриментный ID
    global $dbConnection;
    return $dbConnection->lastInsertId();
}



// FAV
define("SQL_SET_FAV","INSERT INTO favorites(user_id, fav_id) VALUES (:user_id,:fav_id)");
define("SQL_DEL_FAV","DELETE FROM favorites WHERE user_id=:user_id AND fav_id=:fav_id");
define("SQL_GET_FAV","SELECT * FROM favorites WHERE user_id=:user_id AND fav_id=:fav_id");
// LIKES
define("SQL_SET_LIKES","INSERT INTO status_likes(satus_id, user_id) VALUES (:fav_id, :user_id)");
define("SQL_DEL_LIKES","DELETE FROM status_likes WHERE user_id=:user_id AND satus_id=:fav_id");
define("SQL_GET_LIKES","SELECT * FROM status_likes WHERE user_id=:user_id AND satus_id=:fav_id");

// Visitors
define("SQL_SET_VISITOR","INSERT INTO visitors(user_id, guest_id) VALUES (:user_id,:guest_id)");
define("SQL_GET_VISITOR","SELECT * FROM visitors WHERE user_id=:user_id AND guest_id=:guest_id");


// Status
define("SQL_GET_STATUS_OF_USER","SELECT * from statuses where user_id = :id");
define("SQL_SET_STATUS","INSERT INTO statuses(id, user_id, status_text, dt) VALUES ('',:user_id,:status_text,:dt)");
define("SQL_UPDATE_STATUS","UPDATE statuses SET status_text=:text,dt=:datet WHERE id = :id");


// preferences
define("SQL_GET_PREFERENCE_OF_USER","SELECT * from user_preferences where user_id = :id");
define("SQL_SET_PREFERENCES","INSERT INTO user_preferences(id, user_id, status_text, dt) VALUES ('',:user_id,:status_text,:dt)");

define("SQL_UPDATE_PREFERENCE_SEX_M","UPDATE user_preferences SET sex_m=:sex_m WHERE user_id = :user_id");
define("SQL_UPDATE_PREFERENCE_SEX_F","UPDATE user_preferences SET sex_f=:sex_f WHERE user_id = :user_id");
define("SQL_UPDATE_PREFERENCE_AGE","UPDATE user_preferences SET age_f=:age_f, age_s=:age_s  WHERE user_id = :user_id");
define("SQL_UPDATE_PREFERENCE_WIDTH","UPDATE user_preferences SET width_s=:width_s, width_f=:width_f WHERE user_id = :user_id");
define("SQL_UPDATE_PREFERENCE_COUNTRY","UPDATE user_preferences SET country_id=:country_id WHERE user_id = :user_id");
define("SQL_UPDATE_PREFERENCE_CITY","UPDATE user_preferences SET city_id=:city_id WHERE user_id = :user_id");
define("SQL_SET_PREFERENCE_DIRECTION","INSERT INTO destinations(user_id, country_id) VALUES (:user_id,:country_id)");
define("SQL_CHECK_PREFERENCE_DIRECTION","SELECT DISTINCT * FROM destinations WHERE user_id=:user_id AND country_id=:country_id");
define("SQL_GET_PREFERENCE_DIRECTION","SELECT DISTINCT * FROM destinations WHERE user_id=:user_id");
define("SQL_DEL_PREFERENCE_DIRECTION","DELETE FROM destinations WHERE id=:id");




// COUNTRY CITY
define("SQL_GET_COUNTRY", "SELECT * FROM countries");
define("SQL_GET_COUNTRY_NAME", "SELECT nm FROM countries WHERE id = :id");
define("SQL_GET_CITY_NAME", "SELECT nm FROM cities WHERE id = :id");
define("SQL_GET_CITY", "SELECT * FROM cities WHERE country_id = :country_id");

define("SQL_CHECK_USER", "SELECT * FROM users WHERE login = :login");
define("UPDATE_PASS", "UPDATE users SET pass = :pass WHERE id = :id");
define("SQL_NEW_USER","INSERT INTO users(login,phone,sex,pass,first_name,birthday,country,hash) VALUES (:login,:phone,:sex,MD5(:pass),:first_name,:bd,'Россия', :hash)");
define("SQL_NEW_PREFERENCES","INSERT INTO `user_preferences`(`user_id`) VALUES (:user_id)");
define("SQL_GET_PREFERENCES","SELECT * FROM user_preferences WHERE user_id=:user_id");

define("SQL_USER_UPDATE","UPDATE users SET avatar_id=:avatar_id, city=:city, country=:country, login=:email,phone=:phone,sex=:sex,first_name=:first_name,last_name=:last_name,birthday=:birthday,about=:about,growth=:growth,weight=:weight WHERE id=:user_id");

define("SQL_USER_UPDATE_PASS","UPDATE users SET pass=MD5(:pass) WHERE id=:user_id");

define("SQL_USER_UPDATE_SPONSOR","UPDATE users SET is_sponsor=:sponsor WHERE id=:user_id");
define("SQL_USER_UPDATE_HOST","UPDATE users SET is_host=:sponsor WHERE id=:user_id");
define("SQL_USER_UPDATE_BALANCE","UPDATE users SET balance=:balance WHERE id=:user_id");
define("SQL_USER_UPDATE_INVISIBLE","UPDATE users SET invisible=:sponsor WHERE id=:user_id");

define("SQL_GET_USER_BY_LOGIN", "SELECT * FROM users WHERE login = :login");
define("SQL_LOGIN", "SELECT * FROM users WHERE login = :login AND pass =MD5(:pass) AND active = 1");
define("SQL_UPDATE_USER_LOGIN", "UPDATE users SET lastlogin_dt =(CURRENT_TIMESTAMP) WHERE login = :login AND pass =MD5(:pass) AND active = 1");
define("SQL_UPDATE_USER", "UPDATE users SET name = :name,  phone = :phone,  pass = MD5(:pass), login = :login WHERE id = :id");
define("SQL_GET_USER", "SELECT u.*, u.id AS 'user_id', p.url as ava FROM users u left join photos p on (p.id=u.avatar_id) WHERE u.id = :id");
define("SQL_GET_USERS", "SELECT * FROM users");
//define("SQL_GET_USERS2", "SELECT DISTINCT * FROM users u, user_preferences p WHERE u.id=p.user_id AND (sex_m=:sex_m OR sex_f=:sex_f OR age_s=:age_s OR age_f=:age_f OR width_f=:width_f OR width_s=:width_s OR country_id=:country_id OR city_id=:city_id OR width_f=:width_f OR width_s=:width_s OR country_id=:country_id OR city_id=:city_id)");
define("SQL_GET_USERS2", "SELECT DISTINCT * FROM users u, user_preferences p WHERE u.id=p.user_id AND (sex_m=:sex_m OR sex_f=:sex_f OR age_s=:age_s OR age_f=:age_f)");


// OR ((SELECT country_id FROM destinations WHERE user_id=:user_id) IN (SELECT country_id FROM destinations WHERE user_id=p.user_id))

// STATS



// FAV
define("SQL_SET_STAT1","INSERT INTO statuses(satus_id,user_id) VALUES (:satus_id,:user_id)");
define("SQL_DEL_STAT1","DELETE FROM statuses WHERE user_id=:user_id AND satus_id=:satus_id");
define("SQL_GET_STAT1","SELECT * FROM statuses WHERE user_id=:user_id AND satus_id=:satus_id");

define("SQL_GET_STAT", "SELECT * FROM statuses WHERE user_id = :id");
define("SQL_GET_STATS", "SELECT DISTINCT s.dt, s.id AS stat_id, s.status_text, u.first_name, u.id, u.avatar_id, u.birthday FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND u.avatar_id>0 ORDER BY s.dt DESC");
define("SQL_GET_STATS2", "SELECT DISTINCT s.id AS stat_id, s.status_text, u.first_name, u.id, u.avatar_id, u.birthday FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND u.sex =:sex");
define("SQL_GET_STATS3", "SELECT DISTINCT s.id AS stat_id, s.status_text, u.first_name, u.id, u.avatar_id, u.birthday FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND u.sex =:sex AND u.city =:city");
define("SQL_GET_STATS5", "SELECT DISTINCT s.id AS stat_id, s.status_text, u.first_name, u.id, u.avatar_id, u.birthday FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND u.city=:city");
define("SQL_GET_STATS4", "SELECT DISTINCT s.id AS stat_id, s.status_text, u.first_name, u.id, u.avatar_id, u.birthday FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND u.sex =:sex AND u.city=:city AND u.country=:country");
define("SQL_GET_STATS6", "SELECT DISTINCT s.id AS stat_id, s.status_text, u.first_name, u.id, u.avatar_id, u.birthday FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND u.city =:city AND u.country =:country");
define("SQL_GET_STATS7", "SELECT DISTINCT s.id AS stat_id, s.status_text, u.first_name, u.id, u.avatar_id, u.birthday FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND u.country=:country");
define("SQL_GET_STATS8", "SELECT DISTINCT s.id AS stat_id, s.status_text, u.first_name, u.id, u.avatar_id, u.birthday FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND u.sex =:sex AND u.country =:country");
define("SQL_GET_STATS_LIKES", "SELECT case when COUNT(l.dt) =null then 0 else COUNT(l.dt) end AS likes FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND l.user_id = u.id AND l.satus_id = :id");
define("SQL_GET_STATS_FAV", "SELECT DISTINCT u . * FROM statuses s, users u, favorites f WHERE f.fav_id = u.id AND f.user_id =:user_id");
define("SQL_GET_STATS_FAV_RAND", "SELECT DISTINCT u . * FROM statuses s, users u, favorites f WHERE u.sex='f' AND u.avatar_id>0 order by rand() LIMIT 2");
define("SQL_GET_STATS_FAV_RAND_M", "SELECT DISTINCT u . * FROM statuses s, users u, favorites f WHERE u.sex='m' AND u.avatar_id>0 order by rand() LIMIT 2");
define("SQL_GET_STATS_GUEST", "SELECT DISTINCT u.id,u.first_name,u.birthday, v.dt, u.avatar_id  FROM statuses s, users u, visitors v WHERE v.guest_id= u.id AND v.user_id =:user_id ORDER BY v.dt DESC");

define("SQL_GET_STATS_LIKE", "SELECT s.status_text, u.first_name, u.avatar_id, u.birthday, case when COUNT(l.dt) =null then 0 else COUNT(l.dt) end AS likes FROM statuses s, users u, status_likes l WHERE u.id = s.user_id AND l.user_id = u.id AND l.satus_id = s.id AND s.status_text LIKE '%:this%'
");


//     images

define("SQL_UPDATE_USER_IMAGE", "UPDATE users SET avatar_id = :image_id WHERE id = :user_id");
define("SQL_ADD_NEW_IMAGE", "INSERT INTO photos(url,thumb_url,user_id) VALUES(:url, :thumb_url, :user_id)");
define("SQL_GET_IMAGE", "SELECT * FROM photos WHERE id = :id");
define("SQL_DELETE_IMAGE", "DELETE FROM photos WHERE id = :id");
define("SQL_GET_USER_IMAGES", "SELECT * FROM photos WHERE user_id = :user_id");
define("SQL_SET_LIKE_PHOTO","INSERT INTO photo_likes(photo_id,user_id) VALUES (:photo_id,:user_id)");
define("SQL_DEL_LIKE_PHOTO","DELETE FROM photo_likes WHERE user_id=:user_id AND photo_id=:photo_id");
define("SQL_GET_LIKE_PHOTO","SELECT * FROM photo_likes WHERE user_id=:user_id AND photo_id=:photo_id");
define("SQL_GET_PHOTO_LIKES", "SELECT case when COUNT(l.dt) =null then 0 else COUNT(l.dt) end AS likes FROM statuses s, users u, photo_likes l WHERE u.id = s.user_id AND l.user_id = u.id AND l.photo_id = :id");


define("SQL_GET_PROMO_STRIP","INSERT INTO promo_strip(user_id,photo_id) VALUES (:user_id,:photo_id)");

//    gifts
define("SQL_GET_ALL_GIFTS","SELECT * FROM gifts");
define("SQL_GET_GIFT","SELECT * FROM gifts WHERE id=:id");
define("SQL_GET_USER_GIFTS","SELECT * FROM gifts g, gift_instances s WHERE s.user_response=:user_id AND s.girft_id=g.id");
define("SQL_SET_GIFT","INSERT INTO gift_instances(girft_id, user_request, user_response) VALUES (:gift_id,:user_request,:user_response)");

?>