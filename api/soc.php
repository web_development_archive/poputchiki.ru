<?php


$VK_APP_ID = "4899076";
$VK_SECRET_CODE = "A2OX9xTGAM4sjkSXSAnm";

$FB_APP_ID = "896978000343513";
$FB_SECRET_CODE = "b7fd10655623b14a38eb66bcdf87b57e";
	
function vklogin_site($code, $page){

    sess_destroy();
    global $VK_APP_ID, $VK_SECRET_CODE;

    if(empty($code))
        return array
        (
            'status' => 2,
            'title' => 'Ошибка',
            'message' => 'Ошибка авторизации'
        );

    $vk_grand_url = "https://api.vk.com/oauth/access_token?client_id=".$VK_APP_ID."&client_secret=".$VK_SECRET_CODE."&code=".$code."&redirect_uri={$page}";
	//$resp = file_get_contents($vk_grand_url);
    
	$curl_handle=curl_init();
	curl_setopt($curl_handle, CURLOPT_URL, urldecode($vk_grand_url));
	curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Chrome 4.0');
	$resp = curl_exec($curl_handle);
	curl_close($curl_handle);
	
	$data = json_decode($resp, true);
	
    $vk_access_token = $data['access_token'];
    if(empty($data['user_id']))
        return array
        (
            'status' => 1,
            'title' => 'Ошибка',
            'message' => 'Ошибка авторизации'
        );

    $vk_uid = $data['user_id'];
	
    $user = q("SELECT *, 1 as exist FROM users where vk = :vk", array('vk' => $vk_uid));
	
    if($user[0]['exist']==1) {
        return vk_user_exists_site($user[0]);
    } else {

        $res = @file_get_contents("https://api.vk.com/method/users.get?uids=".$vk_uid."&access_token=".$vk_access_token."&fields=uid,first_name,last_name,bdate,screen_name,photo_max,about");
        $data = json_decode($res, true);
        $user_info = $data['response'][0];

        $err = vk_new_user_site(
			$user_info['first_name'],
			$user_info['last_name'],
			$user_info['about'],
			md5(date('Y-m-d H:i:s') . "!BUMERANG!"), 
			"user_{$vk_uid}@vk.com",
			$user_info['photo_max'],
			$vk_uid);
        
        $user = q("SELECT *, 1 as exist FROM users where vk=:vk", array('vk' => $vk_uid));

        if($user[0]['exist']==1) return vk_user_exists_site($user[0]);
        else return $err;
    }
}

function vk_new_user_site($first_name, $last_name, $about, $pass, $login, $photo_max, $vk_uid) {

        $pass=md5($pass);

        $first_name = ucfirst(strtolower($first_name));
		$last_name = ucfirst(strtolower($last_name));

        $existing = q("SELECT *, 1 as exist FROM users WHERE login = ?", array($login));
		
        if($existing[0]['exist']==1)
        {
            return array
            (
                'status' => 1,
                'action' => 'new_user',
                'title' => 'Ошибка',
                'message' => 'Логин или электронная почта уже заняты'
            );
        }

        if($login=='' || $pass=='' || !isset($login) || !isset($pass) )
        {
            return array
            (
                'status' => 2,
                'action' => 'new_user',
                'title' => 'Ошибка',
                'message' => 'Пожалуйста, заполните все регистрационные поля'
            );
        }

        //$image = upload_image_by_url($img);
        //$image = $image['id'];
		
				
        q2("INSERT INTO users (first_name, last_name, login, pass, about, vk, registration_dt) VALUES(:first_name, :last_name, :login, :pass, :about, :vk, :dt)", array(
			'first_name' => $first_name,
			'last_name' => $last_name,
			'login' => $login,
			'pass' => $login,
			'about' => $about,
			'vk' => $vk_uid,
			'dt' => date('Y-m-d H:i:s')
		));
		
		$user_id = qInsertId();
		$image = uploadImage(array(
			'tmp_name' => $photo_max,
			'name' => basename($photo_max)
		), array('using' => true, 'type' => 'user', 'target' => $user_id), 0);

        return array
        (
            'status' => 0,
            'action' => 'vk_new_user',
            'title' => 'Готово',
            "token"=>session_id(),
            'message' => "Вы вошли на сайт через VK"
        );

}

function vk_user_exists_site($user) {
		
		session_start();
        $_REQUEST['token'] = session_id();
        $_SESSION['user'] = getUser($user['id']);
        $_SESSION['user_id'] = $user['id'];

        //setcookie('token', session_id(), time()+60*60*24*30, '/');
        return array
        (
            'status' => 0,
            'action' => 'name',
            'token' => 	session_id(),
            'title' => 'Отлично',
            'user' => $_SESSION['user'],
            'message' => 'Логин и пароль подошли'
        );

}

function fblogin_site($code, $page){
    sess_destroy();
    
	global $FB_APP_ID, $FB_SECRET_CODE;
	
    if(empty($code))
        return array
        (
            'status' => 2,
            'title' => 'Ошибка',
            'message' => 'Ошибка авторизации'
        );

    $fb_grand_url = "https://graph.facebook.com/oauth/access_token?client_id=".$FB_APP_ID."&client_secret=".$FB_SECRET_CODE."&code=".$code."&redirect_uri={$page}";
    //die($vk_grand_url);
    //die(header("Location: ".$fb_grand_url));

    //$resp = @file_get_contents($fb_grand_url);

	$curl_handle=curl_init();
	curl_setopt($curl_handle, CURLOPT_URL, urldecode($fb_grand_url));
	curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Chrome 4.0');
	$resp = curl_exec($curl_handle);
	curl_close($curl_handle);
	
    $prms = explode("&", $resp);
    foreach ($prms as $prm) {
        $arr  = explode("=",$prm);
        $data[$arr[0]] = $arr[1];
    }
		
    $fk_access_token = $data['access_token'];
	
	$curl_handle=curl_init();
	curl_setopt($curl_handle, CURLOPT_URL, urldecode("https://graph.facebook.com/me?access_token=" . $fk_access_token));
	curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Chrome 4.0');
	$resp = curl_exec($curl_handle);
	curl_close($curl_handle);
	
    //$resp = @file_get_contents("https://graph.facebook.com/me?access_token=" . $fk_access_token);

    $data = json_decode($resp);
	
	$curl_handle=curl_init();
	curl_setopt($curl_handle, CURLOPT_URL, urldecode("https://graph.facebook.com/me/picture?redirect=0&height=200&type=normal&width=200&access_token=" . $fk_access_token));
	curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Chrome 4.0');
	$resp = curl_exec($curl_handle);
	curl_close($curl_handle);
	
    //$resp = @file_get_contents("https://graph.facebook.com/me/picture?redirect=0&height=200&type=normal&width=200&access_token=" . $fk_access_token);
    $pic_data = json_decode($resp);
	
    if(empty($data->id))
    {
        return array
        (
            'status' => 1,
            'title' => 'Ошибка',
            'message' => 'Ошибка авторизации'
        );
    }

    $fb_uid = $data->id;

    //die($fb_uid);
    $user = q("SELECT *, 1 as exist FROM users where fb = ?", array($fb_uid));

    //die(print_r($user));

    if($user[0]['exist']==1){
        return fb_user_exists_site($user[0]);
    }else{
        $err = fb_new_user_site($data->first_name,
								$data->last_name,
								$data->gender,
								md5(date('Y-m-d H:i:s') . "!BUMERANG!"), 
								"fb_".$fb_uid."@poputchiki.ru", 
								$pic_data->data->url, 
								$fb_uid . "");//!!@@##
//  			 		$user = q("SELECT *, 1 as exist FROM users where 1=1",null);

        $user = q("SELECT *, 1 as exist FROM users where fb = ?" , array($fb_uid));

        if($user[0]['exist']==1) return fb_user_exists_site($user[0]);
        else return $err;

    }
}

function fb_user_exists_site($user){
	session_start();
    $_REQUEST['token'] = session_id();
    $_SESSION['user'] = getUser($user['id']);
    $_SESSION['user_id'] = $user['id'];


    //setcookie('token', session_id(), time()+60*60*24*30, '/');
    return array
    (
        'status' => 0,
        'action' => 'name',
        'token' => 	session_id(),
        'title' => 'Отлично',
        'user' => $user,
        'message' => 'Логин и пароль подошли'
    );
}

function fb_new_user_site($first_name, $last_name, $gender, $pass, $email, $photo, $uid) {

    $pass=md5($pass);

    $first_name = ucfirst(strtolower($first_name));
	$last_name  = ucfirst(strtolower($last_name));
	$gender = strtolower($gender);

    $existing = q("SELECT *, 1 as exist FROM users WHERE login = ?", array($email));
    //return $existing;
    if($existing[0]['exist']==1)
    {
        //return fb_user_exists($existing[0]);

		q2("UPDATE users SET fb = :fb WHERE id=:id", array('fb'=>$uid, 'id'=>$existing[0]['id']));
		return array
		(
			'status' => 0,
			'action' => 'fb_new_user',
			'title' => 'Готово',
			"token"=>session_id(),
			'message' => "Вы вошли на сайт через Facebook"
		);		
	}

    if($email=='' || $pass=='' || !isset($email) || !isset($pass) )
    {
        return array
        (
            'status' => 2,
            'action' => 'new_user',
            'title' => 'Ошибка',
            'message' => 'Пожалуйста, заполните все регистрационные поля'
        );
    }

    //$image = upload_image_by_url($img);
    //$image = $image['id'];

    $id = q2("INSERT INTO users(first_name, last_name, login, pass, fb, sex) VALUES(:first_name, :last_name, :login, :pass, :fb, :sex)", array(
		'first_name' => $first_name,
		'last_name' => $last_name,
		'login' => $email,
		'pass' => $pass,
		'fb' => $uid,
		'sex' => substr($gender, 0, 1)
	));
	
	$user_id = qInsertId();
	
	$image = upload_image_by_url($photo, $user_id);
	q2("UPDATE users SET avatar_id = :ava WHERE id = :id", array('id' => $user_id, 'ava' => $image['id']));

    return array
    (
        'status' => 0,
        'action' => 'fb_new_user',
        'title' => 'Готово',
        "token"=>session_id(),
        'message' => "Вы вошли на сайт через Facebook"
    );

}

