<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 08.03.2015
 * Time: 0:54
 */

function viewConfirmPurchase($purchase_id) {
	
	$mrh_login = "poputchiki.ru";
	$mrh_pass1 = "ppp000ttt";
	$inv_id = 0;
	$inv_desc = "ROBOKASSA Advanced User Guide";
	$out_summ = "8.96";
	$shp_item = 1;
	$in_curr = "";
	$culture = "ru";
	$encoding = "utf-8";
	$purchase = get_purchase($purchase_id);
	
	q2("INSERT INTO purchase_instances(user_id, purchase_item_id, dt) VALUES(:id, :item_id, :dt)", array(
		'id' => $_SESSION['user']['id'],
		'item_id' => $purchase_id,
		'dt' => date('Y-m-d H:i:s')
	));
	$inv_desc = $purchase['nm'];	
	$out_summ = number_format($purchase['price'],2, '.','');
	$inv_id = qInsertId();
	
	$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1");
	
	$out = '
		<h3>Покупка монет</h3>
		<p>Вы хотите пополнить счет на '.$purchase['nm'].', и сейчас будете перенаправлены на сайт Robokassa.</p>
		<form action=\'https://auth.robokassa.ru/Merchant/Index.aspx\' method=POST target=_blank>
			<input type=hidden name=MrchLogin value='.$mrh_login.'>
			<input type=hidden name=OutSum value='.$out_summ.'>
			<input type=hidden name=InvId value='.$inv_id.'>
			<input type=hidden name=Desc value='.$inv_desc.'>
			<input type=hidden name=SignatureValue value='.$crc.'>
			<a href="#" id="toback" class="btn btn-danger">Назад</a>
			<input type=submit value=\'Купить\' class="btn btn-success">
		</form>
	';
	
	return $out;
}
 
function viewUserGuests($s)
{
    GLOBAL $config;
    $stat='';



    foreach($s as $k=>$v)
    {
        if(isset($v['avatar_id']) && $v['avatar_id']!=null) {
            $av = getImage($v['avatar_id']);
            $avtr = $av['url'];
            if(isset($av['thumb_url']) && $av['thumb_url']!=null)
                $avtr = $av['thumb_url'];
        }else continue;//$avtr = $config['defaultAvatarMini'];

        $date = new DateTime($v['dt']);

        $monthes = array(
            '01' => 'Января', '02' => 'Февраля', '03' => 'Марта', '04' => 'Апреля',
            '05' => 'Мая', '06' => 'Июня', '07' => 'Июля', '08' => 'Августа',
            '09' => 'Сентября', '10' => 'Октября', '11' => 'Ноября', '12' => 'Декабря'
        );
        $agef = ($date->format('d') .' '. $monthes[($date->format('m'))] .' в '. $date->format('H:i'));

        $name = $v['first_name']==""?"Без имени":$v['first_name'];
        $stat.=<<<HTML
            <div class="statuses_1" style="    width: 20%;">
                <a href="{$config['home']}?action=viewprofile&id={$v['id']}">
                <img src="{$avtr}" alt>
                <div>{$name}<br />{$agef}</div></a>
            </div>

HTML;


        //foreach($v as $l=>$s)

    }


    if(!isset($s))
    {
        $stat = '<h4>Гостей пока не было.</h4>';
    }

    $content = <<<HTML

        <div class="col-sm-10">
            <div>
                <div class="stattitle">Ваши гости</div>

            </div>
            <div class="allstats">
                {$stat}
            </div>
        </div>
HTML;
    return $content;
}

function viewUsers($all_users = null)
{
    global $config;
    $content = '';
    $u = get_user_profile($_SESSION['user']['id']);
    $pref = get_preferences($_SESSION['user']['id']);
    $status = getStatuses($_SESSION['user']['id']);

    if (($u['response']['birthday'] != "") && (isset($u['response']['birthday'])) && ($u['response']['birthday']!="0000-00-00"))
    {
        $date = new DateTime($u['response']['birthday']);
        $now = new DateTime();
        $interval = $now->diff($date);
        $agef = $interval->y;
        $m = substr($agef,-1,1);
        $l = substr($agef,-2,2);
        $agef = $agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

    } else $agef="";

    // country
    $countries = '';

    $countries_array = get_country();
    foreach($countries_array as $countries_arr)
    {

        if($countries_arr['id']==$pref['country_id'])
            $countries .='<option selected value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
        else $countries .='<option value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
    }

    $sett = array();
    if($u['response']['is_host']==1)
    {
        $sett[2] = "activ_settings_items";
        $guestscheck = "checked";
    }
    if($u['response']['is_sponsor']==1)
    {
        $sett[1] = "activ_settings_items";
        $sponsorcheck = "checked";
    }
    if($u['response']['invisible']==1)
        $sett[3] = "activ_settings_items";

    // ZNAK ZADIAKA

    $m = date("m",strtotime($u['response']['birthday']));
    $d =  date("d",strtotime($u['response']['birthday']));
    $znakzadiakatitle= getZodiacalSign($m,$d);

    $znakzadiakaimg = getZodiacalSignNumber($m,$d).'.png"';

    if(isset($u['response']['avatar_id']) && $u['response']['avatar_id']!=null) {
        $av = getImage($u['response']['avatar_id']);
        $avtr = $av['url'];
        if(isset($av['thumb_url']) && $av['thumb_url']!=null)
            $avtr = $av['thumb_url'];
    }else $avtr = $config['defaultAvatarMini'];


    if($u['response']['balance']<0 || $u['response']['balance']=="" || !isset($u['response']['balance']))
        $balance = 0;
    else
        $balance = $u['response']['balance'];

    // country
    $countries = '';
    $selected = '';
    $countries_array = get_country();

    $count = 0;
    foreach($countries_array as $countries_arr)
    {
        if($count==0)
        {
            $count = 10;
        }
        if($countries_arr['id']==$pref['country_id'])
        {
            $countries .='<option selected value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
            $id_count = $pref['country_id'];
        }
        else $countries .='<option value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
    }

    $cities = '';
    $selected = '';
    $cities = $id_count;
    $fa = get_city2($id_count);

    

    $count = 0;
    foreach($fa as $countries_arr)
    {
        if($count==0)
        {
            $count = 10;
        }
        if($countries_arr['id']==$pref['city_id'])
            $cities .='<option selected value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
        else $cities .='<option value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
    }



    $directs = '';
    $directions_array = get_interes_directions();

    if(($directions_array!=null) || ($directions_array!="") || (!isset($directions_array)) || (!empty($directions_array))) {

        foreach ($directions_array as $direct) {
            $nm = get_country_name($direct['country_id']);
            $directs .= <<<HTML
         <li id="direction_{$direct['id']}">{$nm}<span onclick='deldeirection({$direct['id']},"$nm")'>x</span></li>
HTML;
        }
    }



    $pref = get_preferences($_SESSION['user']['id']);
    if($pref['sex_m']==1)
        $sexmalecheck = "checked";
    if($pref['sex_f']==1)
        $sexfemalecheck = "checked";
    $name = $u['response']['first_name']==""?"Без имени":$u['response']['first_name'];

    $stat='';

    include_once ROOT."/api/Pagination/Pagination.class.php";

    // --=Pagination=-- //
    $page = isset($_GET['page']) ? ((int) $_GET['page']) : 1; // Current page

    $per = $config['rowPerPage'];

    $start = ($page-1)*$per; // Старт
    $limit = $per; // Сколько выводить на страницу

    if($all_users==null) {
        $s = get_users2($start,$limit);
        $checkedd = "checked";
    }
    else {
        $checkedd = "";
        $s = get_users($start,$limit);
    }

    if(count($s) == 0) return 'Ничего не найдено';

    $count = qRows(); // Общее количество пользователя для пагинаций
    // ----------------------- //


    foreach($s as $k=>$v) {
        if (isset($v['avatar_id']) && $v['avatar_id'] != null) {
            $av = getImage($v['avatar_id']);
            $avtr = $av['url'];
            if (isset($av['thumb_url']) && $av['thumb_url'] != null)
                $avtr = $av['thumb_url'];
        } else continue;//continue;//$avtr = $config['defaultAvatarMini'];


        if (($v['birthday'] != "") && (isset($v['birthday'])) && ($v['birthday']!="0000-00-00"))
        {
            $date = new DateTime($v['birthday']);
            $now = new DateTime();
            $interval = $now->diff($date);
            $agef = $interval->y;
            $m = substr($agef,-1,1);
            $l = substr($agef,-2,2);
            $agef = ", ".$agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

        } else $agef="";

        $name = $v['first_name']==""?"Без имени":$v['first_name'];

        if(isset($v['user_id'])) $iddd =$v['user_id']; else $iddd =$v['id'];
        $stat.=<<<HTML
            <div class="statuses_1" style="    width: 20%;">
                <a href="{$config['home']}?action=viewprofile&id={$iddd}">
                <div class="statuses_1_img"><img src="{$avtr}" alt></div>
                <div>{$name}{$agef}</div></a>
            </div>
HTML;

    }
    $ggg = $config['home']."?action=meeting";


    $content = <<<HTML
        <input type="hidden" value='20' id="loaded_max" />
        <input type="hidden" value='20' id="page" />
        <div class="col-sm-10">
            <div><form>
                <div class="stattitle">Все пользователели{$opopo}</div>
                <div style="display: inline-block; width: 300px;"><label><input {$checkedd} onclick="changesettings2(1)" type="checkbox" value="1"> Настройки поиска <i class="fa fa-cog"></i></label></div>
        </form>
            </div>
            <div class="row" id="interesses" style="display: none">
            <div class="col-sm-12">
                <form >
                    <div class="col-sm-7">
                        <div class="interes_title">Меня интересуют:</div>
                        <div class="interes_1">
                            <div class="interes_2">
                                <input {$sexfemalecheck} onclick="changesettingssex(1)" type="checkbox" value="1" name="sexfemale" class="sexfemale" />
                                <div>Женщины</div>
                            </div>
                            <div class="interes_2">
                                <input {$sexmalecheck} onclick="changesettingssex(2)" type="checkbox" value="1" name="sexmale" class="sexmale" />
                                <div>Мужчины</div1>
                            </div>
                        </div>

                        <div class="interes_title">Возраст:</div>
                        <div class="interes_1 polzunok">
                            <div id="slider-range"></div>
                            <input type="text" id="amount1" readonly style="border:0;">
                            <input type="text" id="amount2" readonly style="border:0;">
                        </div>

                        <div class="interes_title">Вес:</div>
                        <div class="interes_1 polzunok">
                            <div id="slider-range2"></div>
                            <input type="text" id="amount2_1" readonly style="border:0;">
                            <input type="text" id="amount2_2" readonly style="border:0;">
                        </div>

                        <div class="interes_title">Местоположение:</div>
                        <div class="interes_1">
                            <div class="interes_2">
                                <select name="country" id="countries" class="countries form-control">
                                <option value="0">Не задано</option>
                                    {$countries}
                                </select>
                            </div>
                            <div class="interes_2">
                                    <select name="city" id="cities" class="form-control">{$cities}</select>
                                </div>
                            </div>
                            <div class="interes_2">
                            <div id="resetcountries" class="btn btn-blue reset">Сброс</div></div>
                            <div class="interes_2"><div id="resetcities" class="btn btn-blue reset">Сброс</div></div>

                        </div>
                    </div>
                    <div class="col-sm-5">

                        <div class="interes_3">
                            <input  onclick="changesettings(1)" {$sponsorcheck} type="checkbox" name="sponsor" class="sponsor" />
                            <i class="fa fa-usd" style="    margin-left: 3px;"></i>
                            <div>Спонсор</div>
                        </div>

                        <div class="interes_3">
                            <input  onclick="changesettings(2)" {$guestscheck} type="checkbox" name="guests" class="guests" />
                            <i class="fa fa-home"></i>
                            <div>Приглашает в гости</div>
                        </div>

                        <div class="interes_title">Направления путешествий:</div>
                            <div class="interes_1">
                                <ul class="list-unstyled selected_list" id="directions_all">
                                    {$directs}
                                </ul>
                            </div>
                            <div>
                                <select name="country_direct" id="countries_direct" class="countries form-control">
                                    <option value="select" selected>Добавить страну</option>
                                    {$countries}
                                </select>
                            </div>
                        
                        <a href="{$ggg}" style="    margin-top: 20px;" class="btn btn-blue reset">Поиск</a>
                    </div>
                </form>
            </div>
</div>
            <div class="allstats">
                {$stat}
            </div>
        </div>
HTML;
    return $content;
}

function viewUserFavorites($s)
{
    GLOBAL $config;
    $stat='';




    foreach($s as $k=>$v)
    {
        if(isset($v['avatar_id']) && $v['avatar_id']!=null) {
            $av = getImage($v['avatar_id']);
            $avtr = $av['url'];
            if(isset($av['thumb_url']) && $av['thumb_url']!=null)
                $avtr = $av['thumb_url'];
        }else continue;//$avtr = $config['defaultAvatarMini'];

        if (($v['birthday'] != "") && (isset($v['birthday'])) && ($v['birthday']!="0000-00-00"))
        {
            $date = new DateTime($v['birthday']);
            $now = new DateTime();
            $interval = $now->diff($date);
            $agef = $interval->y;
            $m = substr($agef,-1,1);
            $l = substr($agef,-2,2);
            $agef = ", ".$agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

        } else $agef="";
        $name = $v['first_name']==""?"Без имени":$v['first_name'];
        if(check_favorite($v['id'])==null)
            $btntofav = '+ Добавить';
        else $btntofav = '- Удалить';

        $stat.=<<<HTML
            <div class="statuses_1" style="    width: 20%;">
                <a href="{$config['home']}?action=viewprofile&id={$v['id']}">
                <div style="
    height: 150px;
    overflow: hidden;
    border-radius: 20px;
    width: 150px;
    text-align: center;
    margin: 0 auto;
"><img src="{$avtr}" alt></div>
                <div>{$name}{$agef}</div></a>
                <div class="addtofav" onclick="return fav({$v['id']})" data-id="{$v['id']}">{$btntofav}</div>
            </div>

HTML;


        //foreach($v as $l=>$s)

    }

    if(($s=="") || ($s==null))
    {
        $stat="У вас еще нет избранных пользователей, можете добавить прямо сейчас! <br/><br />";
        $r = get_favorite_rand();
        foreach($r as $k=>$v)
        {
            if(isset($v['avatar_id']) && $v['avatar_id']!=null) {
                $av = getImage($v['avatar_id']);
                $avtr = $av['url'];
                if(isset($av['thumb_url']) && $av['thumb_url']!=null)
                    $avtr = $av['thumb_url'];
            }else continue;//$avtr = $config['defaultAvatarMini'];

            if (($v['birthday'] != "") && (isset($v['birthday'])) && ($v['birthday']!="0000-00-00"))
            {
                $date = new DateTime($v['birthday']);
                $now = new DateTime();
                $interval = $now->diff($date);
                $agef = $interval->y;
                $m = substr($agef,-1,1);
                $l = substr($agef,-2,2);
                $agef = ", ".$agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

            } else $agef="";
            $name = $v['first_name']==""?"Без имени":$v['first_name'];
            if(check_favorite($v['id'])==null)
                $btntofav = '+ Добавить';
            else $btntofav = '- Удалить';

            $stat.=<<<HTML
            <div class="statuses_1" style="    width: 20%;">
                <a href="{$config['home']}?action=viewprofile&id={$v['id']}">
                <div style="
    height: 150px;
    overflow: hidden;
    border-radius: 20px;
    width: 150px;
    text-align: center;
    margin: 0 auto;
"><img src="{$avtr}" alt></div>
                <div>{$name}{$agef}</div></a>
                <div class="addtofav" onclick="return fav({$v['id']})" data-id="{$v['id']}">{$btntofav}</div>
            </div>

HTML;


            //foreach($v as $l=>$s)

        }

        $r = get_favorite_rand_m();
        foreach($r as $k=>$v)
        {
            if(isset($v['avatar_id']) && $v['avatar_id']!=null) {
                $av = getImage($v['avatar_id']);
                $avtr = $av['url'];
                if(isset($av['thumb_url']) && $av['thumb_url']!=null)
                    $avtr = $av['thumb_url'];
            }else continue;//$avtr = $config['defaultAvatarMini'];

            if (($v['birthday'] != "") && (isset($v['birthday'])) && ($v['birthday']!="0000-00-00"))
            {
                $date = new DateTime($v['birthday']);
                $now = new DateTime();
                $interval = $now->diff($date);
                $agef = $interval->y;
                $m = substr($agef,-1,1);
                $l = substr($agef,-2,2);
                $agef = ", ".$agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

            } else $agef="";
            $name = $v['first_name']==""?"Без имени":$v['first_name'];
            if(check_favorite($v['id'])==null)
                $btntofav = '+ Добавить';
            else $btntofav = '- Удалить';

            $stat.=<<<HTML
            <div class="statuses_1" style="    width: 20%;">
                <a href="{$config['home']}?action=viewprofile&id={$v['id']}">
                <div style="
    height: 150px;
    overflow: hidden;
    border-radius: 20px;
    width: 150px;
    text-align: center;
    margin: 0 auto;
"><img src="{$avtr}" alt></div>
                <div>{$name}{$agef}</div></a>
                <div class="addtofav" onclick="return fav({$v['id']})" data-id="{$v['id']}">{$btntofav}</div>
            </div>

HTML;


            //foreach($v as $l=>$s)

        }
    }


    $content = <<<HTML

        <div class="col-sm-10">
            <div>
                <div class="stattitle">Избранные пользователели</div>

            </div>
            <div class="allstats">
                {$stat}
            </div>
        </div>
HTML;
    return $content;
}


function viewChat()
{
    GLOBAL $config;
	
	$dialogsList = get_user_dialogs($_SESSION['user']['id']);
	
	$message_list = '';
	$dialogs = '';
	
	if($dialogsList == null){
		$message_list = '
		<div class="chat_right_sms">
			<div class="chat_right_sms_block">
				<div class="chat_right_sms_block_2">У вас еще нет диалогов</div>
			</div>
		</div>
		';
	}else{
	
		if(!isset($_GET['receiver_id'])){
			header("Location: {$config['home']}?action=chat&receiver_id=".$dialogsList[0]['target_id']);
		}
	
		foreach($dialogsList as $dialog){
			$name = $dialog['isender'] ? $dialog['fullname_receiver'] : $dialog['fullname_sender'];
			$class = $dialog['target_id'] == $_GET['receiver_id'] ? ' class="chat_activ"' : '';
			$temp_id = $dialog['target_id'];
			$photo =  empty($dialog['ava']) ? 'images/poputchik.png' : $dialog['ava'];
			$dialogs .= <<<HTML
			<li{$class} style="position: relative">
				<a href="{$config['home']}?action=chat&receiver_id={$temp_id}" class="mainlink"></a>
                <div class="chat_left_ava"><a href="{$config['home']}?action=viewprofile&id={$temp_id}" target="_blank"><img src="{$photo}" alt></a></div>
                <div class="chat_left_text">
					<div class="chat_left_i_s">
						<div class="chat_left_imya"><a href="{$config['home']}?action=viewprofile&id={$temp_id}" target="_blank">{$name}</a></div>
						<span></span>
					</div>
					<div class="chat_left_stat">{$dialog['txt']}</div>
                </div>
            </li>
HTML;
		}
	}
	
	$rightside = '';
	$receiver = array();
	
	$token = session_id();
	
	if(is_numeric($_GET['receiver_id'])) $messages = get_messages_between($_SESSION['user']['id'], $_GET['receiver_id']);
	$me = getUser($_SESSION['user']['id']);
	$av = $me['ava'] == null ? 'images/poputchik.png' : $me['ava'];
	if(is_numeric($_GET['receiver_id'])) $receiver = getUser($_GET['receiver_id']);
	
			if($messages != null){
				$size = count($messages);
				for($i = $size-1;$i>=0;$i--){
					$msg = $messages[$i];
					$prefix = $msg['sender_id'] == $_SESSION['user']['id'] ? '_my' : '';
					$photo =  empty($msg['ava']) ? 'images/poputchik.png' : $msg['ava'];
					$left = $msg['sender_id'] == $_SESSION['user']['id'] ? '' : '<div class="chat_left_ava"><a href="'.$config['home'].'?action=viewprofile&id='.$msg['sender_id'].'" target="_blank"><img src="'.$photo.'" alt></a></div>';
					$right = $msg['sender_id'] == $_SESSION['user']['id'] ? '<div class="chat_left_ava"><a href="'.$config['home'].'?action=viewprofile&id='.$msg['sender_id'].'" target="_blank"><img src="'.$photo.'" alt></a></div>' : '';
					
					$message_list .= <<<HTML
						<div class="chat_right_sms">
							{$left}
							<div class="chat_right_sms_block{$prefix}">
								<!--<span></span>-->
								<div class="chat_right_sms_block{$prefix}_1">
									{$msg['dt']}
								</div>
								<div class="chat_right_sms_block{$prefix}_2">{$msg['txt']}</div>
							</div>
							{$right}
						</div>
HTML;
				}
			}
	
			$rightside = <<<HTML
			<div class="col-sm-7" style="padding-left: 1px;">
				<div class="chat_top2">{$receiver['first_name']} {$receiver['last_name']}</div>
				<!--<div class="chat_right_more">Более ранние сообщения  <span></span></div>-->
				<div class="chat_right_all_sms area_sms_{$_GET['receiver_id']}" id="area_sms">
						{$message_list}
					</div>
				<div class="chat_right_b">
					<input type="text" placeholder="Текст сообщения" id="send_text_form" class="form-control chat_right_input">
					<!--<div class="right_input_smile"></div>-->
					<div class="right_input_send" id="send_me" onclick="return sendBtn()"></div>
				</div>
			</div>
	<script type="text/javascript">
		/*
		$( "#addtofav" ).click(function() {
			id =  {$_GET['id']};
			$.ajax({
				url: '{$config['home']}?action=addtofav',
				type: 'post',
				data: {u_id: id},
				success: function(data){
					if(data==false || data==null)
					{
						$("#my_notifications").html("<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Пользователь успешно удален из избранных!</div>");
						$("#addtofav").html("Добавиль");
					}else if(data==true)
					{
						$("#my_notifications").html("<div class=\"alert alert-success alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Пользователь добавлен в избранное!</div>");
						$("#addtofav").html("Удалить иранных");
					}
				}
			});
		});
		*/
		
		$("#send_text_form").focus();
		
		$('#send_text_form').change(function(){
			mark_read({$_GET['receiver_id']});
		});
		
		function sendBtn(){
			var txt,msg={};
			
			txt = $("#send_text_form");
			
			msg.func = "send_msg";
			msg.txt = txt.val();
			msg.receiver_id = {$receiver['id']};
			if(!msg.txt) { 
				alert("Message can not be empty"); 
				return; 
			}
			txt.val('');
			txt.focus();
			try { 
				send(msg); 
				log(msg.txt, 1, {$receiver['id']}, '{$av}'); 
			} catch(ex) { 
				//log(ex); 
			}
		}
		
		window.setTimeout(function() {
			  $('#area_sms').stop().animate({
				  scrollTop: $("#area_sms")[0].scrollHeight
				}, 200);
			}, 10);
		</script>
HTML;
	
		
	
	
    $content = <<<HTML
        <div class="col-sm-3" style="padding-right: 0;">
            <div class="chat_top"></div>
            <div class="chat_right_all_sms_1">
                <div class="chat_left_h">
                Диалоги
                </div>
                <div class="chat_left_c">
                <ul class="list-unstyled">
					{$dialogs}
                </ul>
            </div>
            </div>
        </div>
		{$rightside}
HTML;
    return $content;
}

function wp_trim_words( $text, $num_words = 55, $more = null ) {
	mb_internal_encoding("UTF-8");
	
	$text= strip_tags($text);
	$lengt = mb_strlen($text);
	$text = trim(mb_substr($text, 0, 100));
	if($lengt > 100) $text .= '...';
	
	return $text;
	
}

function viewUserStatuses()
{
    GLOBAL $config;
    $stat='';

    $s = getStatuses();

    if(count($s) == 0) return 'Ничего не найдено';

    // country
    $countries = '<option value="" selected>Страна</option>';
    $countries_array = get_country();

    foreach($countries_array as $countries_arr)
    {
        $countries .='<option value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
    }


    foreach($s as $k=>$v)
    {
        if(isset($v['avatar_id']) && $v['avatar_id']!=null) {
            $av = getImage($v['avatar_id']);
        $avatar = $av['url'];
            if(isset($av['thumb_url']) && $av['thumb_url']!=null)
                $avatar = $av['thumb_url'];
            
        }else $avatar = $config['defaultAvatarMini'];

        $likes = get_likes($v['stat_id']);

        if (($v['birthday'] != "") && (isset($v['birthday'])) && ($v['birthday']!="0000-00-00"))
        {
            $date = new DateTime($v['birthday']);
            $now = new DateTime();
            $interval = $now->diff($date);
            $agef = $interval->y;
            $m = substr($agef,-1,1);
            $l = substr($agef,-2,2);
            $agef = ", ".$agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

        } else $agef="";
        $name = $v['first_name']==""?"Без имени":$v['first_name'];
        $date = date_create($v['dt']);

        $dateofstat =  date_format($date, 'Y-m-d');
        $dateofstat = russianDate($dateofstat).' '.date_format($date, 'H:i') ;
		$short = wp_trim_words($v['status_text'], 0);
        $stat.=<<<HTML
        <div class="statuses col-sm-4">
            <div class="statuses_1">
                <a href="{$config['home']}?action=viewprofile&id={$v['id']}">
                <div class="statuses_1_img"><img src="{$avatar}" alt></div>
                <div>{$name}{$agef}</div></a>
            </div>
            <div class="statuses_2">
                <div class="statuses_2_text">
                    {$short}
                </div>
                <div class="statuses_2_text_all" style="">
                <div class="overlay" title="окно"></div>
                <div class="popup">                    
                    {$v['status_text']}
                </div></div>
                <div class="statuses_likes" >
                <section style="    display: inline-block;    font-size: 10px;    color: gray;">{$dateofstat}</section>
                    <img src="images/likes.png" onclick="setlike({$v['stat_id']})" alt><div onclick="setlike({$v['stat_id']})" class="like_{$v['stat_id']}">{$likes['likes']}</div>
                </div>
            </div>
        </div>
HTML;


        //foreach($v as $l=>$s)

    }



    $content = <<<HTML

        <div class="col-sm-10">
            <div>
                <div class="stattitle">Статусы пользователей</div>
                <div class="searchfield">
                    <div class="row">
                        <div class="col-sm-4">
                            <select name="sex" id="sex" class="form-control countries_1">
                                <option value="">Пол</option>
                                <option value="m">Мужской</option>
                                <option value="f">Женский</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select name="country" id="countries" class="form-control countries_1" >
                                {$countries}
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <select name="city" id="cities" class="form-control countries_1">
                                <option value="" selected>Город</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div id="olololo"></div>
            <div class="allstats">
                {$stat}
            </div>
        </div>
HTML;
    return $content;
}

function viewEditUserForm($u)
{
    Global $config; 
    if($u['sex']=="m")
        $status[0] = "selected";
    elseif($u['sex']=="f")
        $status[1] = "selected";


    // country
    $countries = '';
    $selected = '';
    $countries_array = get_country();

    foreach($countries_array as $countries_arr)
    {
        if($u['country']==$countries_arr['nm']) 
            $countries .='<option selected value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
        else $countries .='<option '.$selected.' value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
    }

$avatar = $config['defaultAvatarMini'];
    $iArray = getUserImages($_SESSION['user']['id']);
    if(count($iArray) == 0) $images = '';
    else{
        $images = 'Выберите аватар<br />';
        if($u['avatar_id']!="")
        {
            $av = getImage($u['avatar_id']);
            $avatar = $av['url'];
            if(isset($av['thumb_url']) && $av['thumb_url']!=null)                 $avatar = $av['thumb_url'];
            $cc = 5;
        }else
        {
            $avatar = $config['defaultAvatarMini'];
            $cc = 100;
        }
        $avatartitle = '';
        if($avatar=="" || !isset($avatar) || empty($avatar)) $avatar = $config['defaultAvatarMini'];
        foreach($iArray as $img)
        {
            if($cc == 100) $selec = "checked";
            if($cc== 5)
            {
                if($u['avatar_id']==$img['id'])
                    $selec = "checked";
            }
            $url = $img['url'];
            if(isset($img['thumb_url']) && $img['thumb_url']!=null)                 $url = $img['thumb_url'];
            $images .= <<<HTML
            <div class="avatars">
                <input type="radio" {$selec} value="{$img['id']}" name="avatar_id" >
                <img  src="{$url}" style="width: 100px;">
            </div>
HTML;
            $cc = 0;
        }
        $images .= '</ul>';
    }
    $c = <<<HTML
    <div class="col-sm-10">
    <form  type="post" id="avaloadform"  enctype="multipart/form-data" method="post" action="{$config['home']}?action=uploadImageAva2"><input type="file"  name="image" onchange="readURL(this);" value="Загрузить фото" id="fileUpload" style="display: none">
                        <input type="submit" value="Сохранить">
                    </form>
        <form action="{$config['home']}?action=doedituser" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-6">
                    <label>Аватар</label>
                    <span style="    display: block;    font-size: 10px;">Нажмите что бы загрузить аватар:</span>
                    <img id="userImage" alt="Нажмите что бы загрузить фото" style="    text-align: center;  line-height: 20px;  width: 150px;    display: block;" src="{$avatar}">
                </div>
                <div class="col-sm-6">
                    {$images}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Имя</label>
                    <input name="first_name" id="first_name" type="text" value="{$u['first_name']}" class="form-control">
                </div>
                <div class="col-sm-6">
                    <label>Фамилия</label>
                    <input name="last_name" id="last_name" type="text" value="{$u['last_name']}" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>e-mail</label>
                    <input name="email" id="email" type="email" value="{$u['login']}" class="form-control">
                </div>
                <div class="col-sm-6">
                    <label>Телефон</label>
                    <input name="phone" id="phone" type="text" value="{$u['phone']}" class="phone form-control">
                </div> 
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Пол</label>
                    <select name="sex" id="sex" class="form-control">
                        <option value="m" {$status[0]}>Мужской</option>
                        <option value="f" {$status[1]}>Женский</option>
                    </select>
                </div>
                <div class="col-sm-6">
                    <label>Дата рождения</label>
                    <input name="birthday" id="birthday" type="text" value="{$u['birthday']}" class="form-control">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Страна</label>
                    <select name="country" id="countries" class="form-control">
                        {$countries}
                    </select>
                </div>
                <div class="col-sm-6">
                    <label>Город</label>
                    <select name="city" id="cities" class="form-control">

                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>О себе</label>
                    <textarea class="form-control" name="about" id="about">{$u['about']}</textarea>
                </div>
                <div class="col-sm-6"> 
					<div class="row">
						<div class="col-sm-6">
							<label>Рост</label>
							<input name="growth" id="growth" type="text" value="{$u['growth']}" class="form-control"> 
						</div>
						 <div class="col-sm-6">
							<label>Вес</label>
							<input name="weight" id="weight" type="text" value="{$u['weight']}" class="form-control"> 
						</div>
					</div>
                    <a href="{$config['home']}?action=editpassshow"  class="btn btn-default" >Изменить пароль</a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <input type="hidden" name="user_id" value="{$u['id']}" />
                    <input type="submit" value="Сохранить" class="btn btn-default" />
                    <a href="{$config['home']}?action=showimages"  class="btn btn-default" >Фотографии</a>
                </div>
            </div>
        </form>
    </div>
HTML;

    return $c;
}


function viewEditUserPassword($u)
{
    Global $config;

    $c = <<<HTML
    <div class="col-sm-10">
        <form action="{$config['home']}?action=doeditpass" method="post" enctype="multipart/form-data">
            	<div class="row" style="    margin-top: 10px;">
                <div class="col-sm-4">
                    <label>Старый пароль</label>
                    <input name="password" id="password" type="password" value="" class="form-control">
                </div>
                <div class="col-sm-4"></div><div class="col-sm-4"></div>
            </div>
            <div class="row" style="    margin-top: 10px;">
                <div class="col-sm-4">
                    <label>Новый пароль</label>
                    <input name="passwordnew" id="passwordnew" type="password" value="" class="form-control">
                </div>
                <div class="col-sm-4"></div><div class="col-sm-4"></div>
            </div>

            <div class="row" style="    margin-top: 10px;">
                <div class="col-sm-4">
                    <label>Новый пароль еще раз</label>
                    <input name="passwordnew2" id="passwordnew2" type="password" value="" class="form-control">
                </div>
                <div class="col-sm-4"></div><div class="col-sm-4"></div>
            </div>

            <div class="row" style="    margin-top: 10px;">
                <div class="col-sm-12">
                <input type="hidden" name="user_pass" value="{$u['pass']}" />
                    <input type="hidden" name="user_id" value="{$u['id']}" />
                    <input type="submit" value="Сохранить" class="btn btn-default"/>
                </div>
            </div>
        </form>
    </div>
HTML;

    return $c;
}

function showImagesOfProfile()
{
    global $config;
    $iArray = getUserImages($_SESSION['user']['id']);
    if(count($iArray) == 0) $images = 'Фотографии отсутствуют';
    else{
        $images = '<ul id="goodImages">';
        foreach($iArray as $img)
        {
            //$url = getImageThumb($img['filename']);
            $url = $img['url'];
            if(isset($img['thumb_url']) && $img['thumb_url']!=null)                 $url = $img['thumb_url'];
            $images .= <<<HTML
            <li><img src="{$url}" style="    width: 150px;" /><a href="{$config['home']}?action=deleteImage&image_id={$img['id']}"><img src="images/+.png" alt> </a></li>
HTML;
        }
        $images .= '</ul>';
    }


    return <<<HTML
        <div class="col-sm-10">
            <div class="row">
                <h2>Фотографии / <a href="{$config['home']}?action=editprofile">назад</a> </h2>
            </div>
            <div class="row">
                <div class="col-sm-7">
                    {$images}
                </div>
                <div class="col-sm-5">
                    <form action="{$config['home']}?action=uploadImage" method="POST" enctype="multipart/form-data">
                        <input type="file" name="image" />
                        <input type="submit" value="Загрузить" name="subImage" />
                    </form>
                </div>
            </div>
        </div>
HTML;

}


function getZodiacalSign($month, $day) {
    $signs = array("Козерог", "Водолей", "Рыбы", "Овен", "Телец", "Близнецы", "Рак", "Лев", "Девы", "Весы", "Скорпион", "Стрелец");
    $signsstart = array(1=>21, 2=>20, 3=>20, 4=>20, 5=>20, 6=>20, 7=>21, 8=>22, 9=>23, 10=>23, 11=>23, 12=>23);
    return $day < $signsstart[$month + 1] ? $signs[$month - 1] : $signs[$month % 12];
}
function getZodiacalSignNumber($month, $day) {
    $signs = array(1,2,3,4,5,6,7,8,9,10,11,12);
    $signsstart = array(1=>21, 2=>20, 3=>20, 4=>20, 5=>20, 6=>20, 7=>21, 8=>22, 9=>23, 10=>23, 11=>23, 12=>23);
    return $day < $signsstart[$month + 1] ? $signs[$month - 1] : $signs[$month % 12];
}

function viewUserGuest($u_id)
{
    global $config;
    $content = '';
    $u = get_user_profile($u_id);
    $status = getStatuses($u_id);

    if (($u['response']['birthday'] != "") && (isset($u['response']['birthday'])) && ($u['response']['birthday']!="0000-00-00"))
    {
        $date = new DateTime($u['response']['birthday']);
        $now = new DateTime();
        $interval = $now->diff($date);
        $agef = $interval->y;
        $m = substr($agef,-1,1);
        $l = substr($agef,-2,2);
        $agef = $agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

    } else $agef="";
    // country
    $countries = '';

    $countries_array = get_country();

    foreach($countries_array as $countries_arr)
    {
        $countries .='<option value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
        if($countries_arr['id']==$u['response']['country'])
        {
            $country =   <<<HTML
                            <tr>
                                <td>Страна:</td>
                                <td>{$countries_arr['nm']}</td>
                            </tr>
HTML;

        }

    }

    if($u['response']['country']!="") $country = <<<HTML
                            <tr>
                                <td>Страна:</td>
                                <td>{$u['response']['country']}</td>
                            </tr>
HTML;

    $sett = array();
    if($u['response']['is_host']==1)
        $sett[2] = "<button class=\"hostbtn btn activ_settings_items btn-blue\" ><i class=\"fa fa-home\"></i></button>";
    if($u['response']['is_sponsor']==1)
        $sett[1] = "<button class=\"sponsorbtn btn activ_settings_items btn-blue\" ><i class=\"fa fa-dollar\"></i></button>";
    if($u['response']['invisible']==1)
        $sett[3] = "<button class=\"invisiblebtn btn activ_settings_items btn-blue\" ><i class=\"fa fa-eye\"></i></button>";

    // ZNAK ZADIAKA

    $m = date("m",strtotime($u['response']['birthday']));
    $d =  date("d",strtotime($u['response']['birthday']));
    $znakzadiakatitle= getZodiacalSign($m,$d);

    $znakzadiakaimg = getZodiacalSignNumber($m,$d).'.png"';
    if(isset($u['response']['avatar_id']) && $u['response']['avatar_id']!=null) {
        $av = getImage($u['response']['avatar_id']);
        $avatar = $av['url'];
        if(isset($av['thumb_url']) && $av['thumb_url']!=null)                 $avatar = $av['thumb_url'];
    }else $avatar = $config['defaultAvatarMini'];


    if(check_favorite($u_id)==null)
        $btntofav = '<button type="button" id="addtofav" class="season summer">Добавиль в избранное</button>';
    else $btntofav = '<button type="button" id="addtofav" class="season summer">Удалить из избранных</button>';


    $style ='';
    $iArray = getUserImages($u_id);
    if(count($iArray) == 0)
    {
        $images = '';
        $style = '<style> .media{display:none;} </style>';
    }
    else{
        $images = '';
        foreach($iArray as $img)
        {
            //$url = getImageThumb($img['filename']);
            $url = $img['url'];
            if(isset($img['thumb_url']) && $img['thumb_url']!=null)                 $url = $img['thumb_url'];
            $urlfull = $img['url'];
            $likes = get_photo_likes($img['id']);
            $images .= <<<HTML
            <li><div class="statuses_user_img"><div class="statuses_likes" >
                    <img src="images/likes.png" onclick="setlike({$img['id']})" alt><div onclick="setlike({$img['id']})" class="like_{$img['id']}">{$likes['likes']}</div>
                </div>
                <a href="{$urlfull}" data-lightbox="roadtrip"><img  src="{$url}"></a></div></li>
HTML;
        }
        $images .= '</ul>';
    }

if((isset($u['response']['city'])) && ($u['response']['city']!="")) {

    $city = <<<HTML
        <tr>
            <td>Город:</td>
            <td>{$u['response']['city']}</td>
        </tr>

HTML;
}


    $gg = get_user_gifts($u_id);
    $gifts_u = '';
    foreach($gg as $b)
    {
        $gifts_u .= '<li><img src="'.$b['url'].'"></li>';
    }
    if($gg==null || !isset($gg) || empty($gg))
        $gifts_u = <<<HTML
                    <li>Подарков нет</li>
HTML;


    $dateofbirthday = russianDate($u['response']['birthday']);
    //($dateofbirthday=="00") || ($dateofbirthday=="00 ") ||($dateofbirthday==" 00") || ($dateofbirthday==" 00 ")
    if(strpos($dateofbirthday, "00") !== FALSE)
        {
            $dateofbirthday = "Не указана";
            $znakzadiakatitle = "";
            $znakzadiakaimg = '" style="display:none"';
        }
    $g = get_all_gifts();
    $gifts = '';
    $count_cena = 0;
    foreach($g as $b)
    {
        $gifts .= '<li><img onclick="sendgift('.$b['id'].')" src="'.$b['url'].'" data-id="'.$b['id'].'" data-price="'.$b['price'].'"> Цена: '.$b['price'].' монет</li>';
        $count_cena++;
    }


    $name = $u['response']['first_name']==""?"Без имени":$u['response']['first_name'];

	$send_msg = '';
	
	if($u_id != $_SESSION['user']['id']){
		$send_msg = <<<HTML
			<a href="#" class="edit_link" data-toggle="modal" data-target="#send_msg2"><i class="fa fa-pencil"></i> Написать сообщение</a>
			<div class="modal fade text-left" id="send_msg2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Отправить сообщение</h4>
					  </div>
					  <div class="modal-body">
							<form id="send_msg" method="post">
								<div class="form-group">
									<label>Сообщение</label>
									<textarea class="form-control" style="height:150px" name="sm_text" id="sm_text"></textarea>
								</div>
								<input type="hidden" name="sm_user_id" id="sm_user_id" value="{$u_id}" />
								<div class="form-group">
									<input type="submit" class="btn btn-primary" value="Отправить" />
								</div>
							</form>
					  </div>
					</div>
				  </div>
				</div>
HTML;
	}
	
    if($u['response']['sex']=='f') $sex = 'Женский'; else $sex = 'Мужской';
    $content .= <<<HTML
    {$style}
        <div id="my_notifications"></div>
        <div class="overlay overlay3" title="окно"></div>
        <div class="popup popup3">
        <div class="close_window">x</div>
        <ul class="list-inline popupgifts">
                            {$gifts}
                        </ul>
        </div>
        <div class="col-sm-2">
            <div id="profile-avatar">
                <div id="avatar-top">
                <h3 class="btn-add" style=" color: white; position: absolute; border-radius: 50%; margin: -105px 0 0 125px; padding: 9px; font-size: 10px; display:none;">98</h3>
                    <div class="user_logo_img">
						<!--<img src="{$avatar}" />-->
						<div class="poster" style="background:url('{$avatar}') no-repeat;"></div>
					</div>
                    <h3>{$name}</h3>
                    <h4>{$agef}</h4>
					{$send_msg}
                </div>
                <div style="margin:10px 0">
                    <div class="settings-item">
                        {$sett[1]} {$sett[2]} {$sett[3]}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div id="submit_over">
                <div id="submit_status" style="    width: 70%;    display: inline-block;">
                    <span></span>
                    <input type="text" readonly value="{$status[0]['status_text']}" name="status" id="status" placeholder="Пользователь еще не добавил статус" />
                </div>
                {$btntofav}
            </div>
            <!--<div class="seasons row">
                <h4>Сезоны</h4>
                <ul class="list-unstyled seasons-list">
                    <li><button type="button" class="season summer">Лето</button></li>
                    <li><button type="button" class="season autumn">Осень</button></li>
                    <li><button type="button" class="season winter">Зима</button></li>
                    <li><button type="button" class="season spring">Весна</button></li>
                </ul>

            </div>-->
            <div class="about row">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>О себе</h4>
                    </div>
                </div>
                <div class="about-well">
                    <div  style="    width: 90%;">
                        <table class="table table-about" style="    width: 400px;    margin: 0 auto;">
                            <tr style="border-top:0">
                                <td>Дата рождения:</td>
                                <td>{$dateofbirthday}</td>
                            </tr>
                            <tr >
                                <td>Пол:</td>
                                <td>$sex</td>
                            </tr>
                            {$country}
                            {$city}
							<!--<tr>
                                <td>Рост:</td>
                                <td>{$u['response']['growth']} м</td>
                            </tr>
							<tr>
                                <td>Вес:</td>
                                <td>{$u['response']['weight']} кг</td>
                            </tr> -->
                        </table>
                        <p style="    margin: 10px 0 0 50px;">{$u['response']['about']}</p>
                    </div>
                    <div class="sign">
                        <div class="inner">
                            Знак зодиака: <br>
                                {$znakzadiakatitle}
                                <img src="images/png/{$znakzadiakaimg}" alt>
                        </div>
                    </div>
                </div>
            </div>
            <div class="surprices row">
                <h4>Подарки <a style="    float: right;    margin-right: 50px;    font-size: 15px;    text-decoration: none;    cursor: pointer;" class="edit_link" id="btnplus">+ Подарить подарок</a></h4>
                <ul class="list-inline">
                    {$gifts_u}
                </ul>
            </div>
            <div class="media row">
                <h4>Фотографии и видео</h4>
                <ul class="list-inline">
                    {$images}
                </ul>
            </div>

        </div>


HTML;

    return $content;
}


function viewUserInteres($u_id)
{
    global $config;
    $content = '';
    $u = get_user_profile($u_id);
    $pref = get_preferences($_SESSION['user']['id']);
    $status = getStatuses($u_id);

    if (($u['response']['birthday'] != "") && (isset($u['response']['birthday'])) && ($u['response']['birthday']!="0000-00-00"))
    {
        $date = new DateTime($u['response']['birthday']);
        $now = new DateTime();
        $interval = $now->diff($date);
        $agef = $interval->y;
        $m = substr($agef,-1,1);
        $l = substr($agef,-2,2);
        $agef = $agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

    } else $agef="";

    // country
    $countries = '';

    $countries_array = get_country();
    foreach($countries_array as $countries_arr)
    {

        if($countries_arr['id']==$pref['country_id'])
            $countries .='<option selected value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
        else $countries .='<option value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
    }

    $sett = array();
    if($u['response']['is_host']==1)
    {
        $sett[2] = "activ_settings_items";
        $guestscheck = "checked";
    }
    if($u['response']['is_sponsor']==1)
    {
        $sett[1] = "activ_settings_items";
        $sponsorcheck = "checked";
    }
    if($u['response']['invisible']==1)
        $sett[3] = "activ_settings_items";

    // ZNAK ZADIAKA

    $m = date("m",strtotime($u['response']['birthday']));
    $d =  date("d",strtotime($u['response']['birthday']));
    $znakzadiakatitle= getZodiacalSign($m,$d);

    $znakzadiakaimg = getZodiacalSignNumber($m,$d).'.png"';

    if(isset($u['response']['avatar_id']) && $u['response']['avatar_id']!=null) {
        $av = getImage($u['response']['avatar_id']);
        $avtr = $av['url'];
        if(isset($av['thumb_url']) && $av['thumb_url']!=null)
            $avtr = $av['thumb_url'];
    }else continue;//$avtr = $config['defaultAvatarMini'];


    if($u['response']['balance']<0 || $u['response']['balance']=="" || !isset($u['response']['balance']))
        $balance = 0;
    else
        $balance = $u['response']['balance'];

    // country
    $countries = '';
    $selected = '';
    $countries_array = get_country();

    $count = 0;
    foreach($countries_array as $countries_arr)
    {
        if($count==0)
        {
            $count = 10;
        }
        if($countries_arr['id']==$pref['country_id'])
            $countries .='<option selected value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
        else $countries .='<option value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
    }
    $directs = '';
    $directions_array = get_interes_directions();

    if(($directions_array!=null) || ($directions_array!="") || (!isset($directions_array)) || (!empty($directions_array))) {

        foreach ($directions_array as $direct) {
            $nm = get_country_name($direct['country_id']);
            $directs .= <<<HTML
         <li id="direction_{$direct['id']}">{$nm}<span onclick='deldeirection({$direct['id']},"$nm")'>x</span></li>
HTML;
        }
    }



    $pref = get_preferences($_SESSION['user']['id']);
        if($pref['sex_m']==1)
            $sexmalecheck = "checked";
        if($pref['sex_f']==1)
            $sexfemalecheck = "checked";
    $name = $u['response']['first_name']==""?"Без имени":$u['response']['first_name'];

	include_once($_SERVER['DOCUMENT_ROOT']."/integration/robokassa/index.php");
	$popol = get_items();
    $content .= <<<HTML
        <div class="col-sm-2">
            <div id="profile-avatar">
                <div id="avatar-top">
                    <div class="user_logo_img"><div class="poster" style="background:url('{$avtr}') no-repeat;"></div></div>
                    <h3>{$name}</h3>
                    <h4>{$agef}</h4>
                    <a href="{$u['home']}?action=editprofile" class="edit_link"><i class="fa fa-pencil"></i> Редактировать профиль</a>
                </div>
                <div class="balance">
                    Ваш баланс: {$balance} <img src="images/little-logo.png" />
                    <p><button style="    padding: 5px 20px;    border-radius: 20px;" data-toggle="modal" data-target="#popol" class="btn btn-blue">Пополнить</button></p>
                </div>
				<div class="modal fade" id="popol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm modal-pop">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><img src="http://poputchiki.ru/images/logo_small.png" width="32" style="margin-right:10px" />Пополнение счета</h4>
					  </div>
					  <div class="modal-body">
						{$popol}
					  </div>
					</div>
				  </div>
				</div>
                <div class="settings">
                    <div class="settings-item">
                        <button class="sponsorbtn btn {$sett[1]} btn-blue" onclick="changesettings(1)"><i class="fa fa-dollar"></i></button>
                        Включите иконку, если вы готовы оплатить путешествие
                    </div>
                    <div class="settings-item">
                        <button class="hostbtn btn {$sett[2]} btn-blue" onclick="changesettings(2)"><i class="fa fa-home"></i></button>
                        Включите иконку, если вы готовы принять попутчиков у себя дома
                    </div>
                    <div class="settings-item">
                        <button class="invisiblebtn {$sett[3]} btn btn-blue" onclick="changesettings(3)"><i class="fa fa-eye"></i></button>
                        Включите режим Невидимки
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div id="submit_over">
                <div id="submit_status">
                    <span></span>
                    <input type="text" value="{$status[0]['status_text']}" name="status" id="status" placeholder="Введите ваш новый статус! Расскажите друзья о том, куда вы хотите поехать!" />
                </div>
                <button type="button" onclick="status()" class="btn btn-add">Добавить новый статус</button>
            </div>
            <br /><br />
            <div class="col-sm-12">
                <form >
                    <div class="col-sm-7">
                        <div class="interes_title">Меня интересуют:</div>
                        <div class="interes_1">
                            <div class="interes_2">
                                <input {$sexfemalecheck} onclick="changesettingssex(1)" type="checkbox" value="1" name="sexfemale" class="sexfemale" />
                                <div>Женщины</div>
                            </div>
                            <div class="interes_2">
                                <input {$sexmalecheck} onclick="changesettingssex(2)" type="checkbox" value="1" name="sexmale" class="sexmale" />
                                <div>Мужчины</div1>
                            </div>
                        </div>

                        <div class="interes_title">Возраст:</div>
                        <div class="interes_1 polzunok">
                            <div id="slider-range"></div>
                            <input type="text" id="amount1" readonly style="border:0;">
                            <input type="text" id="amount2" readonly style="border:0;">
                        </div>

                        <div class="interes_title">Вес:</div>
                        <div class="interes_1 polzunok">
                            <div id="slider-range2"></div>
                            <input type="text" id="amount2_1" readonly style="border:0;">
                            <input type="text" id="amount2_2" readonly style="border:0;">
                        </div>

                        <div class="interes_title">Местоположение:</div>
                        <div class="interes_1">
                            <div class="interes_2">
                                <select name="country" id="countries" class="countries form-control">
                                <option value="0">Не задано</option>
                                    {$countries}
                                </select>
                            </div>
                            <div class="interes_2">
                                    <select name="city" id="cities" class="form-control"></select>
                                </div>
                            </div>
                            <div class="interes_2">
                            <div id="resetcountries" class="btn btn-blue reset">Сброс</div></div>
                            <div class="interes_2"><div id="resetcities" class="btn btn-blue reset">Сброс</div></div>

                        </div>
                    </div>
                    <div class="col-sm-5">

                    <div class="interes_3">
                        <input  onclick="changesettings(1)" {$sponsorcheck} type="checkbox" name="sponsor" class="sponsor" />
                        <i class="fa fa-usd" style="    margin-left: 3px;"></i>
                        <div>Спонсор</div>
                    </div>

                    <div class="interes_3">
                        <input  onclick="changesettings(2)" {$guestscheck} type="checkbox" name="guests" class="guests" />
                        <i class="fa fa-home"></i>
                        <div>Приглашает в гости</div>
                    </div>

                    <div class="interes_title">Направления путешествий:</div>
                        <div class="interes_1">
                            <ul class="list-unstyled selected_list" id="directions_all">
                                {$directs}
                            </ul>
                        </div>
                        <div>
                            <select name="country_direct" id="countries_direct" class="countries form-control">
                                <option value="select" selected>Добавить страну</option>
                                {$countries}
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>

HTML;

    return $content;
}

function poputchikiright()
{
    global $config;
    $poputchiki = get_users_poputchiki();

    $style ='';
    $iArray = getUserImages($_SESSION['user']['id']);
    if(count($iArray) == 0)
    {
        $images = '';
        $style = '<style> .media{display:none;} </style>';
    }
    else{
        $images = '';
        foreach($iArray as $img)
        {
            //$url = getImageThumb($img['filename']);
            $url = $img['url'];
            if(isset($img['thumb_url']) && $img['thumb_url']!=null)                 $url = $img['thumb_url'];
            $urlfull = $img['url'];
            $likes = get_photo_likes($img['id']);
            $images .= <<<HTML

            <li>
				<div class="statuses_user_img"><div class="statuses_likes">
                    <input type="radio" value="{$img['id']}" name="image_id">
				</div>
                <img  src="{$url}">
			</div>
			</li>
HTML;
        }
        $images .= '';
    }
    $right_poputchiki ='
                ';

    $right_poputchiki .= <<<HTML
            <script type="text/javascript">
                $(window).load(function(){

					var is_mobile = false;
					var pixels = 525;
					if( $('.poputchik2').css('display')=='none') {
						is_mobile = true;
						pixels = 220;
					}
					
                        visible = is_mobile ? 1 : 4, //Set the number of items that will be visible
                        index = 0, //Starting index
                        endIndex = ( $(".poputchik").length / visible ) - 1;
						
                    $(".forward").click(function(){
                        if(index < endIndex ){
                          index++;
                          $(".poputchik").animate({'left':'-='+pixels+'px'});
                        }
                    });

                    $(".backward").click(function(){
                        if(index > 0){
                          index--;
                          $(".poputchik").animate({'left':'+='+pixels+'px'});
                        }
                    });


                    $('.popup2 .close_window, .overlay2').click(function (){
                    $('.popup2, .overlay2').css('opacity','0');
                    $('.popup2, .overlay2').css('visibility','hidden');
                    });
                    $('.iwanttobelikethis').click(function (e){
                    $('.popup2, .overlay2').css('opacity','1');
                    $('.popup2, .overlay2').css('visibility','visible');
                    e.preventDefault();
                    });
                });
            </script>
            <div class="overlay overlay2" title="окно"></div>
            <div class="popup2 popup">
                <div class="close_window">x</div>
					<p>Добавление стоит 150 монет! Выберите фото:</p>
					<form method="post" action="{$config['home']}?action=addtopoputchik">
						<ul class="list-inline popupgifts">
							{$images}
						</ul>
						<input class="btn form-control" type="submit" value="Добавить">
					</form>
            </div>
			<div style="position:relative">
			<a href="#"  onclick="return;" class="mobilewanter iwanttobelikethis visible-xs btn btn-primary"><i class="fa fa-plus"></i></a>
            <div style="display: inline-block;">
            <a class="poputchik2 iwanttobelikethis hidden-xs" style="cursor:pointer; margin-top: 70px;">
                <div class="userblock" style="    margin: 20px 20px 40px 0;">
                    <img src="images/poputchik.png" />
                    <h3 class="btn-add" style="margin: 10px 0 42px 0;    color: white;">Попасть сюда</h3>

                </div>
			</a>
            <nav class="section3 navigations"></nav>
			</div>
			<div class="backward">
				<img src="images/sd.png">
			</div>
            <div class="vippoputchiki">
                <div class="horizontal">

HTML;
;
    $count = 0;
    foreach($poputchiki as $k=>$v)
    {
        foreach($v as $l=>$s)
        {
            if($l =="first_name")
                $pop['first_name'] = $s;
            if($l =="city")
                $pop['city'] = $s;
            if($l =="id")
                $pop['id'] = $s;
            if($l =="avatar_id")
                $pop['avatar_id'] = $s;
            if($l == "last_name")
                $pop['last_name'] = $s;
            if($l == "birthday") {
                if (($s != "") && (isset($s))) {
                    $date = new DateTime($s);
                    $now = new DateTime();
                    $interval = $now->diff($date);
                    $agef = $interval->y;
                    $m = substr($agef, -1, 1);
                    $l = substr($agef, -2, 2);
                    $agef = $agef . ' ' . ((($m == 1) && ($l != 11)) ? 'год' : ((($m == 2) && ($l != 12) || ($m == 3) && ($l != 13) || ($m == 4) && ($l != 14)) ? 'года' : 'лет'));
                }else $agef ="";
            }
        }


        if($pop['avatar_id']!="") {
            $av = getImage($pop['avatar_id']);
            $avatar = $av['url'];             if(isset($av['thumb_url']) && $av['thumb_url']!=null)                 $avatar = $av['thumb_url'];
        }else $avatar = $config['defaultAvatarMini'];
        $count++;
        $name = $pop['first_name']==""?"Без имени":$pop['first_name'];
        $right_poputchiki.='
                <a class="poputchik" href="'.$config['home'].'?action=viewprofile&id='.$pop['id'].'">
                <div id="section'.$count.'" class="hjkl"></div>
                <div class="statuses_1_img3">
					<!--<img src="'.$avatar.'" />-->
					<div class="poster" style="background:url('.$avatar.') no-repeat;"></div>
				</div>
                <div class="userblock">
                    <h3>'.$name.'</h3>
                    <h4 style="    margin: 0;">'.$pop['city'].'</h4>
                    <h4>'.$agef.'</h4>
                </div></a>';
    }
    $right_poputchiki .='</div><div class="row"><div class="col-xs-12 text-left" style="padding-left:35px;margin-bottom:20px;margin-top:-20px"><a href="index.php?action=statuses">Статусы</a> | <a href="/index.php?action=meeting&all=true">Знакомства</a></div></div></div><div class="forward"><img src="images/as.png"></div></div>';
	
	$right_poputchiki .= '';

    return $right_poputchiki;
}

function russianDate($date){
    $date=explode("-", $date);
    switch ($date[1]){
        case 1: $m='Января'; break;
        case 2: $m='Февраля'; break;
        case 3: $m='Марта'; break;
        case 4: $m='Апреля'; break;
        case 5: $m='Мая'; break;
        case 6: $m='Июня'; break;
        case 7: $m='Июля'; break;
        case 8: $m='Августа'; break;
        case 9: $m='Сентября'; break;
        case 10: $m='Октября'; break;
        case 11: $m='Ноября'; break;
        case 12: $m='Декабря'; break;
    }
    switch ($date[2]){
        case '01': $d='1'; break;
        case '02': $d='2'; break;
        case '03': $d='3'; break;
        case '04': $d='4'; break;
        case '05': $d='5'; break;
        case '06': $d='6'; break;
        case '07': $d='7'; break;
        case '08': $d='8'; break;
        case '09': $d='9'; break;
        default:
            $d=$date[2]; break;

    }
    return $d .'&nbsp;'.$m;
}

function viewUserMain($u_id)
{
    global $config;
    $content = '';

    $u = get_user_profile($u_id);
    $status = getStatuses($u_id);

    if (($u['response']['birthday'] != "") && (isset($u['response']['birthday'])) && ($u['response']['birthday']!="0000-00-00"))
    {
        $date = new DateTime($u['response']['birthday']);
        $now = new DateTime();
        $interval = $now->diff($date);
        $agef = $interval->y;
        $m = substr($agef,-1,1);
        $l = substr($agef,-2,2);
        $agef = $agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

    } else $agef="";
    // country
    $countries = '';

    $countries_array = get_country();

    foreach($countries_array as $countries_arr)
    {
        $countries .='<option value="'.$countries_arr['id'].'">'.$countries_arr['nm'].'</option>';
        if($countries_arr['id']==$u['response']['country'])
            $country = $countries_arr['nm'];
    }

    $sett = array();
    if($u['response']['is_host']==1)
        $sett[2] = "activ_settings_items";
    if($u['response']['is_sponsor']==1)
        $sett[1] = "activ_settings_items";
    if($u['response']['invisible']==1)
        $sett[3] = "activ_settings_items";


    if(isset($u['response']['avatar_id']) && $u['response']['avatar_id']!=null) {
        $av = getImage($u['response']['avatar_id']);
        $avatar = $av['url'];
        if(isset($av['thumb_url']) && $av['thumb_url']!=null)                 $avatar = $av['thumb_url'];
    }else $avatar = $config['defaultAvatarMini'];

    if($u['response']['balance']<0 || $u['response']['balance']=="" || !isset($u['response']['balance']))
        $balance = 0;
    else
        $balance = $u['response']['balance'];
    $style ='';
    $iArray = getUserImages($_SESSION['user']['id']);
    if(count($iArray) == 0)
    {
        $images = '';
        $style = '<style> .media{display:none;} </style>';
    }
    else{
        $images = '';
        foreach($iArray as $img)
        {
            //$url = getImageThumb($img['filename']);
            $url = $img['url'];
            $likes = get_photo_likes($img['id']);
            if(isset($img['thumb_url']) && $img['thumb_url']!=null)                 $url = $img['thumb_url'];
            $urlfull = $img['url'];
            $images .= <<<HTML
            <li><div class="statuses_likes" >
                    <img src="images/likes.png" onclick="setlike({$img['id']})" alt><div onclick="setlike({$img['id']})" class="like_{$img['id']}">{$likes['likes']}</div></div>
                <a href="{$urlfull}" data-lightbox="roadtrip"><img  src="{$url}" style="width: 100px;height: 100px;"></a></li>
HTML;
        }
        $images .= '</ul>';
    }
    $name = $u['response']['first_name']==""?"Без имени":$u['response']['first_name'];


        // ZNAK ZADIAKA

    $m = date("m",strtotime($u['response']['birthday']));
    $d =  date("d",strtotime($u['response']['birthday']));
    $znakzadiakatitle= getZodiacalSign($m,$d);

    $znakzadiakaimg = getZodiacalSignNumber($m,$d).'.png"';

    $dateofbirthday = russianDate($u['response']['birthday']);//date('d F', $u['response']['birthday']);
    if(strpos($dateofbirthday, "00") !== FALSE)
        {
            $dateofbirthday = "Не указана";
            $znakzadiakatitle = "";
            $znakzadiakaimg = '" style="display:none"';
        }



    $directs = '';
    $directions_array = get_interes_directions();

    if(($directions_array!=null) || ($directions_array!="") || (!isset($directions_array)) || (!empty($directions_array))) {

        foreach ($directions_array as $direct) {
            $nm = get_country_name($direct['country_id']);
            $directs .= <<<HTML
         <li id="direction_{$direct['id']}">{$nm}<span onclick='deldeirection({$direct['id']},"$nm")'>x</span></li>
HTML;
        }
    }

    $gg = get_user_gifts($_SESSION['user']['id']);
    $gift_u = '';
    foreach($gg as $b)
    {
        $gift_u .= '<li><img src="'.$b['url'].'"></li>';
    }
    if($gg==null || !isset($gg) || empty($gg))
        $gift_u = <<<HTML
                    <li>Подарков нет</li>
HTML;

	include_once($_SERVER['DOCUMENT_ROOT']."/integration/robokassa/index.php");

	$popol = get_items();
    if($u['response']['sex']=='f') $sex = 'Женский'; else $sex = 'Мужской';
	
    $content .= <<<HTML
    {$style}
        <div class="col-sm-2">
            <div id="profile-avatar">
                <div class="row">
				<div class="col-xs-12">
				<div id="avatar-top">
                <h3 class="btn-add" style=" color: white; position: absolute; border-radius: 50%; margin: -105px 0 0 125px; padding: 9px; font-size: 10px; display:none;">98</h3>

                    <form  type="post" id="avaloadform"  enctype="multipart/form-data" method="post" action="{$config['home']}?action=uploadImageAva"><input type="file"  name="image" onchange="readURL(this);" value="Загрузить фото" id="fileUpload" style="display: none">
						<input type="submit" value="Сохранить">
                    </form>
                    <div class="user_logo_img" id="uploadHandler">
						<!--<img id="userImage" alt="Нажмите что бы загрузить фото" style="    text-align: center;  line-height: 20px;  width: 150px;    display: block;" src="{$avatar}" />-->
						<div class="poster" style="background:url('{$avatar}') no-repeat;"></div>
					</div>
                    <h3>{$name}</h3>
                    <h4>{$agef}</h4>
                    <a href="{$u['home']}?action=editprofile" class="edit_link"><i class="fa fa-pencil"></i> Редактировать профиль</a>
                </div>
				</div>
				</div>
				<div class="row">
				<div class="col-xs-12">
                <div class="balance">
                    Ваш баланс: {$balance} <img src="images/little-logo.png" />
                    <p><button style="    padding: 5px 20px;    border-radius: 20px;" data-toggle="modal" data-target="#popol" class="btn btn-blue">Пополнить</button></p>
                </div>
				<div class="modal fade" id="popol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog modal-sm modal-pop">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><img src="http://poputchiki.ru/images/logo_small.png" width="32" style="margin-right:10px" />Пополнение счета</h4>
					  </div>
					  <div class="modal-body">
						{$popol}
					  </div>
					</div>
				  </div>
				</div>
				</div></div>
                <div class="settings">
                    <div class="settings-item">
                        <button class="sponsorbtn btn {$sett[1]} btn-blue" onclick="changesettings(1)"><i class="fa fa-dollar"></i></button>
                        Включите иконку, если вы готовы оплатить путешествие
                    </div>
                    <div class="settings-item">
                        <button class="hostbtn btn {$sett[2]} btn-blue" onclick="changesettings(2)"><i class="fa fa-home"></i></button>
                        Включите иконку, если вы готовы принять попутчиков у себя дома
                    </div>
                    <div class="settings-item">
                        <button class="invisiblebtn {$sett[3]} btn btn-blue" onclick="changesettings(3)"><i class="fa fa-eye"></i></button>
                        Включите режим Невидимки
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div id="submit_over">
                <div id="submit_status">
                    <span></span>
                    <input type="text" value="{$status[0]['status_text']}" name="status" id="status" placeholder="Введите ваш новый статус! Расскажите друзья о том, куда вы хотите поехать!" />
                </div>
                <button type="button" onclick="status()" class="btn btn-add">Добавить новый статус</button>
            </div>
            <!--<div class="seasons row">
                <h4>Сезоны</h4>
                <p>Укажите сезоны, когда вы хотели бы отдыхать.</p>
                <ul class="list-unstyled seasons-list">
                    <li><button type="button" class="season summer">Лето</button></li>
                    <li><button type="button" class="season autumn">Осень</button></li>
                    <li><button type="button" class="season winter">Зима</button></li>
                    <li><button type="button" class="season spring">Весна</button></li>
                </ul>
            </div>-->
            <div class="about row">
                <div class="row">
                    <div class="col-sm-8">
                        <h4>О себе</h4>
                    </div>
                    <div class="col-sm-4" style="padding-top:10px">
                        <a href="{$u['home']}?action=editprofile" class="edit_link">Редактировать</a>
                    </div>
                </div>
                <div class="about-well">
                    <div  style="    width: 90%;">
                        <table class="table table-about" style="    width: 400px;    margin: 0 auto;">
                            <tr style="border-top:0">
                                <td>Дата рождения:</td>
                                <td>{$dateofbirthday}</td>
                            </tr>
                            <tr>
                                <td>Пол:</td>
                                <td>{$sex}</td>
                            </tr>
                            <tr>
                                <td>Страна:</td>
                                <td>{$u['response']['country']}</td>
                            </tr>
                            <tr>
                                <td>Город:</td>
                                <td>{$u['response']['city']}</td>
                            </tr>
							<tr>
                                <td>Рост:</td>
                                <td>{$u['response']['growth']} м</td>
                            </tr>
							<tr>
                                <td>Вес:</td>
                                <td>{$u['response']['weight']} кг</td>
                            </tr>
                        </table>
                        <p style="    margin: 10px 0 0 50px;">{$u['response']['about']}
                    </div>
                    <div class="sign">
                        <div class="inner">
                            Знак зодиака: <br>
                                {$znakzadiakatitle}
                                <img src="images/png/{$znakzadiakaimg} alt>
                        </div>
                    </div>
                </div>
            </div>
            <div class="countries row">
                <h4>Страны</h4>
                <p>Укажите страны, куда бы Вы хотели поехать отдыхать</p>
                <select name="country_direct" id="countries_direct" class="countries form-control" style="width:200px;margin-bottom:10px;">
                    <option value="select" selected>Добавить страну</option>
                        {$countries}
                </select>
                <ul class="list-unstyled selected_list" id="directions_all">
                    {$directs}
                </ul>
            </div>

            <div class="surprices row">
                <h4>Мои подарки</h4>
                <ul class="list-inline">
                    {$gift_u}
                </ul>
            </div>
            <div class="media row">
                <h4>Фотографии и видео <a style="    float: right;    margin-right: 50px;    font-size: 15px;    text-decoration: none;    cursor: pointer;" class="edit_link" id="btnplus">+ Добавить фото/видео</a> <form  type="post"  enctype="multipart/form-data" method="post" action="{$config['home']}?action=uploadImage2"><input type="file"  name="image2" onchange="readURL2(this);" value="Загрузить фото" id="fileUpload2" style="display: none">
                    <input type="submit" id="avaloadform2" value="Сохранить">
                    </form> </h4>
                <ul class="list-inline">
                    {$images}
                </ul>
            </div>

        </div>

HTML;

    return $content;
}

function viewSite($title,$content,$js=null)
{
    $u = get_user_profile($_SESSION['user']['id']);

    $poputchiki  = poputchikiright();

    $messages = getMessages();
    Global $config;
    $name = $u['response']['first_name']==""?"Без имени":$u['response']['first_name'];

    return <<<HTML
        <html>
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>{$title}</title>
            <script src="//code.jquery.com/jquery-1.10.2.js"></script>
			<script src="/js/alertify.js"></script>
			<script src="/js/loader.js"></script>
			
			<script src="/js/ion/ion.sound.min.js"></script>
			<script type='text/javascript'>
				/* <![CDATA[ */
				var user = {
					"ajax":"{$config['site']}ajax.php",
					"home":"{$config['home']}",
					"id":{$_SESSION['user']['id']}
				};
				/* ]]> */
			</script>

            <script src="//code.jquery.com/jquery-1.10.2.js"></script>
            
            <script src="/js/main.js"></script>
            <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
            <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
			<link rel="stylesheet" href="/js/alert-themes/alertify.core.css">
			<link rel="stylesheet" href="/js/alert-themes/alertify.bootstrap.css">
            <link href="api/lightbox/css/lightbox.css" rel="stylesheet" />
            <script src="//code.jquery.com/jquery-1.10.2.js"></script>
            <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
            <link rel="stylesheet" href="css/style.css" />
            <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
            <script src="api/lightbox/js/lightbox.min.js"></script>

        </head>
        <body>
            <div id="my_notifications">{$messages}</div>

            <nav class="navbar navbar-default navbar-travel">
              <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a style="padding: 0;" class="navbar-brand" href="{$config['home']}">
					<img src="images/logo_small.png" style="  height: 40px;  margin: 5px 0;" />
					<span>Попутчики.ru</span>
				  </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="topmenu">
                  <ul class="nav navbar-nav navbar-right">
                    <li><i class="glyphicon glyphicon-user"></i> {$name}</li>
                      <li><a href="{$config['home']}?action=logout" class="logout"><div class="btn btn-primary btn-sm">Выйти</div></a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container -->
            </nav>

            <div class="col-sm-12" style="    text-align: center;">
                        {$poputchiki}
            </div>

            <div class="col-sm-12">
                <div class="row">
					<div class="col-sm-2">
						<div id="leftmenu">
							<ul class="list-unstyled">
								<li><span><i class="fa fa-user"></i></span><a href="{$config['home']}">Мой профиль</a></li>
								<li><span><i class="fa fa-comment"></i></span><a href="{$config['home']}?action=chat">Сообщения</a> <span class="badge" id="counter">0</span></li>
								<li><span><i class="fa fa-star"></i></span><a href="{$config['home']}?action=favorite">Избранное</a></li>
								<li><span><i class="fa fa-eye"></i></span><a href="{$config['home']}?action=guests">Гости</a></li>
								<!--<li><span><i class="fa fa-signal"></i></span><a href="#">Рейтинг</a></li>-->
								<li><span><i class="fa fa-bars"></i></span><a href="{$config['home']}?action=statuses">Статусы</a></li>
								<li><span><i class="fa fa-user-plus"></i></span><a href="{$config['home']}?action=meeting&all=true">Знакомства</a></li>
								<!--<li><span><i class="fa fa-gear"></i></span><a href="#">Настройки</a></li>-->
							</ul>
						</div>
					</div>
					{$content}
				</div>
            </div>
            <script src="/js/masked.js"></script>
            <script>
           $(document).ready(function() {
$("#phone").mask("+7 (999) 999-99-99");
});
            </script>
            {$js}
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
            
    <!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter27388643 = new Ya.Metrika({id:27388643,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27388643" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    
    
        </body>
        </html>
HTML;
}

function viewSiteStart($title,$js = null)
{
    global $config;
	global $FB_APP_ID;
	
	if($_REQUEST['reg'] == 1){
		$js .= '
			<style>
				.loginDiv {display: none;}
				.regDiv  {display: inline-block;}
			</style>
		';
	}
	
	$messages = getMessages();
	
	$url = urldecode("{$config['site']}login.php?action=vk");
	
    if($_SESSION['sex_reg']=="m")  $check_m = "selected";
    if($_SESSION['sex_reg']=="f")  $check_f = "selected";
    
    
    return <<<HTML
        <html>
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>{$title}</title>
            <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
            <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="css/style.css" />
            <link rel="stylesheet" href="css/index.css" />
            <script type="text/javascript" src="/js/jquery.validate.js"></script>
            <script>
                function showRegLog(what)
                {
                    if(what==1)
                    {
                        $(".regDiv").hide("slow");
                        $(".loginDiv").show("slow");
                        /*$(".regDiv").css("display","none");
                        $(".loginDiv").css("display","inline-block");*/
                        $(".c_2_2").css("border-bottom-left-radius","0");
                        $(".c_2_2").css("border-bottom-right-radius","0");
                    }
                    else if(what==2)
                    {
                        $(".loginDiv").hide("slow");
                        $(".regDiv").show("slow");
                        /*$(".loginDiv").css("display","none");
                        $(".regDiv").css("display","inline-block");*/
                        $(".c_2_2").css("border-bottom-left-radius","15px");
                        $(".c_2_2").css("border-bottom-right-radius","15px");
                    }
                };
            </script>
            <script src="lib/jquery.js"></script>
            <script src="dist/jquery.validate.js"></script>
            <script src="dist/additional-methods.js"></script>

            <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

  
  <script src="/js/main.js"></script>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
            <script src="/js/masked.js"></script>            
            
            <script>
            $(function() {
                $('.phone').mask("+7 (999) 999-99-99");

                $( "#dateofbirthda" ).datepicker({
        dateFormat: 'yy-mm-dd',
        dayNames: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        dayNamesShort: ["Вск", "Пон", "Втр", "Срд", "Чтв", "Птн", "Суб"],
        monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthNamesShort: ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сент", "Окт", "Ноя", "Дек"],
        showMonthAfterYear: true,
        changeYear: true,
        changeMonth: true,
        yearRange: "-50:1997",
        firstDay: 1,
        gotoCurrent: true
    });
    
            });
            
            
            </script>
            {$js}
        </head>
        <body>
            <nav class="navbar navbar-default navbar-travel">
              <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">

                  <a class="navbar-brand" href="{$config['home']}"><img src="images/logo_small.png" height="50" /><span>Попутчики.ru</span></a>
                </div>
        
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="" id="topmenu">
                  <ul class="nav navbar-nav navbar-right">
                    <li style="    display: inline-block;"><a onclick="return showRegLog(1);" class="login"><div class="btn btn-sm">Вход</div></a></li>
                    <li style="    display: inline-block;"><a onclick="return showRegLog(2);" class="reg"><div class="btn btn-sm">Регистрация</div></a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container -->
            </nav>
            <div class="container" style="    min-height: 1050px;">
            <div class="col-sm-6">
                <div class="c_1">
                    <div class="c_1_1"><img src="images/logo.png" alt></div>
                    <div class="c_1_2">Попутчики</div>
                    <div class="c_1_3"></div>
                    <div class="c_1_4">«Попутчики» – социальная сеть, где Вы сможете найти попутчиков и совершить совместные путешествия, получить новые впечатления</div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="c_2 loginDiv">
                    <div class="c_2_1">
                        Войдите в свой аккаунт и ищите своего попутчика!
                    </div>
                    <div class="c_2_2">

                        <form method="POST" name="FormLogin" id="FormLogin" onsubmit="return validateLoginForm()" action="{$config['home']}?action=login">
                        {$messages}
                            <div class="form-group text-left">
								<label>Логин</label>
								<input class="" name="login" id="loginLog"  type="text" value="" placeholder="Логин (Email)">
							</div>
							<div class="form-group text-left">
								<label>Пароль</label>
								<input class="" name="pass" id="passLog"  type="password" value="" placeholder="Пароль">
							</div>
                            <div class="row">
                                <div class="col-sm-6 text-left">
									<a href="{$config['home']}?action=recovery">Забыли пароль?</a>
								</div>
                                <div class="col-sm-6 text-right">
									<label style="cursor:pointer">
										Запомнить
										<input name="rememberMe" class="form-control" type="checkbox" value="" />
									</label>
								</div>
                            </div>                    
                            <input class="form-control" type="submit" value="Войти">
                        </form>
                    </div>
                    <div class="c_2_3">
                        <div class="c_2_3_1">Войти с помощью</div>
                        <div class="c_2_3_2">
                            <a href="https://oauth.vk.com/authorize?client_id=4899076&scope=1&redirect_uri={$url}&response_type=code"><img src="images/sn_vk.png" alt />Вконтакте</a>
                            <a style="display:none" href=""><img src="images/sn_ok.png" alt />Одноклассники</a>
                            <a href="https://facebook.com/dialog/oauth?client_id={$FB_APP_ID}&display=page&response_type=code&scope=email,public_profile&redirect_uri={$config['site']}login.php?action=fb"><img src="images/sn_f.png" alt />Facebook</a>
                        </div>                
                    </div>
                </div>
                <div class="c_2 regDiv">
                    <div class="c_2_1">
                        Зарегистрируй аккаунт и ищите своего попутчика!
                    </div>
                    <div class="c_2_2">

                        <form name="FormReg" id="FormReg" action="{$config['home']}?action=new_user" method="post">
                        {$messages}
                            <div class="form-group text-left">
                                <label>Email</label>
                                <input type="text" name="email" id="email" value="{$_SESSION['email_reg']}" placeholder="Email *" class="" />
                            </div>
                            <div class="form-group text-left">
                                <label>Имя</label>
                                <input type="text" name="first_name" id="first_name" value="{$_SESSION['first_reg']}" placeholder="Имя *" class="" />
                            </div>
                            <div class="form-group text-left">
                                <label>Телефон</label>
                                <input type="text" name="phone" id="phone" value="{$_SESSION['phone_reg']}" placeholder="Телефон (необязательно)" class="phone" />
                            </div>
                            <div class="form-group text-left">
                                <label>Пол</label>
                                <select name="sex" id="sex" class="countries form-control">
                                <option value="0">Укажите пол</option>
                                    <option {$check_m} value="m">Мужской</option>
                                    <option {$check_f} value="f">Женский</option>
                                </select>
                            </div>
                            <div class="form-group text-left">
                                <label>Дата рождения</label>
                                <input type="text" class="" value="{$_SESSION['date_reg']}" id="dateofbirthda" name="dateofbirthday"/>
                            </div>
                            <div class="form-group text-left">
                                <label>Пароль</label>
                                <input type="password" name="password" id="password" value="" placeholder="Пароль *" class="" />
                            </div>
                            <div class="form-group text-left">
                                <label>Подтверждение пароля</label>
                                <input type="password" name="confirm_password" id="confirm_password" value="" placeholder="Подтверждение пароля *" class="form-control" />
                            </div>
							<div class="form- text-left">
								<label style="display:block">А ты робот? Введите текст с картинки</label>
								<img src="{$config['home']}?action=secpic" /></p>
								<input type="text" name="sec" placeholder="Проверочный код" />
							</div>
                            <div class="form-group text-left">
								<input type="hidden" name="post_reg" value="1" />
                                <input type="submit" class="btn btn-default" value="Регистрация" />
                            </div>
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <div class="footer">
                <div>
                    <a href="">О нас</a>
                    <a >|</a>
                    <a href="">Правила</a>
                </div>
            </div>
            <script type="text/javascript">
                //$("#FormReg").validate();

                function validateLoginForm() {
                    var x = document.forms["myForm"]["fname"].value;
                    if (x == null || x == "") {
                        alert("Name must be filled out");
                        return false;
                    }
                }
                function validateRegForm() {
                    var email = document.forms["FormReg"]["email"].value;
                    var phone = document.forms["FormReg"]["phone"].value;
                    var password = document.forms["FormReg"]["password"].value;
                    var password2 = document.forms["FormReg"]["password2"].value;

                    if (email == null || email == "") {
                        alert("Name must be filled out");
                        return false;
                    }
                }
            </script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter27388643 = new Ya.Metrika({id:27388643,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27388643" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    
    
        </body>
        </html>
HTML;

}

function viewSiteStartPassRecovery(){
	
	global $config;
		
	$messages = getMessages();
	
    return <<<HTML
        <html>
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Восстановление пароля</title>
            <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
            <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="css/style.css" />
            <link rel="stylesheet" href="css/index.css" />
            <script type="text/javascript" src="js/jquery.validate.js"></script>
            <script src="lib/jquery.js"></script>
			<script src="/js/main.js"></script>
        </head>
        <body>
            <nav class="navbar navbar-default navbar-travel">
              <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">

                  <a class="navbar-brand" href="{$config['home']}"><img src="images/logo_small.png" height="50" /><span>Попутчики.ru</span></a>
                </div>
        
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="" id="topmenu">
                  <ul class="nav navbar-nav navbar-right">
                    <li style="display: inline-block;"><a href="{$config['home']}" class="login"><div class="btn btn-sm">Вход</div></a></li>
                    <li style="display: inline-block;"><a href="{$config['home']}?reg=1" class="reg"><div class="btn btn-sm">Регистрация</div></a></li>
                  </ul>
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container -->
            </nav>
            <div class="container" style="min-height: 950px;">
            <div class="col-sm-6">
                <div class="c_1">
                    <div class="c_1_1"><img src="images/logo.png" alt></div>
                    <div class="c_1_2">Попутчики</div>
                    <div class="c_1_3"></div>
                    <div class="c_1_4">«Попутчики» – социальная сеть, где Вы сможете найти попутчиков и совершить совместные путешествия, получить новые впечатления</div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="c_2 loginDiv">
                    <div class="c_2_1">
                        Восстановление пароля
                    </div>
                    <div class="c_2_2">
                        <form method="POST" name="FormLogin" id="FormLogin" action="{$config['home']}?action=recovery_submit">
                        {$messages}
                            <div class="form- text-left">
								<label>E-mail</label>
								<input type="text" name="email" placeholder="Введите E-mail" />
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-block btn-default" value="Восстановить" />
							</div>
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <div class="footer">
                <div>
                    <a href="">О нас</a>
                    <a >|</a>
                    <a href="">Правила</a>
                </div>
            </div>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter27388643 = new Ya.Metrika({id:27388643,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27388643" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
    
    
        </body>
        </html>
HTML;
	
}
?>