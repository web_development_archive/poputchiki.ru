<?

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

include("db.php");

ini_set('error_reporting', E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);

define("ROOT", $_SERVER['DOCUMENT_ROOT']);
$config = array(
    'home' => 'http://poputchiki.ru/scndchnc.php',
    'rowPerPage' => 10,
    'uploadAvatar'=>'uploads/images/',
    'images' => '/photos/',
    'thumbs' => '/photos/thumbs/',
    'imagesUpload' => 'photos/',
    'thumbsDir' => 'photos/thumbs/',
    'rowPerPage' => 20,
    'defaultAvatarMini' => 'http://firepic.org/images/2014-07/24/nl3phjadnfm2.png'
);

function startSocketServer()
{
	//if($_SESSION['admin']!=1) return;	
	shell_exec("nohup php /var/www/lelouch/data/www/poputchiki.ru/server/serv.php > /var/www/lelouch/data/www/poputchiki.ru/server/serv.log 2>&1  </dev/null &");		
}
	
function stopSocketServer()
{
	//if($_SESSION['admin']!=1) return;
	shell_exec("kill -TERM $(cat /var/www/lelouch/data/www/poputchiki.ru/server/serv.pid)");
}
	
function killSocketServer() {
	//if($_SESSION['admin']!=1) return;
	shell_exec("pkill -9 -f serv.php");
	//truncateConnections();
	@unlink('/var/www/lelouch/data/www/poputchiki.ru/server/serv.pid');
}

function get_user($user_id){
	
	$q = q("SELECT u.*, p.url as ava FROM users u left join photos p ON (p.id=u.avatar_id) WHERE u.id = :id", array('id' => $user_id));
	return $q[0];
	
}

function localizeDate($tm)
{
		$tm = str_replace("Dec", "дек", $tm);
		$tm = str_replace("Jan", "янв", $tm);
		$tm = str_replace("Feb", "фев", $tm);
		$tm = str_replace("Mar", "мар", $tm);
		$tm = str_replace("Apr", "апр", $tm);
		$tm = str_replace("May", "май", $tm);
		$tm = str_replace("Jun", "июн", $tm);
		$tm = str_replace("Jul", "июл", $tm);
		$tm = str_replace("Aug", "авг", $tm);
		$tm = str_replace("Sep", "сен", $tm);
		$tm = str_replace("Oct", "окт", $tm);
		$tm = str_replace("Nov", "ноя", $tm);	
		return $tm;	
}
	
function fancyTime($tm)
{
		$today = strtotime("00:00:01");
		$yesterday = strtotime("-1 day", $today);
		if($tm>$today)
			return date("H:i", $tm);
		else if ($tm<$today &&$tm>$yesterday)
			return "вчера";
		else return localizeDate(date("d M",$tm));
}

function new_user($email, $pass, $pass2, $phone){
	
	if(empty($email)){
		return array(
			'msg' => 'Вы оставили пустым поле Email',
			'color' => 'red',
			'status' => 0
		);
	}
	
	if(empty($pass)){
		return array(
			'msg' => 'Вы оставили пустым поле Пароль',
			'color' => 'red',
			'status' => 0
		);
	}
	
	if($pass != $pass2){
		return array(
			'msg' => 'Пароли должны совпадать',
			'color' => 'red',
			'status' => 0
		);
	}
	
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		return array(
			'msg' => 'Введен некорректный Email',
			'color' => 'red',
			'status' => 0
		);
	}
	
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	    $ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	    $ip = $_SERVER['REMOTE_ADDR'];
	}
	
	q2("INSERT INTO users(email, password, registration_date, phone, reg_ip) VALUES(:email, :pass, :dt, :phone, :ip)", array('email' => $email, 'pass' => $pass, 'dt' => date('Y-m-d H:i:s'), 'phone' => $phone, 'ip' => $ip));
	
	return array(
		'msg' => 'К сожалению, сайт попутчики.ру находится на реконструкции. Мы обязательно пригласим вас, как только заработает новая версия сайта. Возвращайтесь!',
		'color' => 'green',
		'status' => 1
	);
	
}


?>