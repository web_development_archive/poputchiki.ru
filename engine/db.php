<?php
//$dbConnection = new PDO('mysql:dbname=travel;host=localhost;charset=utf8', 'travel_user', 'gjgenxbrb15');

$dbConnection = new PDO('mysql:dbname=poputchiki;host=localhost;charset=utf8', 'poputchiki', 'travel1777Asia');

$dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbConnection->exec("set names utf8");

function q($sql, $params) // запрос к базе — короткое имя для удобства
{
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);
    $stmt->execute($params);
    $result = $stmt->fetchAll();
    return $result;
}

function q2($sql, $params){ // Используется для insert и update
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);

    if($stmt->execute($params)) return true;
    else return false;
}

function qCount($sql, $params){ // Выводит количество записей
    global $dbConnection;
    $stmt = $dbConnection->prepare($sql);
    $stmt->execute($params);
    return $stmt->fetchColumn();
}

function qInsertId(){ // Последнйи автоинкриментный ID
    global $dbConnection;
    return $dbConnection->lastInsertId();
}



?>