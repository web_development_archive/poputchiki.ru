<?php
/**
 * Created by PhpStorm.
 * User: Asus
 * Date: 08.03.2015
 * Time: 0:55
 */

include_once "api/core.php";
include_once "api/view.php";

$action = $_GET['action'];

if(!isset($_SESSION['user']) && !in_array($action, array('login', 'new_user', 'permit_signup', 'secpic', 'recovery', 'recovery_submit'))){
    echo viewSiteStart("Попутчики");
	die();
}

switch($action)
{
	case 'new_purchase':
		$con = viewConfirmPurchase($_POST['purchase']);
        echo $con;
		die();
		break;
	case 'recovery_submit':
		password_recovery();
		header("Location: {$config['home']}?action=recovery");
		break;
	case 'recovery':
		echo viewSiteStartPassRecovery();
		break;
	case 'secpic':
		include_once "api/secpic/secpic.php";
		break;
	case 'permit_signup':
		if(!empty($_GET['hash'])){
			echo 12;
			activate_user($_GET['hash']);
			header("Location: {$config['home']}");
		}
		break;
    case "chat":
        $con = viewChat();
        echo viewSite("Попутчики. Чат.",$con,null);
        break;
    case 'new_user':

        if($_POST['post_reg'] == 1) {
            $_SESSION['email_reg']= $_POST['email'];
            $_SESSION['sex_reg']= $_POST['sex'];
            $_SESSION['date_reg']= $_POST['dateofbirthday'];
            $_SESSION['phone_reg']= $_POST['phone'];
            $_SESSION['first_reg'] = $_POST['first_name'];
			$user = new_user($_POST['first_name'],$_POST['dateofbirthday'],$_POST['sex'],$_POST['email'], $_POST['password'], $_POST['confirm_password'], $_POST['phone'], $_POST['sec'], true);
			buildMsg($user['msg'], $user['type']);
		}

        if($user['status'] == 0) header("Location: {$config['home']}?reg=1");
		else header("Location: {$config['home']}");
        break;
    case "logout":
        sess_destroy();
        header("Location: {$config['home']}");
        break;
    case "login":
        $user = doLogin($_POST['login'],$_POST['pass'],$_POST['rememberMe']);
		
        header("Location: {$config['home']}");
        break;
    case "statusesload":
        GLOBAL $config;
        $stat='';

        if(!isset($_GET['countries']) || empty($_GET['countries']) || ($_GET['countries']==null))
            $ccc = ""; else $ccc = $_GET['countries'];
        if(!isset($_GET['sex']) || empty($_GET['sex']) || ($_GET['sex']==null))
            $sss = ""; else $sss = $_GET['sex'];
        if(!isset($_GET['city']) || empty($_GET['city']) || ($_GET['city']==null))
            $cicc = ""; else $cicc = $_GET['city'];
        $s = getStatuses2($ccc,$sss,$cicc);

        if(count($s) == 0)
            echo 'Ничего не найдено';
        else{


        foreach($s as $k=>$v)
        {
            if(isset($v['avatar_id']) && $v['avatar_id']!=null) {
                $av = getImage($v['avatar_id']);
                $avatar = $av['url'];
                if(isset($av['thumb_url']) && $av['thumb_url']!=null)
                    $avatar = $av['thumb_url'];

            }else $avatar = $config['defaultAvatarMini'];

            $likes = get_likes($v['stat_id']);

            if (($v['birthday'] != "") && (isset($v['birthday'])) && ($v['birthday']!="0000-00-00"))
            {
                $date = new DateTime($v['birthday']);
                $now = new DateTime();
                $interval = $now->diff($date);
                $agef = $interval->y;
                $m = substr($agef,-1,1);
                $l = substr($agef,-2,2);
                $agef = ", ".$agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

            } else $agef="";
            $name = $v['first_name']==""?"Без имени":$v['first_name'];
            $stat.=<<<HTML
        <div class="statuses">
            <div class="statuses_1">
                <a href="{$config['home']}?action=viewprofile&id={$v['id']}">
                <div class="statuses_1_img"><img src="{$avatar}" alt></div>
                <div>{$name}{$agef}</div></a>
            </div>
            <div class="statuses_2">
                <div>
                    <span></span>
                    {$v['status_text']}
                </div>
                <div class="statuses_likes" >
                    <img src="images/likes.png" onclick="setlike({$v['stat_id']})" alt><div onclick="setlike({$v['stat_id']})" class="like_{$v['stat_id']}">{$likes['likes']}</div>
                </div>
            </div>
        </div>
HTML;
        }
    echo $stat;

        }
    break;
    case "statuses":

        $js = <<<HTML
        <style>
         .overlay{;position: fixed!important;width: 100%!important;background: black!important;height: 100%!important;}
         </style>
        <script type="text/javascript">
        $( document ).ready(function() {

            $(".statuses_2_text").each(function() {

                len = $(this).text().length;
                if(len>170)
                {
                    var shorttext = $(this).text().substring(0, 170);
                    $(this).text(shorttext+" ...");

                }
            });

$(".statuses_2_text").click(function(){

    //$(this).next(".statuses_2_text_all").toggleClass("status_full_texts");    

    $(this).next(".statuses_2_text_all").children('.popup').css('opacity','1');
    $(this).next(".statuses_2_text_all").children('.overlay').css('opacity','0.4');
    $(this).next(".statuses_2_text_all").children('.popup, .overlay').css('visibility','visible');
});
$(".overlay, .popup").click(function(){
    $(".overlay, .popup").css('opacity','0');
    $(".overlay, .popup").css('visibility','hidden'); 
    //$(this).toggleClass("status_full_texts");    
});



                    stat =  $("#countries").val();
                    var countries = '<option value="" selected>Город</option>';
                    $.ajax({
                        url: '{$config['home']}?action=getcities',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){

                            var gla = $.parseJSON(data);

                            jQuery.each( gla, function( key, value ) {
                               countries +="<option value=\""+value["nm"]+"\">"+value["nm"]+"</option>";
                            });

                            $("#cities").html(countries );
                        }
                    });
            $('#searchstats').keyup(function() {
                  var stat =  $("#searchstats").val();
                    $.ajax({
                        url: '{$config['home']}?action=getstatuslikes',
                        type: 'post',
                        data: {likes: stat},
                        dataType: "json",
                        success: function(data){
                            var countries = "";
                            var gla = JSON.parse(data);
                            alert(gla);
                            /*var gla = JSON.parse(data);
                            $.each( gla, function( key, value ) {
                            });*/
                            $(".allstats").html(countries );
                        }
                    });
                });
        });

                $( "#countries" ).change(function() {
                  stat =  $("#countries").val();
                  sex =  $("#sex").val();
                  city =  $("#cities").val();
                    var countries = '<option value="" selected>Город</option>';
                    $.ajax({
                        url: '{$config['home']}?action=getcities',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){
                            var gla = $.parseJSON(data);
                            jQuery.each( gla, function( key, value ) {
                               countries +="<option value=\""+value["nm"]+"\">"+value["nm"]+"</option>";
                            });
                            $("#cities").html(countries );
                            $.get("{$config['home']}?action=statusesload&countries="+stat+"&sex="+sex+"&city="+city, function(loaded){
                                $('.allstats').html(loaded);
                             });
                        }
                    });
                });
                $( "#sex" ).change(function() {
                  stat =  $("#countries").val();
                  sex =  $("#sex").val();
                  city =  $("#cities").val();
                    $.get("{$config['home']}?action=statusesload&countries="+stat+"&sex="+sex+"&city="+city, function(loaded){
                        $('.allstats').html(loaded);
                     });
                });
                $( "#cities" ).change(function() {
                  stat =  $("#countries").val();
                  sex =  $("#sex").val();
                  city =  $("#cities").val();
                    $.get("{$config['home']}?action=statusesload&countries="+stat+"&sex="+sex+"&city="+city, function(loaded){
                        $('.allstats').html(loaded);
                     });
                });
           function setlike(like_id) {
                id =  like_id;
                $.ajax({
                    url: '{$config['home']}?action=likecheck',
                    type: 'post',
                    data: {u_id: id},
                    success: function(data){

                        if(data==false) $(".like_"+id).html(0);
                        else $(".like_"+id).html(data);

                    }
                });
            };
        </script>
HTML;

        $con = viewUserStatuses();
        echo viewSite("Попутчики. Статусы.",$con,$js);
        break;
    case "editpassshow":
        $con = viewEditUserPassword(getUser($_SESSION['user']['id']));
        $js = <<<HTML
            <script type="text/javascript">
            $( document ).ready(function() {
                 stat =  $("#countries").val();
                    $.ajax({
                        url: '{$config['home']}?action=getcities',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){
                            var gla = $.parseJSON(data);
                            var countries = "";
                            jQuery.each( gla, function( key, value ) {
                               countries +="<option value=\""+value["nm"]+"\">"+value["nm"]+"</option>";
                            });
                            $("#cities").html(countries );
                        }
                    });
            });
                </script>
HTML;

        echo viewSite("Попутчики. Изменить пароль",$con,$js);
        break;
    case "editprofile":
        $con = viewEditUserForm(getUser($_SESSION['user']['id']));
        $u = getUser($_SESSION['user']['id']);
        $js = <<<HTML

            <style>
                a.btn.btn-default {
                  margin: 10px;
                }
            </style>
            <script type="text/javascript">
            
            function readURL(input) {
                    if (input.files && input.files[0]) {

                        var reader = new FileReader();


                        reader.onload = function (e) {

                            $('#userImage')
                                .attr('src', e.target.result)
                                .width(225);

                        };

                        reader.readAsDataURL(input.files[0]);

                        $("#avaloadform input[type='submit']").click();
                    }



                }

            $( document ).ready(function() {



                $( "#birthday" ).datepicker({
        dateFormat: 'yy-mm-dd',
        dayNames: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        dayNamesShort: ["Вск", "Пон", "Втр", "Срд", "Чтв", "Птн", "Суб"],
        monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthNamesShort: ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сент", "Окт", "Ноя", "Дек"],
        showMonthAfterYear: true,
        changeYear: true,
        changeMonth: true,
        yearRange: "-50:+10",
        firstDay: 1,
        gotoCurrent: true
    });

                

                $('#userImage').click(function(){
                        $('#fileUpload').click();

                    
                }); 

                
                 stat =  $("#countries").val();
                    $.ajax({
                        url: '{$config['home']}?action=getcities',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){
                            var gla = $.parseJSON(data);
                            var countries = "";
                            jQuery.each( gla, function( key, value ) {
                                if(value["nm"]=="{$u['city']}")
                                countries +="<option selected value=\""+value["nm"]+"\">"+value["nm"]+"</option>";     
                               else countries +="<option value=\""+value["nm"]+"\">"+value["nm"]+"</option>";
                            });
                            $("#cities").html(countries );
                        }
                    });
            });
            $( "#countries" ).change(function() {
                  stat =  $("#countries").val();
                    $.ajax({
                        url: '{$config['home']}?action=getcities',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){
                            var gla = $.parseJSON(data);
                            var countries = "";
                            jQuery.each( gla, function( key, value ) {
                               countries +="<option value=\""+value["nm"]+"\">"+value["nm"]+"</option>";
                            });
                            $("#cities").html(countries );
                        }
                    });
                });
                </script>
HTML;

        echo viewSite("Попутчики. Редактирование профиля",$con,$js);
        break;
    case "showimages":
        $con = showImagesOfProfile(getUser($_SESSION['user']['id']));

        echo viewSite("Попутчики. Фотографии профиля",$con);
        break;
    case "doedituser":
        global $config;
        if(is_numeric($_POST['user_id'])) {
            if (doEditUser($_POST)) header("Location: " . $config['home']);
            else header("Location: " . $config['home'] . "?action=editprofile");
        }
        break;
    case "doeditpass":
        global $config;

        if (editopassofuser($_POST['passwordnew'],$_POST['passwordnew2'],$_POST['password'])) header("Location: " . $config['home']);
        else header("Location: " . $config['home'] . "?action=editpassshow");

        break;
    case "setstatus":
            echo set_status($_POST['status_text'],$_SESSION['user']['id']);
        break;
    case "getcities":
            echo get_city($_POST['country_id']);
        break;
    case "getstatuslikes":
        echo get_statuses($_POST['likes']);
        break;
    case "updateinhospon":
        if($_POST['stat']==1)
            echo set_sponsor_status($_SESSION['user']['id']);
        if($_POST['stat']==2)
            echo set_host_status($_SESSION['user']['id']);
        if($_POST['stat']==3)
            echo set_invisible_status($_SESSION['user']['id']);
        break;
    case "updatemalefemaleinteres":
        if($_POST['stat']==2)
            echo set_interes_male($_SESSION['user']['id']);
        if($_POST['stat']==1)
            echo set_interes_female($_SESSION['user']['id']);
        break;
    case "updateprefercountry":
            echo set_interes_country($_POST['country_id']);
        break;
    case "updateprefercity":
        echo set_interes_city($_POST['country_id']);
        break;
    case "updatepreferage":
        echo set_interes_age($_POST['age1'],$_POST['age2']);
        break;
    case "updatepreferwidth":
        echo set_interes_width($_POST['age1'],$_POST['age2']);
        break;
    case "updatepreferdirection":
        echo set_interes_directions($_POST['direction']);
        break;
    case "delpreferdirection":
        echo del_interes_directions($_POST['direction']);
        break;
    case "addtopoputchik":
        echo set_poputchik($_POST['image_id']);
        header("Location: ".$config['home']);
        break;
    case "addtofav":
        echo add_to_fav($_POST['u_id']);
        break;
    case "likecheck":
        echo add_to_fav($_POST['u_id']);
        break;
    case "photolikecheck":
        echo photo_likes_check($_POST['u_id']);
        break;
    case "delfromfav":
        echo delete_from_fav($_POST['u_id']);
        break;
    case "favorite":
        $con = viewUserFavorites(get_favorite($_SESSION['user']['id']));
        $js=<<<HTML
        <script type="text/javascript">

        function fav(id)
                 {
                $.ajax({
                    url: '{$config['home']}?action=addtofav',
                    type: 'post',
                    data: {u_id: id},
                    success: function(data){
                        if(data==false || data==null)
                        {
                            $("#my_notifications").html("<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Пользователь успешно удален из избранных!</div>");
                            //$(this).text("+ Добавиль");
                            location.reload(true);
                        }else if(data==true)
                        {
                            $("#my_notifications").html("<div class=\"alert alert-success alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Пользователь добавлен в избранное!</div>");
                            //$(this).text("- Удалить");
                            location.reload(true);
                        }

                    }
                });
            };
            </script>
HTML;

        echo viewSite("Попутчики. Избранные статусы.",$con,$js);
        break;
    case "guests":
        $con = viewUserGuests(get_visithors());
        echo viewSite("Попутчики. Избранные статусы.",$con,$js);
        break;
    case "sendgift":
        echo send_gift($_POST['gift_id'],$_POST['user_id']);
        break;
	case "sm_user_message":
		echo json_encode(send_msg_in_profile($_POST['user_id'], $_POST['txt']));
		break;
    case "viewprofile":
		global $config;
		
        set_visitors($_GET['id']);

        $js = <<<HTML
			<script type="text/javascript" src="/js/validation/jquery.validate.min.js"></script>
			<script type="text/javascript" src="/js/validation/localization/messages_ru.min.js"></script>
            <script type="text/javascript">

			$(function(){
				$('form#send_msg').validate({
					lang: 'ru',
					rules: {
						sm_text: {
							required: true
						}
					},
					submitHandler: function(form){
						var form = $(this);
						
						/*
						var me = form.find('input[type=submit]');

						me.isLoading({
							position: 'inside',
							class: 'fa fa-refresh fa-spin',
							text: 'Загрузка'
						});
						*/
				
						$.ajax({
							type: 'POST',
							data: {
								user_id: $('#send_msg #sm_user_id').val(),
								txt: $('#send_msg #sm_text').val()
							},
							url: '{$config['home']}?action=sm_user_message',
							dataType: 'json',
							success:function(data){
								if(data.status == 0) alertify.error(data.msg);
								else if(data.status == 1) {
									alertify.success(data.msg);
									setInterval(function(){
										$('#send_msg2').modal('hide');
									}, 1000);
								}
							}
						});
						
						//me.isLoading("hide");
						//me.html("Отправить");
						
						return false;
					}
				});
			});
			
            function setlike(like_id) {
                id =  like_id;
                $.ajax({
                    url: '{$config['home']}?action=photolikecheck',
                    type: 'post',
                    data: {u_id: id},
                    success: function(data){

                        if(data==false) $(".like_"+id).html(0);
                        else $(".like_"+id).html(data);

                    }
                });
            };
            $( "#addtofav" ).click(function() {
                 id =  {$_GET['id']};
                $.ajax({
                    url: '{$config['home']}?action=addtofav',
                    type: 'post',
                    data: {u_id: id},
                    success: function(data){
                        if(data==false || data==null)
                        {
                            $("#my_notifications").html("<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Пользователь успешно удален из избранных!</div>");
                            $("#addtofav").html("Добавиль в избранное");
                        }else if(data==true)
                        {
                            $("#my_notifications").html("<div class=\"alert alert-success alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Пользователь добавлен в избранное!</div>");
                            $("#addtofav").html("Удалить из избранных");
                        }
                    }
                });
            });

            //$(document).ready(function(){
                $('.popup3 .close_window, .overlay3').click(function (){
                $('.popup3, .overlay3').css('opacity','0');
                $('.popup3, .overlay3').css('visibility','hidden');
                });
                $("#btnplus").click(function (e){
                $('.popup3, .overlay3').css('opacity','1');
                $('.popup3, .overlay3').css('visibility','visible');
                e.preventDefault();
                });
            //});

            function sendgift(id)
            {
                $.ajax({
                    url: '{$config['home']}?action=sendgift',
                    type: 'post',
                    data: {gift_id: id,user_id: {$_GET['id']}},
                    success: function(data){
                        if(data==false)
                        {
                            $('.popup3, .overlay3').css('opacity','0');
                            $('.popup3, .overlay3').css('visibility','hidden');
                            $("#my_notifications").html("<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>У вас недостаточно средств!</div>");
                        }else
                        {
                            $('.popup3, .overlay3').css('opacity','0');
                            $('.popup3, .overlay3').css('visibility','hidden');
                            $("#my_notifications").html("<div class=\"alert alert-success alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Вы подарили подарок!</div>");
                        }
                    }
                });
            };
            </script>
HTML;

        $con = viewUserGuest($_GET['id']);

        echo viewSite("Попутчики. Страница профиля",$con,$js);
        break;

    case "deleteImage":
        global $config;
        if(is_numeric($_GET['image_id'])){
            deleteImage($_GET['image_id']);
            header("Location: " . $config['home'] . "?action=showimages");
        }
        break;
    case "uploadImage":
        global $config;
        //echo "FILESimage".$_FILES['image']."<hr />";
        if(uploadImage($_FILES['image'], array('using' => false, 'type' => 'user', 'target' => $_SESSION['user']['id'])))
            header("Location: ". $config['home'] ."?action=showimages");
        else echo "bad =(";
        break;
    case "uploadImage2":
        global $config;
        //echo "FILESimage".$_FILES['image']."<hr />";
        if(uploadImage($_FILES['image2'], array('using' => false, 'type' => 'user', 'target' => $_SESSION['user']['id'])))
            header("Location: ". $config['home']);
        else echo "bad =(";
        break;
    case "uploadImageAva":
        global $config;
        if(uploadImage($_FILES['image'], array('using' => true, 'type' => 'user', 'target' => $_SESSION['user']['id'])))
            header("Location: ". $config['home']);
        else header("Location: ". $config['home']);

        break;
    case "uploadImageAva2":
        global $config;
        if(uploadImage($_FILES['image'], array('using' => true, 'type' => 'user', 'target' => $_SESSION['user']['id'])))
            header("Location: ". $config['home']."?action=editprofile");
        else header("Location: ". $config['home']."?action=editprofile");

        break;
    case "interes":
        global $config;

        $pref = get_preferences($_SESSION['user']['id']);
        if($pref=="" || $pref==null || !isset($pref))
        {
            createInteres($_SESSION['user']['id']);
        }
        $width_f = ($pref['width_f']!=null || $pref['width_f']!="")?$pref['width_f']:0;
        $width_s= ($pref['width_s']!=null || $pref['width_s']!="")?$pref['width_s']:90;

        $age_f = ($pref['age_f']!=null || $pref['age_f']!="")?$pref['age_f']:0;
        $age_s= ($pref['age_s']!=null || $pref['age_s']!="")?$pref['age_s']:90;
        //if($pref['city_id']!="" || $pref['city_id']!=null || isset($pref['city_id']))
        $jjs = <<<HTML
            if(value["id"]=={$pref['city_id']})
            {
                   countries +="<option selected value=\""+value["id"]+"\">"+value["nm"]+"</option>";
            }else    countries +="<option value=\""+value["id"]+"\">"+value["nm"]+"</option>";
HTML;

            $js = <<<HTML
            <script type="text/javascript">

            $(function() {
                $( "#slider-range" ).slider({
                  range: true,
                  min: 18,
                  max: 90,
                  stop: function( event, ui ) {
                      $.ajax({
                        url: '{$config['home']}?action=updatepreferage',
                        type: 'post',
                        data: {age1: ui.values[ 0 ], age2: ui.values[ 1 ]},
                        success: function(data){

                        }
                    });
                  },
                  values: [ {$age_f}, {$age_s} ],
                  slide: function( event, ui ) {

                    $( "#amount1" ).val(ui.values[ 0 ]+" лет");
                    $( "#amount2" ).val(ui.values[ 1 ]+" лет");

                  }
                });

                $( "#amount1" ).val($( "#slider-range" ).slider( "values", 0 )+" лет");
                $( "#amount2" ).val($( "#slider-range" ).slider( "values", 1 )+" лет");
            });
                $(function() {
                    $( "#slider-range2" ).slider({
                      range: true,
                      min: 35,
                      max: 90,
                      stop: function( event, ui ) {
                        $.ajax({
                            url: '{$config['home']}?action=updatepreferwidth',
                            type: 'post',
                            data: {age1: ui.values[ 0 ], age2: ui.values[ 1 ]},
                            success: function(data){

                            }
                        });
                      },
                      values: [ {$width_f}, {$width_s} ],
                      slide: function( event, ui ) {
                        $( "#amount2_1" ).val(ui.values[ 0 ]+" кг");
                        $( "#amount2_2" ).val(ui.values[ 1 ] +" кг");
                      }
                    });

                    $( "#amount2_1" ).val($( "#slider-range2" ).slider( "values", 0 )+" кг");
                    $( "#amount2_2" ).val($( "#slider-range2" ).slider( "values", 1 )+" кг");
                });


            $( document ).ready(function() {

                $("#poiskcountries").click(function(){
                    location.reload();
                });

                $("#resetcountries").click(function(){
                    $.ajax({
                        url: '{$config['home']}?action=updateprefercountry',
                        type: 'post',
                        data: {country_id: 0},
                        success: function(data){
                            reset = '<option selected value="0">Не задано</option>';
                            var bla = $("#countries").html();
                            $("#countries").html(bla+reset);
                        }
                    });
                });
                $("#resetcities").click(function(){
                    $.ajax({
                        url: '{$config['home']}?action=updateprefercity',
                        type: 'post',
                        data: {country_id: 0},
                        success: function(data){
                            reset = '<option selected value="0">Не задано</option>';
                            var bla = $("#cities").html();
                            $("#cities").html(bla+reset);

                        }
                    });
                });
                 stat =  $("#countries").val();
                    $.ajax({
                        url: '{$config['home']}?action=getcities',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){
                            var gla = $.parseJSON(data);
                            var countries = "";
                            jQuery.each( gla, function( key, value ) {
                            {$jjs}
                               //countries +="<option value=\""+value["id"]+"\">"+value["nm"]+"</option>";
                            });
                            countries += '<option value="0">Не задано</option>';
                            $("#cities").html(countries );
                        }
                    });
            });
            $( "#countries" ).change(function() {
                  stat =  $("#countries").val();
                    $.ajax({
                        url: '{$config['home']}?action=getcities',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){
                            var gla = $.parseJSON(data);
                            var countries = "";
                            jQuery.each( gla, function( key, value ) {
                               countries +="<option value=\""+value["id"]+"\">"+value["nm"]+"</option>";
                            });
                            $("#cities").html(countries );
                        }
                    });
                    $.ajax({
                        url: '{$config['home']}?action=updateprefercountry',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){

                        }
                    });
                });

                $( "#cities" ).change(function() {
                  stat =  $("#cities").val();
                    $.ajax({
                        url: '{$config['home']}?action=updateprefercity',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){

                        }
                    });
                });

            function status() {
stat =  $("#status").val();

                    if(stat=="")
                    {
                        $("#my_notifications").html("<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Статус должен содержать текст!</div>");    
                    }else
                    {                    
                
                $.ajax({
                    url: '{$config['home']}?action=setstatus',
                    type: 'post',
                    data: {status_text: stat},
                    success: function(data){

                        $("#status").val(data );
                    }
                });
}

            };

        function changesettingssex(c)
        {
            $.ajax({
                url: '{$config['home']}?action=updatemalefemaleinteres',
                type: 'post',
                data: {stat: c},
                success: function(data){
                    if(data==true)
                    {
                        if(c==2)
                        {

                            $(".sexmale").attr("checked");
                        }
                        if(c==1)
                        {
                            $( ".hostbtn" ).addClass( "activ_settings_items" );

                            $(".sexfemale").attr("checked");
                        }
                    }
                    else if(data==false)
                    {
                        if(c==2)
                        {
                           $( ".sponsorbtn" ).removeClass( "activ_settings_items" );

                            $(".sexmale").prop("checked");
                        }
                        if(c==1)
                        {
                           $( ".hostbtn" ).removeClass( "activ_settings_items" );

                           $(".sexfemale").prop("checked");
                        }

                    }
                    //$("#status").val(data );
                }
            });
        }

        function changesettings(c)
        {
            $.ajax({
                url: '{$config['home']}?action=updateinhospon',
                type: 'post',
                data: {stat: c},
                success: function(data){
                    if(data==true)
                    {
                        if(c==1)
                        {
                            $( ".sponsorbtn" ).addClass( "activ_settings_items" );

                            $(".sponsor").attr("checked");
                        }
                        if(c==2)
                        {
                            $( ".hostbtn" ).addClass( "activ_settings_items" );

                            $(".guests").attr("checked");
                        }
                        if(c==3)
                        {
                           $( ".invisiblebtn" ).addClass( "activ_settings_items" );

                        }
                    }
                    else if(data==false)
                    {
                        if(c==1)
                        {
                           $( ".sponsorbtn" ).removeClass( "activ_settings_items" );

                            $(".sponsor").prop("checked");
                        }
                        if(c==2)
                        {
                           $( ".hostbtn" ).removeClass( "activ_settings_items" );

                           $(".guests").prop("checked");
                        }
                        if(c==3)
                        {
                           $( ".invisiblebtn" ).removeClass( "activ_settings_items" );

                        }

                    }
                    //$("#status").val(data );
                }
            });
        }

        function deldeirection(c,n)
        {
            $.ajax({
                url: '{$config['home']}?action=delpreferdirection',
                type: 'post',
                data: {direction: c},
                success: function(data){

                    $("#direction_"+c).remove();
                }
            });
        }

        $( "#countries_direct" ).change(function() {
                stat =  $("#countries_direct").val();
                nm =  $("#countries_direct option[value='"+stat+"']").text();
                if(stat!="select")
                {
                    $.ajax({
                        url: '{$config['home']}?action=updatepreferdirection',
                        type: 'post',
                        data: {direction: stat},
                        success: function(data){
                        if(data!=false)
                        {

                            $("#directions_all").append('<li id="direction_'+data+'">'+nm+'<span onclick=\'deldeirection('+data+',"'+nm+'")\'>x</span></li>');
                        }else
                        {

                        }

                        }
                    });
                }
                });
            </script>
HTML;
        $con = viewUserInteres($_SESSION['user']['id']);

        echo viewSite("Попутчики. Страница профиля",$con,$js);

        break;
    case "load_meeting":
    global $config;
    $s = get_users2($_GET['start'],$_GET['size']);
    foreach($s as $k=>$v) {
        if (isset($v['avatar_id']) && $v['avatar_id'] != null) {
            $av = getImage($v['avatar_id']);
            $avtr = $av['url'];
            if (isset($av['thumb_url']) && $av['thumb_url'] != null)
                $avtr = $av['thumb_url'];
        } else continue;//continue;//$avtr = $config['defaultAvatarMini'];


        if (($v['birthday'] != "") && (isset($v['birthday'])) && ($v['birthday']!="0000-00-00"))
        {
            $date = new DateTime($v['birthday']);
            $now = new DateTime();
            $interval = $now->diff($date);
            $agef = $interval->y;
            $m = substr($agef,-1,1);
            $l = substr($agef,-2,2);
            $agef = ", ".$agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

        } else $agef="";

        $name = $v['first_name']==""?"Без имени":$v['first_name'];

        $stat.=<<<HTML
            <div class="statuses_1" style="    width: 20%;">
                <a href="{$config['home']}?action=viewprofile&id={$v['id']}">
                <div class="statuses_1_img"><img src="{$avtr}" alt></div>
                <div>{$name}{$agef}</div></a>
            </div>
HTML;

    }
    echo $stat;
    break;
    case "load_meeting_all":
        global $config;
        $s = get_users($_GET['start'],$_GET['size']);
        foreach($s as $k=>$v) {
            if (isset($v['avatar_id']) && $v['avatar_id'] != null) {
                $av = getImage($v['avatar_id']);
                $avtr = $av['url'];
                if (isset($av['thumb_url']) && $av['thumb_url'] != null)
                    $avtr = $av['thumb_url'];
            } else continue;//$avtr = $config['defaultAvatarMini'];


            if (($v['birthday'] != "") && (isset($v['birthday'])) && ($v['birthday']!="0000-00-00"))
            {
                $date = new DateTime($v['birthday']);
                $now = new DateTime();
                $interval = $now->diff($date);
                $agef = $interval->y;
                $m = substr($agef,-1,1);
                $l = substr($agef,-2,2);
                $agef = ", ".$agef. ' ' .((($m==1)&&($l!=11))?'год':((($m==2)&&($l!=12)||($m==3)&&($l!=13)||($m==4)&&($l!=14))?'года':'лет'));

            } else $agef="";

            $name = $v['first_name']==""?"Без имени":$v['first_name'];

            $stat.=<<<HTML
            <div class="statuses_1" style="    width: 20%;">
                <a href="{$config['home']}?action=viewprofile&id={$v['id']}">
                <div class="statuses_1_img"><img src="{$avtr}" alt></div>
                <div>{$name}{$agef}</div></a>
            </div>
HTML;

        }
        echo $stat;
        break;
    case "meeting":

        global $config;
		
		if(!isset($_SESSION['user'])){
            echo viewSiteStart("Попутчики");
			return;
		}
		
        if(isset($_GET['all']))
        {
            $con = viewUsers(1);
            $url = $config['home']."?action=meeting";
            $load_page = "load_meeting_all";
        }else
        {
            $con = viewUsers();
            $url = $config['home']."?action=meeting&all=true";
            $load_page = "load_meeting";
            $pjpjpj = '$("#interesses").css("display","block");';
        }

        $pref = get_preferences($_SESSION['user']['id']);
        if($pref=="" || $pref==null || !isset($pref))
        {
            createInteres($_SESSION['user']['id']);
        }
        $width_f = ($pref['width_f']!=null || $pref['width_f']!="")?$pref['width_f']:0;
        $width_s= ($pref['width_s']!=null || $pref['width_s']!="")?$pref['width_s']:90;

        $age_f = ($pref['age_f']!=null || $pref['age_f']!="")?$pref['age_f']:0;
        $age_s= ($pref['age_s']!=null || $pref['age_s']!="")?$pref['age_s']:90;
        $jjs = <<<HTML
            if(value["id"]=={$pref['city_id']})
            {
                   countries +="<option selected value=\""+value["id"]+"\">"+value["nm"]+"</option>";
            }else    countries +="<option value=\""+value["id"]+"\">"+value["nm"]+"</option>";
HTML;

        //if($pref['city_id']!="" || $pref['city_id']!=null || isset($pref['city_id']))

        $js = <<<HTML
        <script type="text/javascript">

        $(function() {
            
                $( "#slider-range" ).slider({
                  range: true,
                  min: 18,
                  max: 90,
                  stop: function( event, ui ) {
                      $.ajax({
                        url: '{$config['home']}?action=updatepreferage',
                        type: 'post',
                        data: {age1: ui.values[ 0 ], age2: ui.values[ 1 ]},
                        success: function(data){

                        }
                    });
                  },
                  values: [ {$age_f}, {$age_s} ],
                  slide: function( event, ui ) {

                    $( "#amount1" ).val(ui.values[ 0 ]+" лет");
                    $( "#amount2" ).val(ui.values[ 1 ]+" лет");


                  }
                });

                $( "#amount1" ).val($( "#slider-range" ).slider( "values", 0 )+" лет");
                $( "#amount2" ).val($( "#slider-range" ).slider( "values", 1 )+" лет");

            });
                $(function() {
                    $( "#slider-range2" ).slider({
                      range: true,
                      min: 35,
                      max: 90,
                      stop: function( event, ui ) {
                        $.ajax({
                            url: '{$config['home']}?action=updatepreferwidth',
                            type: 'post',
                            data: {age1: ui.values[ 0 ], age2: ui.values[ 1 ]},
                            success: function(data){

                            }
                        });
                      },
                      values: [ {$width_f}, {$width_s} ],
                      slide: function( event, ui ) {
                        $( "#amount2_1" ).val(ui.values[ 0 ]+" кг");
                        $( "#amount2_2" ).val(ui.values[ 1 ] +" кг");

                      }
                    });

                    $( "#amount2_1" ).val($( "#slider-range2" ).slider( "values", 0 )+" кг");
                    $( "#amount2_2" ).val($( "#slider-range2" ).slider( "values", 1 )+" кг");
                });


            $( document ).ready(function() {
                {$pjpjpj}

                $("#resetcountries").click(function(){
                    $.ajax({
                        url: '{$config['home']}?action=updateprefercountry',
                        type: 'post',
                        data: {country_id: 0},
                        success: function(data){
                            reset = '<option selected value="0">Не задано</option>';
                            var bla = $("#countries").html();
                            $("#countries").html(bla+reset);
                        }
                    });
                });
                $("#resetcities").click(function(){
                    $.ajax({
                        url: '{$config['home']}?action=updateprefercity',
                        type: 'post',
                        data: {country_id: 0},
                        success: function(data){
                            reset = '<option selected value="0">Не задано</option>';
                            var bla = $("#cities").html();
                            $("#cities").html(bla+reset);

                        }
                    });
                });
                 stat =  $("#countries").val();
                    $.ajax({
                        url: '{$config['home']}?action=getcities',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){
                            var gla = $.parseJSON(data);
                            var countries = "";
                            jQuery.each( gla, function( key, value ) {
                            {$jjs}
                               //countries +="<option value=\""+value["id"]+"\">"+value["nm"]+"</option>";
                            });
                            countries += '<option value="0">Не задано</option>';
                            $("#cities").html(countries );
                        }
                    });
            });
            $( "#countries" ).change(function() {
                  stat =  $("#countries").val();
                    $.ajax({
                        url: '{$config['home']}?action=getcities',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){
                            var gla = $.parseJSON(data);
                            var countries = "";
                            jQuery.each( gla, function( key, value ) {
                               countries +="<option value=\""+value["id"]+"\">"+value["nm"]+"</option>";
                            });
                            $("#cities").html(countries );
                        }
                    });
                    $.ajax({
                        url: '{$config['home']}?action=updateprefercountry',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){


                        }
                    });
                });

                $( "#cities" ).change(function() {
                  stat =  $("#cities").val();
                    $.ajax({
                        url: '{$config['home']}?action=updateprefercity',
                        type: 'post',
                        data: {country_id: stat},
                        success: function(data){
                        }
                    });
                });

            function status() {
stat =  $("#status").val();

                    if(stat=="")
                    {
                        $("#my_notifications").html("<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Статус должен содержать текст!</div>");    
                    }else
                    {                    
                
                $.ajax({
                    url: '{$config['home']}?action=setstatus',
                    type: 'post',
                    data: {status_text: stat},
                    success: function(data){

                        $("#status").val(data );
                    }
                });
}

            };

        function changesettingssex(c)
        {
            $.ajax({
                url: '{$config['home']}?action=updatemalefemaleinteres',
                type: 'post',
                data: {stat: c},
                success: function(data){
                    if(data==true)
                    {
                        if(c==2)
                        {

                            $(".sexmale").attr("checked");
                        }
                        if(c==1)
                        {
                            $( ".hostbtn" ).addClass( "activ_settings_items" );

                            $(".sexfemale").attr("checked");
                        }
                    }
                    else if(data==false)
                    {
                        if(c==2)
                        {
                           $( ".sponsorbtn" ).removeClass( "activ_settings_items" );

                            $(".sexmale").prop("checked");
                        }
                        if(c==1)
                        {
                           $( ".hostbtn" ).removeClass( "activ_settings_items" );

                           $(".sexfemale").prop("checked");
                        }

                    }
                    //$("#status").val(data );
                }
            });
        }

        function changesettings(c)
        {
            $.ajax({
                url: '{$config['home']}?action=updateinhospon',
                type: 'post',
                data: {stat: c},
                success: function(data){
                    if(data==true)
                    {
                        if(c==1)
                        {
                            $( ".sponsorbtn" ).addClass( "activ_settings_items" );

                            $(".sponsor").attr("checked");
                        }
                        if(c==2)
                        {
                            $( ".hostbtn" ).addClass( "activ_settings_items" );

                            $(".guests").attr("checked");
                        }
                        if(c==3)
                        {
                           $( ".invisiblebtn" ).addClass( "activ_settings_items" );

                        }
                    }
                    else if(data==false)
                    {
                        if(c==1)
                        {
                           $( ".sponsorbtn" ).removeClass( "activ_settings_items" );

                            $(".sponsor").prop("checked");
                        }
                        if(c==2)
                        {
                           $( ".hostbtn" ).removeClass( "activ_settings_items" );

                           $(".guests").prop("checked");
                        }
                        if(c==3)
                        {
                           $( ".invisiblebtn" ).removeClass( "activ_settings_items" );

                        }

                    }
                    //$("#status").val(data );
                }
            });
        }

        function deldeirection(c,n)
        {
            $.ajax({
                url: '{$config['home']}?action=delpreferdirection',
                type: 'post',
                data: {direction: c},
                success: function(data){

                    $("#direction_"+c).remove();
                }
            });
        }

        $( "#countries_direct" ).change(function() {
                stat =  $("#countries_direct").val();
                nm =  $("#countries_direct option[value='"+stat+"']").text();
                if(stat!="select")
                {
                    $.ajax({
                        url: '{$config['home']}?action=updatepreferdirection',
                        type: 'post',
                        data: {direction: stat},
                        success: function(data){
                        if(data!=false)
                        {

                            $("#directions_all").append('<li id="direction_'+data+'">'+nm+'<span onclick=\'deldeirection('+data+',"'+nm+'")\'>x</span></li>');
                        }else
                        {

                        }

                        }
                    });
                }
                });
        function changesettings2(c)
        {
            var url = "{$url}";
            $(location).attr('href',url);
            
        }
        $( window ).load(function() {
        
           $("#loaded_max").val(30);
           $("#page").val(2)
            /*
           $(".statuses_1 img").each(function ()
           {
             $(this).bind("load", function()
             {
                $(this).attr("src","images/spiffygif_30x30.gif");
                $(this).css("width","");
                $(this).css("min-height","");
                $(this).css("min-width","");
            });
          });*/
        });
        var loading = false;
        $(window).scroll(function(){
           if((($(window).scrollTop()+$(window).height())+50)>=$(document).height()){
              if(loading == false){
                 loading = true;
                 start = (parseInt($("#page").val())-1)*parseInt($("#loaded_max").val());
                 limit = parseInt($("#loaded_max").val());

                 $.get("{$config['home']}?action={$load_page}&start="+start+"&size="+limit, function(loaded){
                    $('.allstats').append(loaded);
                    $("#page").val(parseInt(parseInt($("#page").val())+1))
                    loading = false;
                 });
              }
           }
        });
        </script>
HTML;

        echo viewSite("Попутчики. Избранные статусы.",$con,$js);
        break;
    default:
        global $config;

        $check_user = get_user($_SESSION['user']['id']);

        if(!isset($check_user['first_name']) || !isset($check_user['birthday']) || !isset($check_user['avatar_id']) || ($check_user['first_name']=="") || ($check_user['birthday']=="") || ($check_user['avatar_id']==""))
        {
            header('Location: '.$config['home']."?action=editprofile");
        }
            $js = <<<HTML
            <style>
            .btn.active.focus, .btn.active:focus, .btn.focus, .btn:active.focus, .btn:active:focus, .btn:focus
            {
                outline: none!important;
                outline-offset:  none!important;
            }
            .btn.focus, .btn:focus
            {
                color:white!important;   
            }
            .btn:hover
            {
                color:#85c4ec!important;
            }
            </style>
                <script type="text/javascript">
                $(document).ready(function(){
                    $('#uploadHandler').click(function(){
                        $('#fileUpload').click();
                    });
                    $('#btnplus').click(function(){
                        $('#fileUpload2').click();

                    });
                });

                function readURL2(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();



                        reader.onload = function (e) {

                            $('#userImage')
                                .attr('src', e.target.result)
                                .width(225);


                        };

                        reader.readAsDataURL(input.files[0]);
                        $("#avaloadform2").click();
                    }

                }

                function readURL(input) {
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();



                        reader.onload = function (e) {

                            $('#userImage')
                                .attr('src', e.target.result)
                                .width(225);

                        };

                        reader.readAsDataURL(input.files[0]);

                        $("#avaloadform input[type='submit']").click();
                    }

                }


                function status() {


                    stat =  $("#status").val();

                    if(stat=="")
                    {
                        $("#my_notifications").html("<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Статус должен содержать текст!</div>");    
                    }else
                    {                    
                        $.ajax({
                            url: '{$config['home']}?action=setstatus',
                            type: 'post',
                            data: {status_text: stat},
                            success: function(data){
                                $("#my_notifications").html("<div class=\"alert alert-success alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>Новый статус: "
                                +data
                                +"</div>");
                                $("#status").val(data );
                            }
                        });
                    }

                };

            function changesettings(c)
            {
                $.ajax({
                    url: '{$config['home']}?action=updateinhospon',
                    type: 'post',
                    data: {stat: c},
                    success: function(data){
                        if(data==true)
                        {
                            if(c==1)
                            {
                                $( ".sponsorbtn" ).addClass( "activ_settings_items" );

                            }
                            if(c==2)
                            {
                                $( ".hostbtn" ).addClass( "activ_settings_items" );

                            }
                            if(c==3)
                            {
                               $( ".invisiblebtn" ).addClass( "activ_settings_items" );

                            }
                        }
                        else if(data==false)
                        {
                            if(c==1)
                            {
                               $( ".sponsorbtn" ).removeClass( "activ_settings_items" );


                            }
                            if(c==2)
                            {
                               $( ".hostbtn" ).removeClass( "activ_settings_items" );

                            }
                            if(c==3)
                            {
                               $( ".invisiblebtn" ).removeClass( "activ_settings_items" );

                            }

                        }
                        //$("#status").val(data );
                    }
                });
            }

            function deldeirection(c,n)
            {
                $.ajax({
                    url: '{$config['home']}?action=delpreferdirection',
                    type: 'post',
                    data: {direction: c},
                    success: function(data){

                        $("#direction_"+c).remove();
                    }
                });
            }

            $( "#countries_direct" ).change(function() {
                stat =  $("#countries_direct").val();
                nm =  $("#countries_direct option[value='"+stat+"']").text();
                if(stat!="select")
                {
                    $.ajax({
                        url: '{$config['home']}?action=updatepreferdirection',
                        type: 'post',
                        data: {direction: stat},
                        success: function(data){
                        if(data!=false)
                        {

                            $("#directions_all").append('<li id="direction_'+data+'">'+nm+'<span onclick=\'deldeirection('+data+',"'+nm+'")\'>x</span></li>');
                        }else
                        {

                        }

                        }
                    });
                }
                });
                </script>
HTML;
            $con = viewUserMain($_SESSION['user']['id']);

            echo viewSite("Попутчики. Страница профиля",$con,$js);
        break;
}

?>