$(function(){
	
	/*$('.phone').mask("+7 (999) 999-99-99");*/

	$('#first_form #buybtn').click(function(e){
		e.preventDefault();
		var id = $('#first_form input[name=purchase]:checked').val();
		$('#first_form').slideUp();
		$('#second_form').html('<div class="alert alert-info">Идет загрузка...</div>');
		$.ajax({
			url: user.home+'?action=new_purchase',
			type: "post",
			data: {purchase: id},
			success: function(data){
				$('#second_form').html(data);
			}
			
		});
	});
	
	$('#both_form').delegate('#toback', 'click', function(e){
		e.preventDefault();
		$('#second_form').html('');
		$('#first_form').slideDown();
	});
	
	$('.datepicker').datepicker({
		dateFormat: 'yy-mm-dd',
		dayNames: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
		dayNamesMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
		dayNamesShort: ["Вск", "Пон", "Втр", "Срд", "Чтв", "Птн", "Суб"],
		monthNames: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
		monthNamesShort: ["Янв", "Фев", "Март", "Апр", "Май", "Июнь", "Июль", "Авг", "Сент", "Окт", "Ноя", "Дек"],
		showMonthAfterYear: true,
		changeYear: true,
		changeMonth: true,
		yearRange: "-100:1997",
		firstDay: 1,
		gotoCurrent: true
	});

	
	setInterval(unread_count, 5000);

	ion.sound({
		sounds: [
			{
				name: "button_tiny"
			},
			{
				name: "water_droplet",
			},
			{
				name: "water_droplet_2",
			},
			{
				name: "water_droplet_3"
			}
		],
		volume: 0.5,
		path: "js/ion/sounds/",
		preload: true
	});

	var send_text_form = document.getElementById("send_text_form");
	
	if (send_text_form!=null)
	send_text_form.addEventListener("keydown", function(e) {
		if (!e) { var e = window.event; }

		// Enter is pressed
		if (e.keyCode == 13) { sendBtn(); }
	}, false);
	
});

var socket;
var attempts = 1;

function init() 
{
	var host = "ws://poputchiki.ru:9000/server/serv.php?user_id="+user.id;
	try {
		socket = new WebSocket(host);
		
		socket.onopen    = function(msg) { 
								attempts = 1;
								console.log('ok');
								unread_count();
						   };
		socket.onmessage = function(msg) 
							{
								var obj = eval('('+msg.data+')');
							   //log("Received: "+ obj.txt);
							    if(obj.status == 'message') {
									alertify.log(obj.from+obj.txt);
									ion.sound.play("button_tiny");
									log(obj.txt, 0, obj.sender_id, obj.ava)                                                    ;
								}else if(obj.status == 'count_read'){
									if(obj.number == 0){
										$('#counter').hide();
									}else{
										$('#counter').html(obj.number);
										$('#counter').show();
									}
								}
						   };
		socket.onclose   = function(msg) 
							{
								console.log('Disco');
								var time = generateInterval(attempts);
								
								setTimeout(function () 
								{
									attempts++;
									init();
								}, time); 
						   };
	}
	catch(ex)
	{ 
		//log(ex); 
	}
}
function generateInterval (k) 
{
	var maxInterval = (Math.pow(2, k) - 1) * 1000;
	if (maxInterval > 30*1000) {
	maxInterval = 30*1000; // If the generated interval is more than 30 seconds, truncate it down to 30 seconds.
	}
	// generate the interval to a random number between 0 and the maxInterval determined from above
	return Math.random() * maxInterval;
}

function send(msg)
{	
	socket.send(JSON.stringify(msg)); 
}

function quit(){
	if (socket != null) {
		//log("Goodbye!");
		socket.close();
		socket=null;
	}
}

function unread_count(){
	var obj = {};
	
	obj.func = "count_read";
	
	try { 
		send(obj);
	} catch(ex) { 
		console.log(ex);
	}
	
}

function mark_read(sender_id) {
	
	var obj = {};
	
	obj.func = "mark_read";
	obj.sender_id = sender_id;
	
	try {
		send(obj);
	} catch(e){
	}
	
}

// Utilities
function log(msg, with_prefix, id, ava){
	
	with_prefix = with_prefix == 1 ? 1 : 0;
	
	var html = '';
	
	var prefix = !with_prefix ? '' : '_my';
	
	ava = ava == '' ?  'images/poputchik.png' : ava;
	
	html += '<div class="chat_right_sms">';
	html += '';
	if(!with_prefix) html += '<div class="chat_left_ava"><a href="'+user.home+'?action=viewprofile&id='+id+'" target="_blank"><img src="'+ava+'" alt></a></div>';
	html += '<div class="chat_right_sms_block'+prefix+'">';
	html += '<span></span>';
	html += '<div class="chat_right_sms_block'+prefix+'_1">';
	html += '</div>';
	html += '<div class="chat_right_sms_block'+prefix+'_2">'+msg+'</div>';
	html += '</div>';
	if(with_prefix) html += '<div class="chat_left_ava"><a href="'+user.home+'?action=viewprofile&id='+user.id+'" target="_blank"><img src="'+ava+'" alt></a></div>';
	html += '</div>';
	
	$('.area_sms_'+id).append(html);
	
	$('.area_sms_'+id).stop().animate({
		scrollTop: $(".area_sms_"+id)[0].scrollHeight
	}, 800);
	
}

init();