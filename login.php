<?php

include_once "api/core.php";
global $config;

$action = $_GET['action'];

switch($action) {
	case "vk":
		$login = vklogin_site($_REQUEST['code'], $config['site'].'login.php?action=vk');
		$color = $login['status'] == 0 ? 'success' : 'danger';
		if($login['status'] == 1) buildMsg($login['message'], $color);
		//echo print_r($login);
		header("Location: ".$config['home']);
		break;
	case "fb":
		$login = fblogin_site($_REQUEST['code'], $config['site'].'login.php?action=fb');
		$color = $login['status'] == 0 ? 'success' : 'danger';
		if($login['status'] == 1) buildMsg($login['message'], $color);
		header("Location: ".$config['home']);
		break;
	
}