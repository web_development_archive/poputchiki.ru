#!/usr/bin/env php
<?php
// nohup ./serv.php </dev/null >> serv.log &
// kill -TERM $(cat ./serv.pid)
// cat ./serv.log

	session_save_path("/var/www/lelouch/data/bin-tmp");

	require_once('/var/www/lelouch/data/www/poputchiki.ru/server/websockets.php');
	require_once('/var/www/lelouch/data/www/poputchiki.ru/engine/core.php');
	
	if(file_exists('/var/www/lelouch/data/www/poputchiki.ru/server/serv.pid')) die("\nAlready running!\n\n");
	set_time_limit(0);
//	proc_nice(-15);

	declare(ticks = 1);
	function sig_handler_int() 
	{
		echo "Killed by int!\n";
		sig_handler();
	}
	function sig_handler_term() 
	{
		echo "Killed by term!\n";
		sig_handler();
	}
	function sig_handler_hup() 
	{
		echo "Killed by hup!\n";
		sig_handler();
	}
	function sig_handler_regular() 
	{
		echo "Killed by regular!\n";
		sig_handler();
	}
	
	function sig_handler() 
	{
		echo "Exiting!\n";
		@unlink('/var/www/lelouch/data/www/poputchiki.ru/server/serv.pid');
		//truncateConnections();		
		exit;
	}
	pcntl_signal(SIGINT,  "sig_handler_int");
	pcntl_signal(SIGTERM, "sig_handler_term");
	pcntl_signal(SIGHUP,  "sig_handler_hup");
	register_shutdown_function('sig_handler_regular');
	

	$pid = fopen('/var/www/lelouch/data/www/poputchiki.ru/server/serv.pid','w');
	fwrite($pid, getmypid());
	fclose($pid);

	if($argv[1]=='debug')
		error_reporting(E_ALL ^ E_NOTICE);
	else
		error_reporting(0);	
	//truncateConnections();

	class echoServer extends WebSocketServer 
	{
		//protected $maxBufferSize = 1048576; //1MB... overkill for an echo server, but potentially plausible for other applications.

		protected function send_msg($user, $message)
		{

			$to = get_user($message->receiver_id);
			$from = get_user($user->id);

			q2("INSERT INTO messages(sender_id, receiver_id, dt, txt) VALUES(:sender_id, :receiver_id, :dt, :txt)", array('sender_id' => $user->id, 'receiver_id' => $message->receiver_id, 'dt' => date('Y-m-d H:i:s'), 'txt' => $message->txt));		
			$id = qInsertId();
			
			//$this->send($user, array('id'=>$id, 'txt'=>$message->txt, 'receiver_id'=>$message->receiver_id, 'type' => 'outgoing', 'time' => fancyTime(time()), 'dt'=>time()));									

			echo "Send message to: {$to['id']}.\n";
			if ($to['id']!=-1)
			{	
				foreach($this->users as $usr)
				{	
					if($usr->id==$to['id'])
					{	
						$this->send($usr, array('status' => 'message', 'id'=>$id, 'from' => $from['first_name']." ".$from['last_name'].": ", 'sender_id' => $user->id, 'txt'=> $message->txt, 'receiver_id'=> $message->receiver_id, 'type' => 'incoming', 'time' => fancyTime(time()), 'dt'=>time(), 'ava' => $from['ava']));
					}
				}
			}
		}
		
		
		protected function mark_read($user, $message)
		{
			q2("UPDATE messages SET unread = 0 WHERE sender_id = :sender AND receiver_id = :receiver", array('sender' => $message->sender_id, 'receiver' => $user->id));
		}		

		protected function count_read($user, $message)
		{
			$num = qCount("SELECT COUNT(*) FROM messages WHERE receiver_id = :id AND unread = 1", array('id' => $user->id));
			$this->send($user, array('status' => 'count_read', 'number' => $num));									
		}
		
		protected function process ($user, $message) 
		{		
			try
			{
				if($message->func == 'send_msg')
					$this->send_msg($user, $message);
				if($message->func == 'mark_read')
					$this->mark_read($user, $message);
				if($message->func == 'count_read')
					$this->count_read($user, $message);
			}
				catch (Exception $e) 
 			{
			    echo 'Exception in function process(\n'.var_dump($user)."\n\n".var_dump($message). '\n)'.  $e->getMessage(). "\n";
			}	

		}

		
		protected function connected ($user) 
		{
	 		try
	 		{
				session_write_close();
				session_id();			
				@session_start();
				$path = session_save_path().'/sess_'.session_id();
				chmod($path, 0777);			
				$user->id = $user->headers['get']['user_id'];
				$user->admin = 0;
				session_write_close();		
				
				$u = q("SELECT * FROM users WHERE id = ?", array($user->id));
				
				if(count($u)>0)
				{
					$user->mail = $u[0]['mail'];
					$user->time_connected = time();
				}
				else
				{
					echo "Can't find him (user_id: {$user->id}). Kick!\n";
					$this->disconnect($user->socket);				
					return;
				}

				echo "Connected user: {$user->id} (token: {$user->cookie['token']})\n";
				//q2(SQL_INSERT_CONNECTION, array('hash'=>$user->hash, 'user_id'=> $user->id, 'token'=> $user->cookie['token'], 'params'=>json_encode($user->headers)));
 			}
 			catch (Exception $e) 
 			{
			    echo 'Exception in function connected(): ',  $e->getMessage(), "\n";
			}			

		}

		protected function closed ($user) 
		{
			try 
			{	
				//q2(SQL_DELETE_CONNECTION, array('hash'=>$user->hash));		
			}
			catch (Exception $e) 
 			{
			    echo 'Exception in function closed(): ',  $e->getMessage(), "\n";
			}

			// Do nothing: This is where cleanup would go, in case the user had any sort of
			// open files or other objects associated with them.  This runs after the socket 
			// has been closed, so there is no need to clean up the socket itself here.
		}
	}

	$echo = new echoServer("0.0.0.0","9000");
	try 
	{
		$echo->run();
	}
	catch (Exception $e) 
	{
		$echo->stdout($e->getMessage());
	}

?>