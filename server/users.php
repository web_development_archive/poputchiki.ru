<?php

class WebSocketUser {

  public $socket;
  public $hash;
  public $headers = array();
  public $handshake = false;

  public $handlingPartialPacket = false;
  public $partialBuffer = "";

  public $sendingContinuous = false;
  public $partialMessage = "";
  
  public $hasSentClose = false;

  function __construct($hash, $socket) {
    $this->hash = $hash;
    $this->socket = $socket;
  }
}